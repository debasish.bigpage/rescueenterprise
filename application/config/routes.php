<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Auth_controller';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['login']             = 'auth_controller/index';
$route['login-check']       = 'auth_controller/login_post';
$route['logout']            = 'auth_controller/logout';
$route['register']            = 'auth_controller/register';
$route['save-application']            = 'auth_controller/register_post';
$route['registration-compleate/(:any)']  = 'auth_controller/reg_thank_you/$1';
$route['change-password']   = 'auth_controller/change_password';
$route['update-password']   = 'auth_controller/change_password_post';
$route['reset-password']    = 'auth_controller/reset_password';
$route['forgot-password'] = 'auth_controller/forgot_password';
$route['check-forgot-password'] = 'auth_controller/forgot_password_post';
$route['staff-portal'] = 'staff_contoller/index';

$route['view-member/(:num)'] = 'Profile_controller/view_profile/$1';
// cron jobs
$route['cron/check-leg']= 'cron_controller/index';
$route['cron/cancel-lavel-income']= 'cron_controller/cancel_income';

/*
 *
 * ADMIN ROUTES
 *
 */
$route['admin']         = 'admin_controller/index';
$route['admin/dashboard']         = 'admin_controller/index';
$route['admin/email-settings']    = 'admin_controller/emailSettings';
$route['admin/pagination-settings']    = 'admin_controller/paginationSettings';
$route['admin/settings']    = 'admin_controller/settings';
$route['admin/visual-settings'] = 'admin_controller/visual_settings';
$route['admin/bank-settings'] = 'admin_controller/bank_settings';

// business plan settings
$route['admin/business-settings'] = 'admin_controller/business_settings';
$route['admin/add-business-plan'] = 'admin_controller/add_business_plan';
$route['admin/save-business-plan'] = 'admin_controller/add_business_plan_post';
$route['admin/edit-business-plan/(:num)'] = 'admin_controller/edit_business_plan/$1';
$route['admin/update-business-plan'] = 'admin_controller/edit_business_plan_post';

// product settings
$route['admin/products'] = 'Admin_product_controller/products';
$route['admin/add-product'] = 'Admin_product_controller/add_product';
$route['admin/save-product'] = 'Admin_product_controller/add_product_post';
$route['admin/edit-product/(:num)'] = 'Admin_product_controller/edit_product/$1';
$route['admin/edit-product-status/(:num)'] = 'Admin_product_controller/edit_product_status/$1';
$route['admin/update-product'] = 'Admin_product_controller/edit_product_post';

// order section
$route['admin/orders'] = 'order_controller/index';
$route['admin/create-order'] = 'order_controller/add_order';
$route['admin/save-order'] = 'order_controller/create_order';
$route['order-compleate/(:any)'] = 'order_controller/order_compleate/$1';
$route['admin/view-order-details/(:num)'] = 'order_controller/view_order_details/$1';
$route['admin/set-network'] = 'order_controller/setToNetwork';

// profile section
$route['admin/profile'] = 'profile_controller/profile';
$route['admin/save-profile'] = 'profile_controller/save_profile';

//staff section
$route['admin/staff'] = 'admin_controller/staff';
$route['admin/add-staff'] = 'admin_controller/add_staff';
$route['admin/save-staff'] = 'admin_controller/add_staff_post';
$route['admin/edit-staff/(:num)'] = 'admin_controller/edit_staff/$1';
$route['admin/update-staff'] = 'admin_controller/update_staff';
$route['admin/edit-staff-status/(:num)'] = 'admin_controller/edit_staff_status/$1';
$route['admin/edit-account/(:num)'] = 'admin_controller/edit_customer_account/$1';
$route['admin/save-alter-user'] = 'admin_controller/alter_user';
//report Section
$route['admin/transactions'] = 'admin_controller/transactions';
$route['admin/filter-transactions'] = 'admin_controller/filter_transactions';

$route['admin/payout-report'] = 'admin_controller/payouts';
$route['admin/filter-payouts'] = 'admin_controller/filter_payout';
$route['admin/release-all-payouts'] = 'admin_controller/release_bulk_payout';
$route['admin/release-payout/(:num)'] = 'admin_controller/release_payout/$1';
$route['admin/pending-payout-report'] = 'admin_controller/pending_payouts';
$route['admin/blocked-payout'] = 'admin_controller/blocked_payouts';
$route['admin/unblock-payout/(:num)'] = 'admin_controller/unblock_payout/$1';
$route['admin/unblock-all-payouts'] = 'admin_controller/unblock_all_payout';

$route['admin/mlm-users'] = 'admin_controller/mlm_user';
$route['admin/mlm-accounts'] = 'admin_controller/mlm_accounts';
$route['admin/edit-user-status/(:num)'] = 'admin_controller/edit_user_status/$1';
$route['admin/view-all-accounts/(:num)'] = 'admin_controller/view_all_ac_by_id/$1';
$route['admin/tree-view/(:num)'] = 'admin_controller/generology/$1';
$route['admin/leavel-list'] = 'admin_controller/leavel_list';


// order section
$route['staff-portal/orders'] = 'order_controller/index';
$route['staff-portal/create-order'] = 'order_controller/add_order';
$route['staff-portal/save-order'] = 'order_controller/create_order';
$route['staff-portal/view-order-details/(:num)'] = 'order_controller/view_order_details/$1';
$route['staff-portal/set-network'] = 'order_controller/setToNetwork';
//report Section
$route['staff-portal/transactions'] = 'staff_contoller/transactions';
$route['staff-portal/filter-transactions'] = 'staff_contoller/filter_transactions';
//profile
$route['staff-portal/profile'] = 'profile_controller/profile';
$route['staff-portal/save-profile'] = 'profile_controller/save_profile';
/*
 *
 * Member ROUTES
 *
 */
$route['member-portal'] = 'member_contoller/index';
// order section
$route['member-portal/orders'] = 'order_controller/index';
$route['member-portal/view-order-details/(:num)'] = 'order_controller/view_order_details/$1';
//report Section
$route['member-portal/transactions'] = 'member_contoller/transactions';
$route['member-portal/filter-transactions'] = 'member_contoller/filter_transactions';
//
$route['member-portal/my-accounts'] = 'member_contoller/my_accounts';
$route['member-portal/tree-view/(:num)'] = 'member_contoller/generology/$1';
$route['member-portal/received-reward'] = 'member_contoller/received_payout';
$route['member-portal/blocked-reward'] = 'member_contoller/blocked_payout';
$route['member-portal/pending-reward'] = 'member_contoller/pending_payout';


