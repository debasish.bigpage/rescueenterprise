<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Admin_controller extends Admin_Core_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!auth_check()) {
            redirect('login');
        }
        if (!is_admin()) {
            redirect('dashboard');
        }
    }
    public function index()
    {
        $data['title'] = "Admin Panel";
        $data['total_member'] = $this->authication_model->get_cond_users_count();
        $data['total_product'] =$this->product_admin_model->get_products_count();
        $data['total_order'] =$this->order_model->get_orders_count();
        $data['total_accounts'] =$this->accounts_model->get_account_count();
        $data['orders'] =$this->order_model->get_orders();
        $data['income'] =$this->transaction_model->getSumIncome();
        $data['expenses'] =$this->transaction_model->getSumExpenses();
        $data['pay_pending'] =$this->payout_model->get_all_payouts_count(['status'=>0]);
        $data['pay_blocked'] =$this->payout_model->get_all_payouts_count(['status'=>2]);
        $data['pending_pay'] = $this->payout_model->get_all_payouts(array('status' => 0));
        $data['block_pay'] = $this->payout_model->get_all_payouts(array('status' => 2));
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/index');
        $this->load->view('admin/includes/footer');
    }
    public function settings()
    {
        $data['title'] = "settings";
        $data['panel_settings'] = $this->settings_model->get_panel_settings();
        $data['settings'] = $this->settings_model->get_settings();
        $data['general_settings'] = $this->settings_model->get_general_settings();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/settings');
        $this->load->view('admin/includes/footer');
    }
    /**
     * Recaptcha Settings Post
     */
    public function recaptcha_settings_post()
    {
        if ($this->settings_model->update_recaptcha_settings()) {
            $this->session->set_flashdata('success', "Setting Updated");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable To Update Settings");
            redirect($this->agent->referrer());
        }
    }

    /**
     * Settings Post
     */
    public function settings_post()
    {
        if ($this->settings_model->update_settings()) {
            $this->settings_model->update_general_settings();
            $this->session->set_flashdata('success', "Setting Updated");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable To Update Settings");
            redirect($this->agent->referrer());
        }
    }

    public function emailSettings()
    {
        $data['title'] = "Email Settings";
        $data['general_settings'] = $this->settings_model->get_general_settings();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/email-settings');
        $this->load->view('admin/includes/footer');
    }

    /**
     * Email Settings Post
     */
    public function emailSettings_post()
    {
        if ($this->settings_model->update_email_settings()) {
            $this->session->set_flashdata('success', "Setting Updated");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable To Update Settings");
            redirect($this->agent->referrer());
        }
    }
    /*
    *-------------------------------------------------------------------------------------------------
    * Visual SETTINGS
    *-------------------------------------------------------------------------------------------------
    */

    /*
    * Visual Settings
    */
    public function visual_settings()
    {
        $data['title'] = "visual settings";
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/visual_settings', $data);
        $this->load->view('admin/includes/footer');
    }

    /**
     * Visual Settings Post
     */
    public function visual_settings_post()
    {
        if ($this->settings_model->update_visual_settings()) {
            $this->session->set_flashdata('success', "updated");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "fail");
            redirect($this->agent->referrer());
        }
    }

    /*
    *-------------------------------------------------------------------------------------------------
    * Business SETTINGS
    *-------------------------------------------------------------------------------------------------
    */
    public function business_settings()
    {
        $data['title'] = "Business Plan settings";
        $data['business_settings'] = $this->businessplan_model->get_all_business_plans();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/business/business_settings', $data);
        $this->load->view('admin/includes/footer');
    }

    public function add_business_plan()
    {
        $data['title'] = "Add Business Plan";
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/business/add_new');
        $this->load->view('admin/includes/footer');
    }

    /**
     * Add blog Category post
     */
    public function add_business_plan_post()
    {
        //validate inputs
        $this->form_validation->set_rules('level', "Leavel", 'required|xss_clean|is_unique[business_settings.level]');
        $this->form_validation->set_rules('leg', "Total number of person", 'required|xss_clean');
        $this->form_validation->set_rules('amount', "Amount per set", 'required|xss_clean|numeric');
        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {
            if ($this->businessplan_model->add_business_plan()) {
                $this->session->set_flashdata('success', "Plan Added Successfuly");
                redirect($this->agent->referrer());
            } else {
                $this->session->set_flashdata('error', "Unable To Add Plan");
                redirect($this->agent->referrer());
            }
        }
    }

    


    /**
     * edit blog  team
     */
    public function edit_business_plan($id)
    {

        $data['title'] = "Update Business Plan";
        $data['plan'] = $this->businessplan_model->get_business_plan_by_id($id);
        if (empty($data['plan'])) {
            redirect($this->agent->referrer());
        }
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/business/edit_plan', $data);
        $this->load->view('admin/includes/footer');
    }

    /**
     * edit blog team post
     */
    public function edit_business_plan_post()
    {
        //validate inputs
        $this->form_validation->set_rules('level', "Leavel", 'required|xss_clean');
        $this->form_validation->set_rules('leg', "Total number of person", 'required|xss_clean');
        $this->form_validation->set_rules('amount', "Amount per set", 'required|xss_clean|numeric');
        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {

            $id = $this->input->post('id', true);
            if ($this->businessplan_model->update($id)) {
                $this->session->set_flashdata('success', "Plan Updated Successfuly");
                redirect(admin_url() . 'business-settings');
            } else {
                $this->session->set_flashdata('error', "Unable To Update ");
                redirect($this->agent->referrer());
            }
        }
    }
    public function delete_businessplan()
    {
        $id = $this->input->post('id', true);
        //check blog posts
        if ($this->businessplan_model->delete_business_plan($id)) {
            $this->session->set_flashdata('success', "Plan Deleted");
        } else {
            $this->session->set_flashdata('error', "Unable To delete Plan");
        }
    }

    /*
    *-------------------------------------------------------------------------------------------------
    * STAFF SETTINGS
    *-------------------------------------------------------------------------------------------------
    */

    public function staff()
    {
        $data['title'] = "Staff";
        $data['staffs'] = $this->authication_model->get_Staffs();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/staff/staff', $data);
        $this->load->view('admin/includes/footer');
    }

    public function add_staff()
    {
        $data['title'] = "Add Staff";
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/staff/add_new');
        $this->load->view('admin/includes/footer');
    }

    /**
     * Add blog Category post
     */
    public function add_staff_post()
    {
        $mem_count = get_totalUserCount();
        $month = chr(64 + date('m'));
        $username = 'RES' . date('y') . $month . date('d') . '0' . ($mem_count + 1);
        $this->form_validation->set_rules('full_name', "Name", 'required|xss_clean|min_length[4]|max_length[50]');
        $this->form_validation->set_rules('dob', "Date of birth", 'required|xss_clean');
        $this->form_validation->set_rules('mobile', "Mobile Number", 'required|xss_clean|min_length[10]|max_length[10]|is_unique[users.mobile]');
        $this->form_validation->set_rules('email', "Email", 'required|xss_clean|min_length[5]|is_unique[users.email]|valid_email');
        $this->form_validation->set_rules('password', "Password", 'required|min_length[4]|max_length[255]');
        $this->form_validation->set_rules('cpass', "Confirm password", 'required|matches[password]');

        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {
                $this->load->library('bcrypt');
                $data = array(
                    'username' => $username,
                    'full_name' => $this->input->post('full_name', true),
                    'password' => $this->bcrypt->hash_password($this->input->post('password', true)),
                    'dob' => $this->input->post('dob', true),
                    'mobile' => $this->input->post('mobile', true),
                    'email' => $this->input->post('email', true),
                    'last_seen' => date('Y-m-d H:i:s'),
                    'created_on' => date('Y-m-d H:i:s'),
                    'is_approved' => 0,
                    'email_status' => 1,
                    'role' => 'staff',
                    'token' => generate_token(),
                    'user_type' => 'registered',
                    'slug' => $username,
                );
                if ($this->authication_model->register($data)) {
                    redirect('admin/staff');
                } else {
                    $this->session->set_flashdata('error', "Unable to create account!");
                    redirect($this->agent->referrer());
                }
        }
    }
    /**
     * edit blog  team
     */
    public function edit_staff($id)
    {
        
        $data['title'] = "Update Staff";
        $data['staff'] = $this->authication_model->get_staff($id);
        if (empty($data['staff'])) {
            redirect($this->agent->referrer());
        }
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/staff/edit_staff', $data);
        $this->load->view('admin/includes/footer');
    }

    /**
     * edit blog team post
     */
    public function update_staff()
    {
        //validate inputs
        $this->form_validation->set_rules('full_name', "Name", 'required|xss_clean|min_length[4]|max_length[50]');
        $this->form_validation->set_rules('dob', "Date of birth", 'required|xss_clean');
        $this->form_validation->set_rules('password', "Password", 'min_length[4]|max_length[255]');
        $this->form_validation->set_rules('cpass', "Confirm password", 'matches[password]');
        if ($this->form_validation->run() === false) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {
            $id = $this->input->post('id', true);
            $full_name = $this->input->post('full_name', true);
            $dob = $this->input->post('dob', true);
            $password = $this->input->post('password', true);
            $cpass = $this->input->post('cpass', true);
            $updateArr = array(
                'full_name'=> $full_name,
                'dob'=> $dob,
            );
            if(!empty($password)){
                $this->load->library('bcrypt');
                $updateArr['password']= $this->bcrypt->hash_password($password);

            }
            if ($this->authication_model->updateUsers($updateArr,$id)) {
                $this->session->set_flashdata('success', "Staff Updated Successfuly");
                redirect(admin_url() . 'staff');
            } else {
                $this->session->set_flashdata('error', "Unable To Update ");
                redirect($this->agent->referrer());
            }
        }
    }

    public function edit_staff_status($id)
    {
        $id = clean_number($id);
        if($this->authication_model->ban_remove_ban_user($id)) {
            $this->session->set_flashdata('success', "Staff Updated");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable To Update ");
            redirect($this->agent->referrer());
        }
    }

    /*
    *-------------------------------------------------------------------------------------------------
    * Report Section
    *-------------------------------------------------------------------------------------------------
    */

    /*
    *-------------------------------------------------------------------------------------------------
    * Transactions
    *-------------------------------------------------------------------------------------------------
    */

    public function transactions()
    {
        $data['title'] = "Total Transactions";
        $data['transactions'] = $this->transaction_model->get_all_trannsactions();

        if (empty($data['transactions'])) {
            redirect($this->agent->referrer());
        }
        $data['pl_status'] = $this->transaction_model->getTnsPL();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/transactions', $data);
        $this->load->view('admin/includes/footer');
    }

    public function filter_transactions()
    {
        $formInp = $this->input->post('form');
        $toInp = $this->input->post('to');
        $form = $formInp . ' 00:00:00';
        $to  = date("Y-m-d H:i:s", strtotime($toInp));
        $where = " 	created_at >= '" . $form . "' AND 	created_at <= '" . $to . "' ORDER BY id DESC";
        $data['title'] = "Total Transactions";
        $sql = "SELECT * FROM transactions WHERE";
        $sql .= $where;
        $data['transactions'] = $this->db->query($sql)->result();
        $data['pl_status'] = $this->transaction_model->getTnsPL($where);
        $data['form_date'] = $formInp;
        $data['to_date'] = $toInp;
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/filter_transactions', $data);
        $this->load->view('admin/includes/footer');
    }
    /*
    *-------------------------------------------------------------------------------------------------
    * Payouts
    *-------------------------------------------------------------------------------------------------
    */
    public function payouts()
    {
        $data['title'] = "Payouts";
        $data['transactions'] = $this->payout_model->get_all_payouts();
        $data['pay_pending'] = $this->payout_model->get_all_payouts_count(array('status' => 0));
        $data['pay_done'] = $this->payout_model->get_all_payouts_count(array('status' => 1));
        $data['blocked_pay'] = $this->payout_model->get_all_payouts_count(['status' => 2]);
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/payout', $data);
        $this->load->view('admin/includes/footer');
    }

    public function filter_payout()
    {
        $formInp = $this->input->post('form');
        $toInp = $this->input->post('to');
        $form = $formInp . ' 00:00:00';
        $to  = $toInp . ' 23:59:59';
        $where = " created_at >= '" . $form . "' AND created_at <= '" . $to . "' ORDER BY id DESC";
        $data['title'] = "Filter Payouts";
        $sql = "SELECT * FROM payout_tracking WHERE";
        $sql .= $where;
        $data['transactions'] = $this->db->query($sql)->result();
        $data['form_date'] = $formInp;
        $data['to_date'] = $toInp;
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/filter_payout', $data);
        $this->load->view('admin/includes/footer');
    }

    public function pending_payouts()
    {
        $data['title'] = "Pending Payouts";
        $data['transactions'] = $this->payout_model->get_all_payouts(array('status' => 0));
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/pending_payout', $data);
        $this->load->view('admin/includes/footer');
    }

    public function release_payout($id)
    {
        $id = clean_number($id);
        $payoutData = $this->payout_model->get_payout_by_id($id);
        $tranactionArr = [
            'form_user_id' => 1,
            'to_user_id' => $payoutData->user_id,
            'form_account_id' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'dr' => 0,
            'cr' => $payoutData->amount,
            'perticullars' => $payoutData->description,
        ];
        $updateArr = [
            'payment_on' => date("Y-m-d H:i:s"),
            'status' => 1
        ];
        if ($this->transaction_model->add_transaction($tranactionArr)) {
            $this->payout_model->payout_update($updateArr, $id);
            $this->session->set_flashdata('success', "Payment Released");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable Release Payment");
            redirect($this->agent->referrer());
        }
    }

    public function release_bulk_payout()
    {

        $user_ids = $this->input->post('user_ids');
        if (!empty($user_ids)) {
            foreach ($user_ids as $id) {
                $this->do_payout_release($id);
                $this->session->set_flashdata('success', "All Payment Released");
                redirect($this->agent->referrer());
            }
        } else {
            $this->session->set_flashdata('error', "No Payment Selected");
            redirect($this->agent->referrer());
        }
    }

    protected function do_payout_release($id)
    {
        $id = clean_number($id);
        $payoutData = $this->payout_model->get_payout_by_id($id);
        $tranactionArr = [
            'form_user_id' => 1,
            'to_user_id' => $payoutData->user_id,
            'form_account_id' => $payoutData->user_id,
            'user_by' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'dr' => 0,
            'cr' => $payoutData->amount,
            'perticullars' => $payoutData->description,
        ];
        $updateArr = [
            'payment_on' => date("Y-m-d H:i:s"),
            'status' => 1
        ];
        $this->transaction_model->add_transaction($tranactionArr);
        $this->payout_model->payout_update($updateArr, $id);
    }

    public function blocked_payouts()
    {
        $data['title'] = "Blocked Payouts";
        $data['transactions'] = $this->payout_model->get_all_payouts(['status' => 2]);
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/blocked_payout', $data);
        $this->load->view('admin/includes/footer');
    }

    public function unblock_payout($id)
    {
        $id = clean_number($id);
        $updateArr = [
            'status' => 0
        ];
        if ($this->payout_model->payout_update($updateArr, $id)) {
            $this->session->set_flashdata('success', "Payout Unblocked");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable Unblock");
            redirect($this->agent->referrer());
        }
        
        
    }

    public function unblock_all_payout()
    {
        $user_ids = $this->input->post('user_ids');
        if (!empty($user_ids)) {
            foreach ($user_ids as $id) {
                $updateArr = [
                    'status' => 0
                ];
                $this->payout_model->payout_update($updateArr, $id);
                $this->session->set_flashdata('success', "All Payouts Unblocked");
                redirect($this->agent->referrer());
            }
        } else {
            $this->session->set_flashdata('error', "No Item Selected");
            redirect($this->agent->referrer());
        }
    }

    /*
    *-------------------------------------------------------------------------------------------------
    * MLM SECTION REPORT
    *-------------------------------------------------------------------------------------------------
    */
    public function mlm_user()
    {
        $data['title'] = "Customer List";
        $data['users'] = $this->authication_model->getUserListResult();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/mlm_users', $data);
        $this->load->view('admin/includes/footer');
    }

    public function mlm_accounts()
    {
        $data['title'] = "Customer Account List";
        $data['users'] = $this->accounts_model->get_all_accounts();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/mlm_accounts', $data);
        $this->load->view('admin/includes/footer');
    }

    public function edit_user_status($id)
    {
        $id = clean_number($id);
        if ($this->authication_model->ban_remove_ban_user($id)) {
            $this->session->set_flashdata('success', "User Updated");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable To Update ");
            redirect($this->agent->referrer());
        }
    }

    public function view_all_ac_by_id($id)
    {
        $id = clean_number($id);
        $users = $this->accounts_model->get_all_accounts(['user_id' => $id]);
        $data['title'] = "All A/C Against CIF";
        $data['users'] = $users;
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/child_accounts', $data);
        $this->load->view('admin/includes/footer');
    }
    public function generology($id)
    {
        $id = clean_number($id);
        $data['parentData']= $this->accounts_model->get_account_by_id($id);
        $data["childData"] = $this->network_model->getNetworkChild($id);
        $data['title'] = "Tree View";
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/mlm_tree', $data);
        $this->load->view('admin/includes/footer');
    }


    public function leavel_list()
    {
        
        $data['title'] = "User Leavel List";
        $data['l1Accounts'] = $this->accounts_model->get_all_accounts(['mlm_level'=>1]);
        $data['l2Accounts'] = $this->accounts_model->get_all_accounts(['mlm_level'=>2]);
        $data['l3Accounts'] = $this->accounts_model->get_all_accounts(['mlm_level'=>3]);
        $data['l4Accounts'] = $this->accounts_model->get_all_accounts(['mlm_level'=>4]);
        $data['l5Accounts'] = $this->accounts_model->get_all_accounts(['mlm_level'=>5]);
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/report/leavel_List', $data);
        $this->load->view('admin/includes/footer');
    }

     /*
    *-------------------------------------------------------------------------------------------------
    *EDIT CUSTOMER / ALTER Customer
    *-------------------------------------------------------------------------------------------------
    */


    public function edit_customer_account($id)
    {
        $data['title'] = "Edit Customer Account";
        $data['user_data'] = $this->authication_model->get_user($id);
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/edit_account', $data);
        $this->load->view('admin/includes/footer');
    }

    public function alter_user()
    {
        $this->form_validation->set_rules('sex', "Sex", 'required|xss_clean');
        $this->form_validation->set_rules('address', "Address", 'required|xss_clean');
        $this->form_validation->set_rules('state', "State", 'required|xss_clean');
        $this->form_validation->set_rules('pin', "PIN Code", 'required|xss_clean|min_length[6]|max_length[6]|numeric');
        $this->form_validation->set_rules('mobile', "Mobile Number", 'required|xss_clean|min_length[10]|max_length[10]|is_unique[users.mobile]');
        $this->form_validation->set_rules('email', "Email", 'xss_clean|min_length[5]|is_unique[users.email]|valid_email');


        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {
            $id = $this->input->post('id', true);
            $user =$this->authication_model->get_user($id);
            if($user->avatar!=NULL || !empty($user->avatar)){
                // del image
                delete_file_from_server('uploads/member-doc/' . $user->avatar);
            }
            $this->load->library('bcrypt');
            
            $data = array(
                'full_name' => $this->input->post('full_name', true),
                'dob' => $this->input->post('dob', true),
                'sex' => $this->input->post('sex', true),
                'address' => $this->input->post('address', true),
                'state' => $this->input->post('state', true),
                'pin' => $this->input->post('pin', true),
                'mobile' => $this->input->post('mobile', true),
                'email' => $this->input->post('email', true),
                'password'=> $this->bcrypt->hash_password($this->input->post('mobile', true)),
                'avatar'=>"",
            );
            if ($this->authication_model->updateUsers($data, $id)) {
                $this->session->set_flashdata('success', "Account Alter new Password is mobile number");
                redirect($this->agent->referrer());
            } else {

                $this->session->set_flashdata('error', "Unable update profile!");
                redirect($this->agent->referrer());
            }
        }
    }

 
}
