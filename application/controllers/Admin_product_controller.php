<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Admin_product_controller extends Admin_Core_Controller
{
    public function __construct()
    {
        parent::__construct();
        //check user
        if (!is_admin()) {
            redirect(admin_url() . 'login');
        }
    }

    /**
     * Products
     */
    public function products()
    {
        $data['title'] = "products";
        $data['products'] = $this->product_admin_model->get_products();
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/product/products', $data);
        $this->load->view('admin/includes/footer');
    }

    public function add_product()
    {
        $data['title'] = "Add product";
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/product/add_product', $data);
        $this->load->view('admin/includes/footer');
    }

    public function add_product_post()
    {
        $this->form_validation->set_rules('title', "Product Name", 'required|xss_clean|min_length[4]');
        $this->form_validation->set_rules('price', "Product Price", 'required|xss_clean');
        $this->form_validation->set_rules('status', "Product Status", 'required|xss_clean');
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        }else{
            
            if ($this->product_admin_model->add_product()) {
                //last id
                $last_id = $this->db->insert_id();
                //update slug
                $this->product_admin_model->update_slug($last_id);
                $this->session->set_flashdata('success', "Product Added");
                redirect('admin/products');
            } else {
                $this->session->set_flashdata('error', "Try again");
                redirect($this->agent->referrer());
            }
        }
    }
  

    /**
     * Product Details
     */
    public function edit_product($id)
    {
        $data['title'] = "Add product";
        $data['product'] = $this->product_admin_model->get_product($id);
        if (empty($data['product'])) {
            redirect($this->agent->referrer());
        }
        $this->load->view('admin/includes/header', $data);
        $this->load->view('admin/product/edit_product', $data);
        $this->load->view('admin/includes/footer');
    }

    public function edit_product_post()
    {
        $this->form_validation->set_rules('title', "Product Name", 'required|xss_clean|min_length[4]');
        $this->form_validation->set_rules('price', "Product Price", 'required|xss_clean');
        $this->form_validation->set_rules('status', "Product Status", 'required|xss_clean');
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {
            $id = $this->input->post('id');
            if ($this->product_admin_model->update_porduct($id)) {
                //update slug
                $this->product_admin_model->update_slug($id);
                $this->session->set_flashdata('success', "Product Updated");
                redirect('admin/products');
            } else {
                $this->session->set_flashdata('error', "Try again");
                redirect($this->agent->referrer());
            }
        }
    }

   
    /**
     * Delete Product
     */
    public function delete_product()
    {
        $id = $this->input->post('id', true);
        if ($this->product_admin_model->delete_product($id)) {
            $this->session->set_flashdata('success', "product deleted");
        } else {
            $this->session->set_flashdata('error', "unable to delete");
        }

        //reset cache
        
    }

    public function edit_product_status($id)
    {
        $id = clean_number($id);
        if ($this->product_admin_model->change_status($id)) {
            $this->session->set_flashdata('success', "Product Updated");
            redirect($this->agent->referrer());
        } else {
            $this->session->set_flashdata('error', "Unable To Update ");
            redirect($this->agent->referrer());
        }
    }

   
}
