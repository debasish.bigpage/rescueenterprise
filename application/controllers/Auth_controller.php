<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Auth_controller extends Home_Core_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if (auth_check()) {
            redirect('dashboard');
        } else {
            $data['title'] = "Login";
            $data['description'] = "Login" . " - " . $this->app_name;
            $data['keywords'] = "Login" . "," . $this->app_name;
            $this->load->view('auth/_header', $data);
            $this->load->view('auth/index');
            $this->load->view('auth/_footer');
        }
    }

    public function login_post()
    {
        //check auth
        if (auth_check()) {
            echo "User Already Login";
            die;
        }
        //validate inputs
        $username =  remove_special_characters($this->input->post('username', true));

        if (!empty($username)) {
            $this->form_validation->set_rules('username', 'Userid', 'required|xss_clean|max_length[100]');
        }
        $this->form_validation->set_rules('password', "Password", 'required|xss_clean|max_length[30]');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('errors', validation_errors());
            redirect($this->agent->referrer());
        } else {

            if ($this->authication_model->login()) {
                redirect('dashboard');
            } else {
                redirect($this->agent->referrer());
            }
        }
    }


    public function logout()
    {
        $this->authication_model->update_last_seen();
        $this->authication_model->logout();
        redirect('login');
    }



    /**
     * Forgot Password
     */
    public function forgot_password()
    {
        //check if logged in
        if (auth_check()) {
            redirect(base_url());
        }
        $this->load->view('auth/_header');
        $this->load->view('auth/forgot_password');
        $this->load->view('auth/_footer');
    }

    /**
     * Forgot Password Post
     */
    public function forgot_password_post()
    {
        //check auth
        if ($this->auth_check) {
            redirect(base_url());
        }
        $email = $this->input->post('email', true);
        //get user
        $user = $this->authication_model->get_user_by_email($email);
        //if user not exists
        if (empty($user)) {
            $this->session->set_flashdata('error', "No such user found");
            redirect($this->agent->referrer());
        } else {
            $this->load->model("email_model");
            $this->email_model->send_email_reset_password($user->id);
            $this->session->set_flashdata('success', "We've sent an email for resetting your password to your email address. Please check your email for next steps");
            redirect($this->agent->referrer());
        }
    }

    /**
     * Reset Password
     */
    public function reset_password()
    {
        get_method();
        //check if logged in
        if ($this->auth_check) {
            redirect(base_url());
        }
        $data['title'] = "Reset Password";
        $data['description'] = "Reset Password" . " - " . $this->app_name;
        $data['keywords'] = "Reset Password" . "," . $this->app_name;
        //$data['user_session'] = get_user_session();
        $token = $this->input->get('token', true);
        //get user
        $data["user"] = $this->authication_model->get_user_by_token($token);
        $data["success"] = $this->session->flashdata('success');
        if (empty($data["user"]) && empty($data["success"])) {
            redirect(base_url());
        }
        $this->load->view('auth/reset_password', $data);
    }

    /**
     * Reset Password Post
     */
    public function reset_password_post()
    {
        $success = $this->input->post('success', true);
        if ($success == 1) {
            redirect(base_url());
        }
        $user_id = $this->input->post('id', true);
        $this->form_validation->set_rules('password', "New Password", 'required|xss_clean|min_length[4]|max_length[50]');
        $this->form_validation->set_rules('password_confirm', "Password Confirm", 'required|xss_clean|matches[password]');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('errors', validation_errors());
            //$this->session->set_flashdata('form_data', $this->profile_model->change_password_input_values());
            $data["user"] = $this->authication_model->get_user($user_id);
            $data["success"] = $this->session->flashdata('success');
            $headerdata['page_title'] = "Reset Password";
            $this->load->view('auth/reset_password', $data);
        } else {
            if ($this->authication_model->reset_password($user_id)) {
                $this->session->set_flashdata('success', "Password reset successful");
                redirect('login');
            } else {
                $this->session->set_flashdata('error', "Unable to reset password. Try again!");
                redirect('login');
            }
        }
    }

    public function change_password()
    {
        //check user
        if (!auth_check()) {
            redirect('signin');
        }
        $data['title'] = "Change Password";
        if ($this->session->userdata('bp_sess_user_role') == "admin") {
        $this->load->view('admin/includes/header', $data);
        $this->load->view('auth/change_password');
        $this->load->view('admin/includes/footer');
        } elseif ($this->session->userdata('bp_sess_user_role') == "staff") {
            $this->load->view('staff/includes/header', $data);
            $this->load->view('auth/change_password');
            $this->load->view('staff/includes/footer');
        } elseif ($this->session->userdata('bp_sess_user_role') == "member") {
            $this->load->view('user/includes/header', $data);
            $this->load->view('auth/change_password');
            $this->load->view('user/includes/footer');
        }
    }

    /**
     * Change Password Post
     */
    public function change_password_post()
    {
        //check user
        if (!auth_check()) {
            redirect('signin');
        }

        $this->form_validation->set_rules('old_password', "Old Password", 'required|xss_clean|min_length[4]|max_length[50]');
        $this->form_validation->set_rules('new_password', "New Password", 'required|xss_clean|min_length[4]|max_length[50]');
        $this->form_validation->set_rules('password_confirm', "Password Confirm", 'required|xss_clean|matches[new_password]|min_length[4]|max_length[50]');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('errors', validation_errors());
            redirect($this->agent->referrer());
        } else {
            $data =  array(
                'old_password' => $this->input->post('old_password', true),
                'new_password' => $this->input->post('new_password', true),
                'password_confirm' => $this->input->post('password_confirm', true)
            );
            if ($this->authication_model->change_password($data)) {
                $this->session->set_flashdata('success', "Password Updated Successfully");
                redirect('change-password');
            } else {
                $this->session->set_flashdata('error', "Change Password Error");
                redirect('change-password');
            }
        }
    }

    /**
     * Register
     */
    public function register()
    {
        //check if logged in
        if ($this->auth_check) {
            redirect(base_url());
        }
        $data['title'] = "Register";
        $data['description'] = "Register" . " - " . $this->app_name;
        $data['keywords'] = "Register" . "," . $this->app_name;
        $this->load->view('auth/_header', $data);
        $this->load->view('auth/register');
        $this->load->view('auth/_footer');
    }

    public function register_post()
    {
        $mem_count = get_totalUserCount();
        $month = chr(64 + date('m'));
        $username = 'ACM' . date('y') . $month . date('d') . '0' . ($mem_count + 1);
        $this->form_validation->set_rules('full_name', "Name", 'required|xss_clean|min_length[4]|max_length[50]');
        $this->form_validation->set_rules('dob', "Date of birth", 'required|xss_clean');
        $this->form_validation->set_rules('sex', "Gender", 'required|xss_clean');
        $this->form_validation->set_rules('pan', "PAN Number", 'required|xss_clean|min_length[10]|max_length[10]|is_unique[users.pan]');
        $this->form_validation->set_rules('mobile', "Mobile Number", 'required|xss_clean|min_length[10]|max_length[10]|is_unique[users.mobile]');
        $this->form_validation->set_rules('email', "Email", 'required|xss_clean|min_length[5]|is_unique[users.email]|valid_email');
        $this->form_validation->set_rules('spon_id', "Sponsor ID", 'required|xss_clean|min_length[5]|callback_sponsor_exists');
        $this->form_validation->set_rules('password', "Password", 'required|min_length[4]|max_length[255]');
        $this->form_validation->set_rules('cpass', "Confirm password", 'required|matches[password]');

        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('error', validation_errors());

            redirect($this->agent->referrer());
        } else {
            if (!$this->recaptcha_verify_request()) {

                $this->session->set_flashdata('error', "Please confirm that you are not a robot!");
                redirect($this->agent->referrer());
            } else {
                $this->load->library('bcrypt');
                $data = array(
                    'username' => $username,
                    'full_name' => $this->input->post('full_name', true),
                    'password' => $this->bcrypt->hash_password($this->input->post('password', true)),
                    'dob' => $this->input->post('dob', true),
                    'sex' => $this->input->post('sex', true),
                    'pan' => $this->input->post('pan', true),
                    'mobile' => $this->input->post('mobile', true),
                    'age' => $this->input->post('age', true),
                    'email' => $this->input->post('email', true),
                    'spon_id' => get_user_id_by_username($this->input->post('spon_id', true)),
                    'last_seen' => date('Y-m-d H:i:s'),
                    'created_on' => date('Y-m-d H:i:s'),
                    'is_approved' => 0,
                    'email_status' => 1,
                    'role' => 'member',
                    'token' => generate_token(),
                    'user_type' => 'registered',
                    'slug' => $username,

                );
                if ($this->authication_model->register($data)) {
                    redirect('registration-compleate/' . $username);
                } else {
                    $this->session->set_flashdata('error', "Unable to add user!");
                    redirect($this->agent->referrer());
                }
            }
        }
    }

    function is_user_exists_by_mobile()
    {
        $mobile =$this->input->post('mobile',true);
        $resp = $this->authication_model->get_user_by_mobile($mobile);
        
        if (!empty($resp)) {
            if($resp->banned==0){
                $count_order = $this->checkOrderLockTime($resp->id);
                if($count_order>0){
                        $data['status'] = 3;
                        echo json_encode($data);
                }else{
                    $data['status']=1;
                    $data['user_data']=$resp;
                    echo json_encode($data);
                }
            }else{
                $data['status'] = 2;
                echo json_encode($data);
            }
        
        } else {
            $data['status'] = 0;
            echo json_encode($data);
        }
    }

    private function checkOrderLockTime($user_id)
    {
        $sql = "SELECT * FROM orders WHERE user_id=" . $user_id;
        $sql .= " AND created_on > '" . $this->dateCalculator(25) . "' AND created_on < '" . date('Y-m-d H:i:s') . "'";
        return $order = $this->db->query($sql)->num_rows();
    }

    private function dateCalculator($days)
    {
        $today = date("Y-m-d H:i:s");
        $expireDate = date('Y-m-d', strtotime('-' . $days . ' days'));
        return $expireDate . " 00:00:00";
    }

    public function reg_thank_you($username)
    {
        $userdata = $this->authication_model->get_user_by_username($username);
        if (empty($userdata)) {
            redirect('register');
        } else {
            $data['title'] = "Registration Compleate";
            $data['description'] = "Registration Compleate" . " - " . $this->app_name;
            $data['keywords'] = "Registration Compleate" . "," . $this->app_name;
            $data['userdata'] = $userdata;
            $this->load->view('auth/_header', $data);
            $this->load->view('auth/thank_you');
            $this->load->view('auth/_footer');
        }
    }




    public function testmMale()
    {
        $user = $this->authication_model->get_user(2);
        if (!empty($user)) {
            $token = $user->token;
            //check token
            if (empty($token)) {
                $token = generate_token();
                $data = array(
                    'token' => $token
                );
                $this->db->where('id', $user->id);
                $this->db->update('users', $data);
            }

            $data1['token'] = $token;
            // $this->load->view('includes/header',$headerdata);
            $this->load->view('email/email_reset_password', $data1);
            // $this->load->view('includes/footer');


            $data = array(
                'subject' => "Reset Password",
                'to' => $user->email,
                'template_path' => "email/email_reset_password",
                'token' => $token
            );





            /* $data['to'] = "debasish.bigpage@gmail.com";
        $data['subject'] = "Testmail";
        $data['subject'] = "Testmail";
        $data['msg'] = "<h1>Testmail body</h1>";


             $this->email_model->send_email($data);*/
        }
    }
}
