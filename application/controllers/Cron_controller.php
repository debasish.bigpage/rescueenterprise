<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Cron_controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
      $accounts = $this->accounts_model->get_all_accounts_cron(['stop_payment'=>0]);
      if(!empty($accounts)){
        foreach ($accounts as $account) {
            $account_id = $account->id;
            $user_id = $account->user_id;
            $old_lavel = $account->mlm_level;
            $old_child = $account->total_child;
            $child_leg_count = $this->checkLegs($account_id);
            if($child_leg_count> $old_child){
              //update leg in accounts table
              $updatearr = [
                'total_child' => $child_leg_count,
              ];
               $this->accounts_model->update($updatearr, $account_id);
              $mlmLavel = $this->mlmLevelSetter($child_leg_count);
              $current_lavel = $mlmLavel['level'];
              $payble_amount = $mlmLavel['total_amount'];
              if($current_lavel>$old_lavel){
                $this->accounts_model->update(['mlm_level'=>$current_lavel], $account_id);
                //payout init
                $payoutArr = [
                  'user_id'=>$user_id,
                  'account_id'=> $account_id,
                  'amount'=> $payble_amount,
                  'description'=>'Comapny reward allotted for upgrading the leavel form leavel-'. $old_lavel.' to level-'. $current_lavel,
                  'created_at'=>date('Y-m-d H:i:s'),
                  'status'=>0,
                ];
                $this->payout_model->add_payout($payoutArr);
              }
          } 
        }
      }
      
     
     
    }

   

    private function checkLegs($id)
    {
        $mysqli = new mysqli($this->db->hostname, $this->db->username, $this->db->password, $this->db->database);
        $mainArray = array();
        $subArray = array();
        array_push($subArray, $id);
        $temp = 0;
        while ($temp != count($subArray)) {
          $sql = "SELECT l1, l2, l3, l4, l5 from network WHERE `user_account_id` = '$subArray[$temp]'";
          $stateq = mysqli_query($mysqli, $sql) or die(mysqli_error($mysqli));
          $row = mysqli_fetch_array($stateq);
          if ($row["l1"] != 0) {
            array_push($subArray, $row["l1"]);
          }
          if ($row["l2"] != 0) {
            array_push($subArray, $row["l2"]);
          }
          if ($row["l3"] != 0) {
            array_push($subArray, $row["l3"]);
          }
          if ($row["l4"] != 0) {
            array_push($subArray, $row["l4"]);
          }
          if ($row["l5"] != 0) {
            array_push($subArray, $row["l5"]);
          }
          array_push($mainArray, $subArray[$temp]);

          $temp++;
        }
    array_shift($mainArray);
    return count($mainArray);
    }

    private function mlmLevelSetter($leg_counter)
    {
            
      $mlmPlans = $this->businessplan_model->get_all_business_plansCron();
      foreach ($mlmPlans as $plan) {
        if($leg_counter>=$plan->leg){
          $returnArr['level']= $plan->level;
          $returnArr['total_amount']= $plan->total;  
          return $returnArr;
          
        }
       
      }
    }

    public function cancel_income()
    {
      $payouts = $this->db->query("SELECT * FROM payout_tracking WHERE 'status'=0 AND created_at > '" . date('Y-m-d ') ."00:00:00". '" AND created_at < "' . date('Y-m-d H:i:s') . "'")->result();
      foreach ($payouts as $payout) {
        $user_id = $payout->user_id;
        $payout_id = $payout->id;
        $order_count = $this->orderTracker($user_id);
        if($order_count<1){
          $this->payout_model->payout_update(['status'=>2],$payout_id);
        }
      }
      
    }

  private function dateCalculator($days)
  {
    $today = date("Y-m-d H:i:s");
    $expireDate = date('Y-m-d', strtotime('-' . $days . ' days'));
    return $expireDate . " 00:00:00";
  }

  private function orderTracker($user_id)
  {
    $sql = "SELECT * FROM orders WHERE user_id=".$user_id ;
    $sql .= " AND created_on > '" . $this->dateCalculator(30) . "' AND created_on < '" . date('Y-m-d H:i:s') . "'";
    return $order = $this->db->query($sql)->num_rows();
  }

}