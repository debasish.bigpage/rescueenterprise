
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member_contoller extends Home_Core_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!auth_check()) {
            redirect('login');
        }
        //member_url
    }

    public function index()
    {
        $data['title'] = "Member Panel";
        $user_id = $this->session->userdata('bp_sess_user_id');
        
        $data['total_order'] = $this->order_model->get_orders_count(['user_id'=>$user_id]);
        $data['total_accounts'] = $this->accounts_model->get_account_count(['user_id' => $user_id]);
        $data['orders'] = $this->order_model->get_orders(['user_id' => $user_id]);
        $data['income'] = $this->transaction_model->getSumIncomeMember(['to_user_id'=>$user_id]);
        $data['expenses'] = $this->transaction_model->getSumExpensesMember(['form_user_id'=>$user_id]);
        $data['pay_pending'] = $this->payout_model->get_all_payouts_count(['user_id' => $user_id,'status' => 0]);
        $data['pay_blocked'] = $this->payout_model->get_all_payouts_count(['user_id' => $user_id,'status' => 2]);
        $this->load->view('user/includes/header', $data);
        $this->load->view('user/index');
        $this->load->view('user/includes/footer');
    }

    /*
    *-------------------------------------------------------------------------------------------------
    * Transactions
    *-------------------------------------------------------------------------------------------------
    */

    public function transactions()
    {
        $data['title'] = "Total Transactions";
        $user_id = $this->session->userdata('bp_sess_user_id');
        $data['transactions'] = $this->transaction_model->get_all_member_trannsactions(['form_user_id' => $user_id, 'to_user_id'=>$user_id]);

        if (empty($data['transactions'])) {
            //redirect($this->agent->referrer());
        }
        $this->load->view('user/includes/header', $data);
        $this->load->view('user/report/transactions', $data);
        $this->load->view('user/includes/footer');
    }

    public function filter_transactions()
    {
        $user_id = $this->session->userdata('bp_sess_user_id');
        $formInp = $this->input->post('form');
        $toInp = $this->input->post('to');
        $form = $formInp . ' 00:00:00';
        $to  = date("Y-m-d H:i:s", strtotime($toInp));
        $where = " 	created_at >= '" . $form . "' AND 	created_at <= '" . $to . "' ORDER BY id DESC";
        $data['title'] = "Total Transactions";
        $sql = "SELECT * FROM transactions WHERE (form_user_id='" . $user_id . "' OR to_user_id='".$user_id."' ) AND ";
        $sql .= $where;
        
        $data['transactions'] = $this->db->query($sql)->result();

        $data['form_date'] = $formInp;
        $data['to_date'] = $toInp;
        $this->load->view('user/includes/header', $data);
        $this->load->view('user/report/filter_transactions', $data);
        $this->load->view('user/includes/footer');
    }

    /*
    *-------------------------------------------------------------------------------------------------
    * MLM SECtion
    *-------------------------------------------------------------------------------------------------
    */    

    public function my_accounts()
    {
        $user_id = $this->session->userdata('bp_sess_user_id');
        $users = $this->accounts_model->get_all_accounts(['user_id' => $user_id]);
        $data['title'] = "My Accounts";
        $data['users'] = $users;
        $this->load->view('user/includes/header', $data);
        $this->load->view('user/report/my_accounts', $data);
        $this->load->view('user/includes/footer');
    }
    public function generology($id)
    {
        $id = clean_number($id);
        $data['parentData'] = $this->accounts_model->get_account_by_id($id);
        $data["childData"] = $this->network_model->getNetworkChild($id);
        $data['title'] = "Member Tree";
        $this->load->view('user/includes/header', $data);
        $this->load->view('user/report/mlm_tree', $data);
        $this->load->view('user/includes/footer');
    }

     /*
    *-------------------------------------------------------------------------------------------------
    * Payout Section
    *-------------------------------------------------------------------------------------------------
    */ 
    public function received_payout()
    {
        $data['title'] = "My Received Reward";
        $user_id = $this->session->userdata('bp_sess_user_id');
        $data['transactions'] = $this->payout_model->get_all_payouts(['user_id'=>$user_id,'status'=>1]);
        $this->load->view('user/includes/header', $data);
        $this->load->view('user/report/payout', $data);
        $this->load->view('user/includes/footer');
    }

    public function blocked_payout()
    {
        $data['title'] = "My Blocked Reward";
        $user_id = $this->session->userdata('bp_sess_user_id');
        $data['transactions'] = $this->payout_model->get_all_payouts(['user_id' => $user_id, 'status' => 2]);
        $this->load->view('user/includes/header', $data);
        $this->load->view('user/report/payout', $data);
        $this->load->view('user/includes/footer');
    }

    public function pending_payout()
    {
        $data['title'] = "My Pending Reward";
        $user_id = $this->session->userdata('bp_sess_user_id');
        $data['transactions'] = $this->payout_model->get_all_payouts(['user_id' => $user_id, 'status' => 0]);
        $this->load->view('user/includes/header', $data);
        $this->load->view('user/report/payout', $data);
        $this->load->view('user/includes/footer');
    }
}
