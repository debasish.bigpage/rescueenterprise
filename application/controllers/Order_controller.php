<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Order_controller extends Admin_Core_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!auth_check()) {
            redirect('login');
        }
      
    }

    public function index()
    {
        $data['title'] = "Orders";
        if($this->session->userdata('bp_sess_user_role')=="admin"){
            $data['orders'] = $this->order_model->get_orders();
            $this->load->view('admin/includes/header', $data);
            $this->load->view('admin/order/order', $data);
            $this->load->view('admin/includes/footer');
        } elseif ($this->session->userdata('bp_sess_user_role') == "staff") {
            $data['orders'] = $this->order_model->get_orders(['create_by'=>$this->session->userdata('bp_sess_user_id')]);
            $this->load->view('staff/includes/header', $data);
            $this->load->view('staff/order/order', $data);
            $this->load->view('staff/includes/footer');
        }else{
            $data['orders'] = $this->order_model->get_orders(['user_id' => $this->session->userdata('bp_sess_user_id')]);
            $this->load->view('user/includes/header', $data);
            $this->load->view('user/order/order', $data);
            $this->load->view('user/includes/footer');
        }
        
    }

    public function add_order()
    {
        if (!is_access_user()) {
            redirect('dashboard');
        }
        $data['title'] = "Add Order";
        if ($this->session->userdata('bp_sess_user_role') == "admin") {
            $data['products'] = $this->product_admin_model->get_products();
            $this->load->view('admin/includes/header', $data);
            $this->load->view('admin/order/add_new', $data);
            $this->load->view('admin/includes/footer');
        } elseif ($this->session->userdata('bp_sess_user_role') == "staff") {
            $data['products'] = $this->product_admin_model->get_products();
            $this->load->view('staff/includes/header', $data);
            $this->load->view('staff/order/add_new', $data);
            $this->load->view('staff/includes/footer');
        }
    }

    public function create_order()
    {
        if (!is_access_user()) {
            redirect('dashboard');
        }
        $this->form_validation->set_rules('full_name', "Name", 'required|xss_clean|min_length[4]|max_length[250]');
        $this->form_validation->set_rules('mobile', "Mobile Number", 'required|xss_clean|min_length[10]|max_length[10]');
        $this->form_validation->set_rules('email', "Email", 'xss_clean|min_length[5]|valid_email');
        $this->form_validation->set_rules('address', "Address", 'required|xss_clean|min_length[5]');
        $this->form_validation->set_rules('state', "State", 'required|min_length[4]|max_length[255]');
        $this->form_validation->set_rules('pin', "Pin Code", 'required|min_length[6]|max_length[6]');
       
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('error', validation_errors());

            redirect($this->agent->referrer());
        } else {
            $this->load->library('bcrypt');
            $ntotal = $this->input->post('ntotal');
            $mobile = $this->input->post('mobile');
            $full_name = $this->input->post('full_name');
            $email = $this->input->post('email');
            $address = $this->input->post('address');
            $state = $this->input->post('state');
            $pin = $this->input->post('pin');
            $is_user_exists = $this->input->post('is_user_exists');
            if($ntotal<=0){
                $this->session->set_flashdata('error', "You have no product added in the cart");
                redirect($this->agent->referrer());
            }
            //add user if not exists
            if($is_user_exists==0){
                $mem_count = get_totalUserCount();
                $month = chr(64 + date('m'));
                $username = 'REM' . date('y') . $month . date('d') . '0' . ($mem_count + 1);
                $userInsertArr = array(
                    'username' => $username,
                    'full_name' => $full_name,
                    'password' => $this->bcrypt->hash_password($mobile),
                    'mobile' => $mobile,
                    'email' => $email,
                    'address' => $address,
                    'state' => $state,
                    'pin' => $pin,
                    'last_seen' => date('Y-m-d H:i:s'),
                    'created_on' => date('Y-m-d H:i:s'),
                    'email_status' => 1,
                    'role' => 'member',
                    'token' => generate_token(),
                    'user_type' => 'registered',
                    'slug' => $username,
                    'created_by' => $this->session->userdata('bp_sess_user_id'),
                );
                $this->authication_model->register($userInsertArr);
                $cif = $this->db->insert_id();
                //create New Account Agist CIF
                $account_id = $this->createNewAccount($cif);
            }else{
                $user = $this->authication_model->get_user_by_mobile($mobile);
                $cif = $user->id;
                $username = $user->username;
                $account_id = $this->createNewAccount($user->id);

            }
            //add user to netowk Table
            $networkUserinsArr  = array(
                'user_id'=>$cif,
                'user_account_id'=> $account_id,
            );
            $this->network_model->add_to_network($networkUserinsArr);

            // product cart
            $postdata = $_POST;
            $i = 0;
            $productsArr = array();
            foreach ($postdata['qty']  as $single_qty) {
                $product_qty =  trim($single_qty);
                $product_id =  trim($postdata['product_id'][$i]);
                $product_unit_price =  trim($postdata['rate'][$i]);
                $i++;
                if (!empty($product_qty)&& $product_qty>0) {
                    // insert into multi dimantion array
                    $subdataArray['product_id'] = $product_id;
                    $subdataArray['product_unit_price'] = $product_unit_price;
                    $subdataArray['product_qty'] = $product_qty;
                    $subdataArray['product_total_price'] = $product_unit_price*$product_qty;
                    array_push($productsArr, $subdataArray);
            }
        }
            // genarate order
            $orderInsertArr = array(
                'user_id'=>$cif,
                'account_id'=> $account_id,
                'create_by'=> $this->session->userdata('bp_sess_user_id'),
                'price_total'=> $ntotal,
                'created_on' => date('Y-m-d H:i:s'),
                'products'=> $productsArr,
            );
            $this->setToNetwork($account_id);
            if($this->order_model->add_order($orderInsertArr)){
                redirect('order-compleate/'.$username);
            } else {
                $this->session->set_flashdata('error', "Unable to create order");
                redirect($this->agent->referrer());
            }
    }
}

    public function createNewAccount($cif)
    {
        $mem_count = get_totalAccountCount();
        $month = chr(64 + date('m'));
        $username = 'REA' . date('y') . $month . date('d') . '0' . ($mem_count + 1);
        $accoutCreationArr = array(
                    'user_id'=> $cif,
                    'account_id'=> $username,
                    'mlm_level'=> 0,
                    'created_on' => date('Y-m-d H:i:s'),
                    'created_by'=> $this->session->userdata('bp_sess_user_id'),
                    'stop_payment'=> 0,
                );
        $this->accounts_model->create_account($accoutCreationArr);
        return $this->db->insert_id();
    }


    public function order_compleate($username)
    {
        $userdata = $this->authication_model->get_user_by_username($username);
        if (empty($userdata)) {
            redirect('register');
        } else {
            $data['title'] = "Order Compleate";
            $data['description'] = "Order Compleate" . " - " . $this->app_name;
            $data['keywords'] = "Order Compleate" . "," . $this->app_name;
            $data['userdata'] = $userdata;
            $data['accountDetails'] = $this->accounts_model->get_account_data_by_cif($userdata->id);
            $this->load->view('auth/_header', $data);
            $this->load->view('auth/thank_you');
            $this->load->view('auth/_footer');
        }
    }

    public function view_order_details($orderId)
    {
        $data['title'] = "View Order";
        if ($this->session->userdata('bp_sess_user_role') == "admin") {
            $data['order'] = $this->order_model->get_order($orderId);
            $data['customer_data'] = $this->authication_model->get_user($data['order']->user_id);
            $data['products'] = $this->order_model->get_order_products($orderId);
            $this->load->view('admin/includes/header', $data);
            $this->load->view('admin/order/view_order', $data);
            $this->load->view('admin/includes/footer');
        } elseif ($this->session->userdata('bp_sess_user_role') == "staff") {
            $data['order'] = $this->order_model->get_order($orderId);
            $data['customer_data'] = $this->authication_model->get_user($data['order']->user_id);
            $data['products'] = $this->order_model->get_order_products($orderId);
            $this->load->view('staff/includes/header', $data);
            $this->load->view('staff/order/view_order', $data);
            $this->load->view('staff/includes/footer');
        } else {
            $data['order'] = $this->order_model->get_order($orderId);
            $data['customer_data'] = $this->authication_model->get_user($data['order']->user_id);
            $data['products'] = $this->order_model->get_order_products($orderId);
            $this->load->view('user/includes/header', $data);
            $this->load->view('user/order/view_order', $data);
            $this->load->view('user/includes/footer');
        }
    }

    /* --------------------------------------------------
    *
    *NETWORK SISTEM / MLM
    * ---------------------------------------------------
    */

    public function setToNetwork($account_id)
    {
        $parrentUser = $this->accounts_model->getSingleUserList(array('seat_full' => 0));
        $userdata = $this->accounts_model->get_account_by_id($account_id);
        $legFinder = $this->checkLegisFree($parrentUser->id);
        if ($legFinder['status'] == 1) {
            $newtworkLegUpdateArr = array(
                $legFinder['pos'] => $userdata->id,
            );
            $newtworkUpdateArr = array(
                'parrent_id' => $parrentUser->user_id,
                'parrent_account_id' => $parrentUser->id,
            );
            $userArray = array(
                'is_set_leg' => 1,
            );
            $this->network_model->update_network($newtworkLegUpdateArr, $legFinder['row_id']);
            $this->network_model->update_network_parrent($newtworkUpdateArr, $userdata->id);
            $this->accounts_model->update($userArray, $userdata->id);
            $this->checkSetCompleate();
        }
    }

    public function checkSetCompleate()
    {
        $networkUsers = $this->network_model->getNetworkUserList(array('is_my_set_compleate'=>0));
        foreach ($networkUsers as $networkUser) {
            if($networkUser->l1>0 && $networkUser->l2>0 && $networkUser->l3>0 && $networkUser->l4>0 && $networkUser->l5>0 ){    
                $updateArr = array(
                    'is_my_set_compleate'=>1,
                );
                $updateAccArr = array(
                    'seat_full'=>1
                );
                $this->network_model->update_network($updateArr, $networkUser->id);
                $this->accounts_model->update($updateAccArr, $networkUser->user_account_id);
            }
        }
    }



    public function checkLegisFree($parrent_id)
    {
        $returnData = array();
        $parrentuserData = $this->network_model->checkLegisFreeByparrentAccountID($parrent_id);
        if (($parrentuserData->is_my_set_compleate == 0)) {
            //check the leg 
            if ($parrentuserData->l1 == 0) {
                $returnData['status'] = 1;
                $returnData['pos'] = 'l1';
                $returnData['user_id'] = $parrentuserData->user_id;
                $returnData['parrent_id'] = $parrentuserData->parrent_id;
                $returnData['user_account_id'] = $parrentuserData->user_account_id;
                $returnData['parrent_account_id'] = $parrent_id;
                $returnData['row_id'] = $parrentuserData->id;
            } elseif ($parrentuserData->l2 == 0) {
                $returnData['status'] = 1;
                $returnData['pos'] = 'l2';
                $returnData['user_id'] = $parrentuserData->user_id;
                $returnData['parrent_id'] = $parrentuserData->parrent_id;
                $returnData['user_account_id'] = $parrentuserData->user_account_id;
                $returnData['parrent_account_id'] = $parrent_id;
                $returnData['row_id'] = $parrentuserData->id;
            } elseif ($parrentuserData->l3 == 0) {
                $returnData['status'] = 1;
                $returnData['pos'] = 'l3';
                $returnData['user_id'] = $parrentuserData->user_id;
                $returnData['parrent_id'] = $parrentuserData->parrent_id;
                $returnData['user_account_id'] = $parrentuserData->user_account_id;
                $returnData['parrent_account_id'] = $parrent_id;
                $returnData['row_id'] = $parrentuserData->id;
            } elseif ($parrentuserData->l4 == 0) {
                $returnData['status'] = 1;
                $returnData['pos'] = 'l4';
                $returnData['user_id'] = $parrentuserData->user_id;
                $returnData['parrent_id'] = $parrentuserData->parrent_id;
                $returnData['user_account_id'] = $parrentuserData->user_account_id;
                $returnData['parrent_account_id'] = $parrent_id;
                $returnData['row_id'] = $parrentuserData->id;
            } elseif ($parrentuserData->l5 == 0) {
                $returnData['status'] = 1;
                $returnData['pos'] = 'l5';
                $returnData['user_id'] = $parrentuserData->user_id;
                $returnData['parrent_id'] = $parrentuserData->parrent_id;
                $returnData['user_account_id'] = $parrentuserData->user_account_id;
                $returnData['parrent_account_id'] = $parrent_id;
                $returnData['row_id'] = $parrentuserData->id;
            
            }else {
                $returnData['status'] = 0;
            }
        } else {
            $returnData['status'] = 2;
        }
        return $returnData;
    }


   

}
