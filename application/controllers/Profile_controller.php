
<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Profile_controller extends Home_Core_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!auth_check()) {
            redirect('login');
            exit;
        }
        
    }
    public function profile()
    {
        $user= user();
        if ($user->role=="admin") {
            $data['user_data'] =$user;
            $data['title'] = "Profile";
            $this->load->view('admin/includes/header', $data);
            $this->load->view('admin/profile/profile', $data);
            $this->load->view('admin/includes/footer');
        }elseif($user->role=="member"){
            $data['user_data'] = $user;
            $data['title'] = "Profile";
            $this->load->view('user/includes/header', $data);
            $this->load->view('user/profile/profile', $data);
            $this->load->view('user/includes/footer');
        }elseif($user->role=="staff"){
            $data['user_data'] = $user;
            $data['title'] = "Profile";
            $this->load->view('staff/includes/header', $data);
            $this->load->view('staff/profile/profile', $data);
            $this->load->view('staff/includes/footer');
        }
    }
    public function save_profile()
    {
        $this->load->library('upload');
        $oldIng = $this->input->post('oldIng');
        $oldpan = $this->input->post('oldpan');
       
        $this->form_validation->set_rules('sex', "Sex", 'required|xss_clean');
       
        $this->form_validation->set_rules('address', "Address", 'required|xss_clean');
        $this->form_validation->set_rules('state', "State", 'required|xss_clean');
        $this->form_validation->set_rules('pin', "PIN Code", 'required|xss_clean|min_length[6]|max_length[6]|numeric');
        
        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->agent->referrer());
        } else {
            $data = array(
                'father_name' => $this->input->post('father_name', true),
                'full_name' => $this->input->post('full_name', true),
                'sex' => $this->input->post('sex', true),
                'occupation' => $this->input->post('occupation', true),
                'address' => $this->input->post('address', true),
                'state' => $this->input->post('state', true),
                'pin' => $this->input->post('pin', true),
                'bank_name' => $this->input->post('bank_name', true),
                'acno' => $this->input->post('acno', true),
                'bank_ifsc' => $this->input->post('bank_ifsc', true),
                'nomine_name' => $this->input->post('nomine_name', true),
                'age' => $this->input->post('age', true),
                'pan' => $this->input->post('pan', true),
                'mobile' => $this->input->post('mobile', true),
                'dob' => $this->input->post('dob', true),
            );
            if ($_FILES['avatar']['name'] != '') {
                $path = FCPATH . 'uploads/member-doc/';
                if (!is_dir($path)) {
                    mkdir($path, 0755, TRUE);
                }
                $config_prof['encrypt_name']  = TRUE;
                $config_prof['upload_path']   = $path;
                $config_prof['allowed_types'] = 'png|jpeg|jpg';
                $config['max_size']           = '5000';
                $config_prof['overwrite']     = true;
                $this->upload->initialize($config_prof);
                if ($this->upload->do_upload('avatar')) {
                    $file_data = $this->upload->data();
                    $this->resizeImage($file_data);
                    $data["avatar"] =  $file_data['file_name'];
                    if (!empty($oldIng)) {
                        delete_file_from_server('uploads/member-doc/' . $oldIng);
                    }
                } else {
                    $this->session->set_flashdata('error',  $this->upload->display_errors());
                    redirect($this->agent->referrer());
                }
            }
            if ($_FILES['pan_img']['name'] != '') {
                $path = FCPATH . 'uploads/member-doc/';
                if (!is_dir($path)) {
                    mkdir($path, 0755, TRUE);
                }
                $config_prof['encrypt_name']  = TRUE;
                $config_prof['upload_path']   = $path;
                $config_prof['allowed_types'] = 'png|jpeg|jpg';
                $config['max_size']           = '5000';
                $config_prof['overwrite']     = true;
                $this->upload->initialize($config_prof);
                if ($this->upload->do_upload('pan_img')) {
                    $file_data = $this->upload->data();
                    $this->resizeImage($file_data);
                    $data["pan_img"] =  $file_data['file_name'];
                    if (!empty($oldpan)) {
                        delete_file_from_server('uploads/member-doc/' . $oldpan);
                    }
                } else {
                    $this->session->set_flashdata('error',  $this->upload->display_errors());
                    redirect($this->agent->referrer());
                }
            }
         
            if ($this->authication_model->updateUsers($data, $this->session->userdata('bp_sess_user_id'))) {
                $this->session->set_flashdata('success', "Profile Updated");
                redirect($this->agent->referrer());
            } else {

                $this->session->set_flashdata('error', "Unable update profile!");
                redirect($this->agent->referrer());
            }
        }
    }

    public function resizeImage($file_data)
    {
        $this->load->library('image_lib');
        $path = FCPATH . "/uploads/member-doc/";
        $height = 480;
        $width = 640;
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $file_data["raw_name"] . $file_data['file_ext'];
        $config['new_image'] = $path . $file_data["raw_name"] . $file_data['file_ext'];
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = "60";
        $config['width']         = $width;
        $config['height']       = $height;
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    public function view_profile($id)
    {
        $id = clean_number($id);
        $data['user_data']=$this->authication_model->get_user($id);
        $data['sub_account']=$this->accounts_model->get_all_accounts(['user_id'=>$id]);
        $role =  $this->session->userdata('bp_sess_user_role');
        if ($role == "admin") {
            $data['title'] = "Member Details";
            $this->load->view('admin/includes/header', $data);
            $this->load->view('user_account_details', $data);
            $this->load->view('admin/includes/footer');
        } elseif ($role == "member") {
            $data['title'] = "Member Details";
            $this->load->view('user/includes/header', $data);
            $this->load->view('user_account_details', $data);
            $this->load->view('user/includes/footer');
        } elseif ($role == "staff") {
            $data['title'] = "Member Details";
            $this->load->view('staff/includes/header', $data);
            $this->load->view('user_account_details', $data);
            $this->load->view('staff/includes/footer');
        }
    }

}