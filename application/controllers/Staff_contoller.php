
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Staff_contoller extends Home_Core_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!auth_check()) {
            redirect('login');
        }
        if (!is_access_user()) {
            redirect('dashboard');
        }
    }

    public function index()
    {
        $data['title'] = "Staff Panel";
        $user_id = $this->session->userdata('bp_sess_user_id');
        $data['total_member'] = $this->authication_model->get_cond_users_count(['created_by'=>$user_id]);
        $data['total_order'] = $this->order_model->get_orders_count(['create_by'=>$user_id]);
        $data['total_accounts'] = $this->accounts_model->get_account_count(['created_by'=>$user_id]);
        $data['orders'] = $this->order_model->get_orders(['create_by' => $user_id]);
        $this->load->view('staff/includes/header', $data);
        $this->load->view('staff/index');
        $this->load->view('staff/includes/footer');
    }

    /*
    *-------------------------------------------------------------------------------------------------
    * Transactions
    *-------------------------------------------------------------------------------------------------
    */

    public function transactions()
    {
        $data['title'] = "Total Transactions";
        $data['transactions'] = $this->transaction_model->get_all_trannsactions(['user_by'=> $this->session->userdata('bp_sess_user_id')]);
        
        if (empty($data['transactions'])) {
            redirect($this->agent->referrer());
        }
        $this->load->view('staff/includes/header', $data);
        $this->load->view('staff/report/transactions', $data);
        $this->load->view('staff/includes/footer');
    }

    public function filter_transactions()
    {
        $formInp = $this->input->post('form');
        $toInp = $this->input->post('to');
        $form = $formInp . ' 00:00:00';
        $to  = date("Y-m-d H:i:s", strtotime($toInp));
        $where = " 	created_at >= '" . $form . "' AND 	created_at <= '" . $to . "' ORDER BY id DESC";
        $data['title'] = "Total Transactions";
        $sql = "SELECT * FROM transactions WHERE user_by='". $this->session->userdata('bp_sess_user_id')."' AND " ;
        $sql .= $where;
        $data['transactions'] = $this->db->query($sql)->result();
       
        $data['form_date'] = $formInp;
        $data['to_date'] = $toInp;
        $this->load->view('staff/includes/header', $data);
        $this->load->view('staff/report/filter_transactions', $data);
        $this->load->view('staff/includes/footer');
    }

}