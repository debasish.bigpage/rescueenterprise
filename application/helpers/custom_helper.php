<?php
/*
 * Custom Helpers
 *
 */
if (strpos($_SERVER['REQUEST_URI'], '/index.php') !== false) {
    $ci = &get_instance();
    $ci->load->helper('url');
    redirect(current_url());
    exit();
}
//check auth
if (!function_exists('auth_check')) {
    function auth_check()
    {
        // Get a reference to the controller object
        $ci = &get_instance();
        return $ci->authication_model->is_logged_in();
    }
}

//generate token
if (!function_exists('generate_token')) {
    function generate_token()
    {
        $token = uniqid("", TRUE);
        $token = str_replace(".", "-", $token);
        return $token . "-" . rand(10000000, 99999999);
    }
}

//remove special characters
if (!function_exists('remove_special_characters')) {
    function remove_special_characters($str)
    {
        $ci = &get_instance();
        $str = str_replace('#', '', $str);
        $str = str_replace(';', '', $str);
        $str = str_replace('!', '', $str);
        $str = str_replace('"', '', $str);
        $str = str_replace('$', '', $str);
        $str = str_replace('%', '', $str);
        $str = str_replace("'", '', $str);
        $str = str_replace('(', '', $str);
        $str = str_replace(')', '', $str);
        $str = str_replace('*', '', $str);
        $str = str_replace('+', '', $str);
        $str = str_replace('/', '', $str);
        $str = str_replace('\'', '', $str);
        $str = str_replace('<', '', $str);
        $str = str_replace('>', '', $str);
        $str = str_replace('=', '', $str);
        $str = str_replace('?', '', $str);
        $str = str_replace('[', '', $str);
        $str = str_replace(']', '', $str);
        $str = str_replace('\\', '', $str);
        $str = str_replace('^', '', $str);
        $str = str_replace('`', '', $str);
        $str = str_replace('{', '', $str);
        $str = str_replace('}', '', $str);
        $str = str_replace('|', '', $str);
        $str = str_replace('~', '', $str);
        $str = mysqli_real_escape_string($ci->db->conn_id, $str);
        return $str;
    }
}
//remove forbidden characters
if (!function_exists('remove_forbidden_characters')) {
    function remove_forbidden_characters($str)
    {
        $str = str_replace(';', '', $str);
        $str = str_replace('"', '', $str);
        $str = str_replace('$', '', $str);
        $str = str_replace('%', '', $str);
        $str = str_replace('*', '', $str);
        $str = str_replace('/', '', $str);
        $str = str_replace('\'', '', $str);
        $str = str_replace('<', '', $str);
        $str = str_replace('>', '', $str);
        $str = str_replace('=', '', $str);
        $str = str_replace('?', '', $str);
        $str = str_replace('[', '', $str);
        $str = str_replace(']', '', $str);
        $str = str_replace('\\', '', $str);
        $str = str_replace('^', '', $str);
        $str = str_replace('`', '', $str);
        $str = str_replace('{', '', $str);
        $str = str_replace('}', '', $str);
        $str = str_replace('|', '', $str);
        $str = str_replace('~', '', $str);
        return $str;
    }
}
//clean number
if (!function_exists('clean_number')) {
    function clean_number($num)
    {
        $ci = &get_instance();
        $num = trim($num);
        $num = $ci->security->xss_clean($num);
        $num = intval($num);
        return $num;
    }
}

//clean string
if (!function_exists('clean_str')) {
    function clean_str($str)
    {
        $ci = &get_instance();
        $str = $ci->security->xss_clean($str);
        $str = remove_special_characters($str, false);
        return $str;
    }
}
//is admin
if (!function_exists('is_admin')) {
    function is_admin()
    {
        // Get a reference to the controller object
        $ci = &get_instance();
        return $ci->authication_model->is_admin();
    }
}

//is admin
if (!function_exists('is_access_user')) {
    function is_access_user()
    {
        // Get a reference to the controller object
        $ci = &get_instance();
        return $ci->authication_model->is_access_user();
    }
}

//////////////////////////
//post method
if (!function_exists('post_method')) {
    function post_method()
    {
        $ci = &get_instance();
        if ($ci->input->method(FALSE) != 'post') {
            exit();
        }
    }
}

//get logged user
if (!function_exists('user')) {
    function user()
    {
        // Get a reference to the controller object
        $ci = &get_instance();
        $user = $ci->authication_model->get_logged_user();
        if (empty($user)) {
            $ci->authication_model->logout();
        } else {
            return $user;
        }
    }
}

//get ci core constructor
// if (!function_exists('get_ci_core_construct')) {
// 	function get_ci_core_construct()
// 	{
// 		return @ci_core_construct();
// 	}
// }
if (!function_exists('time_ago')) {
    function time_ago($timestamp)
    {
        $time_ago = strtotime($timestamp);
        $current_time = time();
        $time_difference = $current_time - $time_ago;
        $seconds = $time_difference;
        $minutes = round($seconds / 60);           // value 60 is seconds
        $hours = round($seconds / 3600);           //value 3600 is 60 minutes * 60 sec
        $days = round($seconds / 86400);          //86400 = 24 * 60 * 60;
        $weeks = round($seconds / 604800);          // 7*24*60*60;
        $months = round($seconds / 2629440);     //((365+365+365+365+366)/5/12)*24*60*60
        $years = round($seconds / 31553280);     //(365+365+365+365+366)/5 * 24 * 60 * 60
        if ($seconds <= 60) {
            return "Just now";
        } else if ($minutes <= 60) {
            if ($minutes == 1) {
                return "1 " . "Minute ago";
            } else {
                return "$minutes " . "Minutes ago";
            }
        } else if ($hours <= 24) {
            if ($hours == 1) {
                return "1 " . "hour ago";
            } else {
                return "$hours " . "Hours ago";
            }
        } else if ($days <= 30) {
            if ($days == 1) {
                return "1 " . "day ago";
            } else {
                return "$days " . "Days ago";
            }
        } else if ($months <= 12) {
            if ($months == 1) {
                return "1 " . "month ago";
            } else {
                return "$months " . "Months ago";
            }
        } else {
            if ($years == 1) {
                return "1 " . "year ago";
            } else {
                return "$years " . "Years ago";
            }
        }
    }
}
//get user by id
if (!function_exists('get_user')) {
    function get_user($user_id)
    {
        // Get a reference to the controller object
        $ci = &get_instance();
        return $ci->authication_model->get_user($user_id);
    }
}
//get settings
if (!function_exists('get_settings')) {
    function get_settings()
    {
        $ci = &get_instance();
        $ci->load->model('settings_model');
        return $ci->settings_model->get_settings();
    }
}

//delete file from server
if (!function_exists('delete_file_from_server')) {
    function delete_file_from_server($path)
    {
        $full_path = FCPATH . $path;
        if (strlen($path) > 15 && file_exists($full_path)) {
            @unlink($full_path);
        }
    }
}

//get general settings
if (!function_exists('get_general_settings')) {
    function get_general_settings()
    {
        $ci = &get_instance();
        $ci->load->model('settings_model');
        return $ci->settings_model->get_general_settings();
    }
}

if (!function_exists('formatted_date')) {
    function formatted_date($timestamp)
    {
        return date("Y-m-d / H:i", strtotime($timestamp));
    }
}
//get method
if (!function_exists('get_method')) {
    function get_method()
    {
        $ci = &get_instance();
        if ($ci->input->method(FALSE) != 'get') {
            exit();
        }
    }
}

//get method
if (!function_exists('get_totalUserCount')) {
    function get_totalUserCount()
    {
        $ci = &get_instance();
        return $ci->db->query("select id from users")->num_rows();
    }
}

//get method
if (!function_exists('get_totalAccountCount')) {
    function get_totalAccountCount()
    {
        $ci = &get_instance();
        return $ci->db->query("select id from user_accounts")->num_rows();
    }
}
//get recaptcha
if (!function_exists('generate_recaptcha')) {
    function generate_recaptcha()
    {
        $ci = &get_instance();
        if ($ci->recaptcha_status) {
            $ci->load->library('recaptcha');
            echo '<div class="form-group">';
            echo $ci->recaptcha->getWidget();
            echo $ci->recaptcha->getScriptTag();
            echo ' </div>';
        }
    }
}

//admin url
if (!function_exists('admin_url')) {
    function admin_url()
    {
        $ci = &get_instance();
        return base_url() . 'admin/';
    }
}

if (!function_exists('staff_url')) {
    function staff_url()
    {
        $ci = &get_instance();
        return base_url() . 'staff-portal/';
    }
}

if (!function_exists('member_url')) {
    function member_url()
    {
        $ci = &get_instance();
        return base_url() . 'member-portal/';
    }
}

//get user by id
if (!function_exists('get_user_id_by_username')) {
    function get_user_id_by_username($user_id)
    {
        // Get a reference to the controller object
        $ci = &get_instance();
        $resp =  $ci->authication_model->get_user_by_username($user_id);
        return $resp->id;
    }
}

if (!function_exists('getFormatedDate')) {
    function getFormatedDate($date)
    {
        $ret = date('M d, Y', strtotime($date));
        return $ret;
    }
}

//get user by id
if (!function_exists('getUsernameById')) {
    function getUsernameById($user_id)
    {
        // Get a reference to the controller object
        if (!empty($user_id) && $user_id > 0) {
            $ci = &get_instance();
            $resp =  get_user($user_id);
            return $resp->username;
        }else{
            return "NA";
        }
    }
}

//get user accountdata By Account ID
if (!function_exists('getAccountDataByAcID')) {
    function getAccountDataByAcID($ac_id)
    {
        // Get a reference to the controller object
        $ci = &get_instance();
        $x =  $ci->accounts_model->get_account_by_id($ac_id);
        if (!empty($x)) {
            return $x;
        }else{
            return false;
        }
    }
}




if (!function_exists('in_array_any')) {
    function in_array_any($needles, $haystack)
    {
        return empty(array_intersect($needles, $haystack));
    }
}

//get user by id
if (!function_exists('get_user_by_account_id')) {
    function get_user_by_account_id($user_id)
    {
        // Get a reference to the controller object
        $ci = &get_instance();
        return $ci->accounts_model->get_account_by_id($user_id);
    }
}

if (!function_exists('get_product_by_id')) {
    function get_product_by_id($product_id)
    {
        // Get a reference to the controller object
        $ci = &get_instance();
        return $ci->product_admin_model->get_product($product_id);
    }
}

if (!function_exists('get_subAc_count_by_cif')) {
    function get_subAc_count_by_cif($cif)
    {
        // Get a reference to the controller object
        $ci = &get_instance();
        return $ci->accounts_model->get_account_count_by_cif($cif);
    }
}

if (!function_exists('totalOrderByCif')) {
    function totalOrderByCif($cif)
    {
        // Get a reference to the controller object
        $ci = &get_instance();
        return $ci->order_model->get_orders_count_by_cif($cif);
    }
}


