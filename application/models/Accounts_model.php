<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Accounts_model extends CI_Model
{
   
    public function get_all_accounts($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->order_by('id','DESC');
        $query = $this->db->get('user_accounts');
        return $query->result();
    }

    public function get_all_accounts_cron($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        
        $query = $this->db->get('user_accounts');
        return $query->result();
    }

    public function getSingleUserList($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->order_by("id");
        $this->db->limit(1);
        $query = $this->db->get('user_accounts');
        return $query->row();
    }

    public function create_account($data)
    {
        return $this->db->insert('user_accounts', $data);
    }

    //delete contact message
    public function delete_account($id)
    {
        $id = clean_number($id);
        $project = $this->get_account_by_id($id);
        if (!empty($project)) {
            delete_file_from_server('uploads/reward/' . $project->image);
            $this->db->where('id', $id);
            return $this->db->delete('user_accounts');
        } else {
            return false;
        }
    }

    //get project by id
    public function get_account_by_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('user_accounts');
        return $query->row();
    }

    public function get_account_by_accountId($id)
    {
        $this->db->where('account_id', $id);
        $query = $this->db->get('user_accounts');
        return $query->row();
    }

    public function update($data,$id)
    {
            $this->db->where('id', $id);
            return $this->db->update('user_accounts', $data);
    }

    public function get_account_data_by_cif($id)
    {
        $this->db->where('user_id', $id);
        $this->db->order_by('id','DESC');
        $this->db->limit(1);
        $query = $this->db->get('user_accounts');
        return $query->row();
    }

    public function get_account_count_by_cif($id)
    {
        
        $this->db->where('user_id', $id);
        $query = $this->db->get('user_accounts');
        return $query->num_rows();
    }

    public function get_account_count($where='')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get('user_accounts');
        return $query->num_rows();
    }

    

}
