<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Businessplan_model extends CI_Model
{
    protected function input_values()
    {
        $data = array(
            'level' => $this->input->post('level', true),
            'leg' => $this->input->post('leg', true),
            'amount' => $this->input->post('amount', true),
        );
        return $data;
    }

    public function get_all_business_plans($where = '')
    {
        if (!empty($where)) {
            $this->bp->where($where);
        }
        $query = $this->db->get('business_settings');
        return $query->result();
    }
    public function get_all_business_plansCron($where = '')
    {
        if (!empty($where)) {
            $this->bp->where($where);
        }
        $this->db->order_by('id','DESC');
        $query = $this->db->get('business_settings');
        return $query->result();
    }

    public function add_business_plan()
    {
        $this->load->library('upload');
        $data = $this->input_values();
        $data['total']= $data['leg'] * $data['amount'];
        if ($_FILES['image']['name'] != '') {
            $path = FCPATH . 'uploads/reward/';
            if (!is_dir($path)) {
                mkdir($path, 0755, TRUE);
            }
            $config_prof['encrypt_name']  = TRUE;
            $config_prof['upload_path']   = $path;
            $config_prof['allowed_types'] = 'png|jpeg|jpg';
            $config['max_size']           = '5000';
            $config_prof['overwrite']     = true;
            $this->upload->initialize($config_prof);
            if ($this->upload->do_upload('image')) {
                $file_data = $this->upload->data();
                $this->resizeImage($file_data);
                $data["image"] =  $file_data['file_name'];
            } else {
                return false;
            }
        }
        return $this->db->insert('business_settings', $data);
    }

    //delete contact message
    public function delete_business_plan($id)
    {
        $id = clean_number($id);
        $project = $this->get_business_plan_by_id($id);
        if(!empty($project)){
            delete_file_from_server('uploads/reward/' . $project->image);
        $this->db->where('id', $id);
        return $this->db->delete('business_settings');
        }else{
            return false;
        }
    }

    //get project by id
    public function get_business_plan_by_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('business_settings');
        return $query->row();
    }

    public function update($id)
    {
        //set values
        $oldIng = $this->input->post('old_img');
        $this->load->library('upload');
        $data = $this->input_values();
        $data['total'] = $data['leg'] * $data['amount'];
        if ($_FILES['image']['name'] != '') {
            $path = FCPATH . 'uploads/reward/';
            if (!is_dir($path)) {
                mkdir($path, 0755, TRUE);
            }
            $config_prof['encrypt_name']  = TRUE;
            $config_prof['upload_path']   = $path;
            $config_prof['allowed_types'] = 'png|jpeg|jpg';
            $config['max_size']           = '5000';
            $config_prof['overwrite']     = true;
            $this->upload->initialize($config_prof);
            if ($this->upload->do_upload('image')) {
                $file_data = $this->upload->data();
                $this->resizeImage($file_data);
                $data["image"] =  $file_data['file_name'];
                if (!empty($oldIng)) {
                    delete_file_from_server('uploads/reward/' . $oldIng);
                }
            } else {
                $this->session->set_flashdata('error',  $this->upload->display_errors());
                redirect($this->agent->referrer());
            }
        }
        $project = $this->get_business_plan_by_id($id);
        if (!empty($project)) {
            $this->db->where('id', $id);
            return $this->db->update('business_settings', $data);
        }
        return false;
    }

    public function resizeImage($file_data)
    {
        $this->load->library('image_lib');
        $path = FCPATH . "/uploads/amount/";
        $height = 450;
        $width = 750;
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $file_data["raw_name"] . $file_data['file_ext'];
        $config['new_image'] = $path . $file_data["raw_name"] . $file_data['file_ext'];
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = "60";
        $config['width']         = $width;
        $config['height']       = $height;
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }
}
