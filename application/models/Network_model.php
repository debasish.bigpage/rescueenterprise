<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Network_model extends CI_Model
{
    public function add_to_network($data)
    {
        return $this->db->insert('network', $data);
    }

    public function checkLegisFreeByparrentAccountID($user_id)
    {
        $this->db->where('user_account_id', $user_id);
        $query = $this->db->get('network');
        return $query->row();
    }

    public function getNetworkUserList($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get('network');
        return $query->result();
    }

   

    public function update_network($data,$id)
    {
       $this->db->where('id', $id);
       return $this->db->update('network', $data);
       
    }

    public function update_network_parrent($data, $id)
    {
        $this->db->where('user_account_id', $id);
        return $this->db->update('network', $data);
    }

    public function get_users_count($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get('network');
        return $query->num_rows();
    }

    public function getNetworkChild($parrent_id)
    {
        $this->db->where('user_account_id', $parrent_id);
        $query = $this->db->get('network');
        return $query->row();

    }

   /*  public function query_string()
    {
        return "SELECT user_accounts.*, blog_categories.name as category_name, blog_categories.slug as category_slug
                FROM blog_posts
                INNER JOIN blog_categories ON blog_posts.category_id = blog_categories.id" . " ";
    }

    //get post
    public function get_post($id)
    {
        $sql = "SELECT * FROM blog_posts WHERE id =  ?";
        $query = $this->db->query($sql, array(clean_number($id)));
        return $query->row();
    }

    //get post joined
    public function get_post_joined($id)
    {
        $sql = $this->query_string() . "WHERE blog_posts.id = ?";
        $query = $this->db->query($sql, array(clean_number($id)));
        return $query->row();
    }*/
} 
