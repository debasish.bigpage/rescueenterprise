<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Order_model extends CI_Model
{

    public function get_orders($where="")
    {
        if(!empty($where)){
            $this->db->where($where);
        }
        $this->db->order_by("id","DESC");
        $query = $this->db->get('orders');
        return $query->result();

    }


    //add order
    public function add_order($data_transaction)
    {
        $data = array(
            'user_id' => $data_transaction['user_id'],
            'account_id' => $data_transaction['account_id'],
            'create_by' => $data_transaction['create_by'],
            'price_total' => $data_transaction['price_total'],
            'created_on' => date('Y-m-d H:i:s')
            );
            
            if ($this->db->insert('orders', $data)) {
                $order_id = $this->db->insert_id();

                //update order number
                $this->update_order_number($order_id);

                //add order products
                $this->add_order_products($order_id, $data_transaction);

                //add payment transaction
                $this->add_payment_transaction($data_transaction, $order_id);

                return $order_id;
            }
            return false;
       
    }
    //update order number
    public function update_order_number($order_id)
    {
        $order_id = clean_number($order_id);
        $data = array(
            'order_number' => $order_id + 10000
        );
        $this->db->where('id', $order_id);
        $this->db->update('orders', $data);
    }

   
    //add order products
    public function add_order_products($order_id, $data_transaction)
    {
        $cart_items = $data_transaction['products'];
        $order_id = clean_number($order_id);
        foreach ($cart_items as $cart_item) {
                     $data = array(
                        'order_id' => $order_id,
                        'product_id' => $cart_item['product_id'],
                        'product_unit_price' => $cart_item['product_unit_price'],
                        'product_qty' => $cart_item['product_qty'],
                        'product_total_price' => $cart_item['product_total_price'],
                    );
                    
                $this->db->insert('order_products', $data);
        }
    }

  

   

   
    //add payment transaction
    public function add_payment_transaction($data_transaction, $order_id)
    {
        $order_id = clean_number($order_id);
        $data = array(
            'form_user_id' => $data_transaction["user_id"],
            'form_account_id' => $data_transaction["account_id"],
            'user_by' => $data_transaction["create_by"],
            'created_at' => date('Y-m-d H:i:s'),
            'perticullars' => "Product purchase",
            'dr' => $data_transaction["price_total"],
            'cr' => 0,
        );
        
        $this->db->insert('transactions', $data);
    }

  

    //get orders count
    public function get_orders_count_by_cif($user_id)
    {
        $user_id = clean_number($user_id);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('orders');
        return $query->num_rows();
    }

    //get orders count
    public function get_orders_count_account($user_id)
    {
        $user_id = clean_number($user_id);
        $this->db->where('account_id', $user_id);
        $query = $this->db->get('orders');
        return $query->num_rows();
    }

    //get order products
    public function get_order_products($order_id)
    {
        $order_id = clean_number($order_id);
        $this->db->where('order_id', $order_id);
        $query = $this->db->get('order_products');
        return $query->result();
    }

   
    //get order product
    public function get_order_product($order_product_id)
    {
        $this->db->where('id', clean_number($order_product_id));
        $query = $this->db->get('order_products');
        return $query->row();
    }

    //get order
    public function get_order($id)
    {
        $id = clean_number($id);
        $this->db->where('id', $id);
        $query = $this->db->get('orders');
        return $query->row();
    }

    //get order by order number
    public function get_order_by_order_number($order_number)
    {
        $this->db->where('order_number', clean_number($order_number));
        $query = $this->db->get('orders');
        return $query->row();
    }

    public function get_orders_count($where='')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $query = $this->db->get('orders');
        return $query->num_rows();
    }
}
