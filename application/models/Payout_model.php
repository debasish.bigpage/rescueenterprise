<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Payout_model extends CI_Model
{
    public function get_all_payouts($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->order_by('id', "DESC");
        $query = $this->db->get('payout_tracking');
        return $query->result();
    }

    public function add_payout($data)
    {
        return $this->db->insert('payout_tracking', $data);
    }

    //delete contact message
    public function delete_payout($id)
    {
        $id = clean_number($id);
        $this->db->where('id', $id);
        return $this->db->delete('payout_tracking');
    }

    //get project by id
    public function get_payout_by_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('payout_tracking');
        return $query->row();
    }

    public function payout_update($data, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update('payout_tracking', $data);
    }
    public function get_all_payouts_count($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->order_by('id', "DESC");
        $query = $this->db->get('payout_tracking');
        return $query->num_rows();
    }
   
}
