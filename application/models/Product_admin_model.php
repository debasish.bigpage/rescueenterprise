<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_admin_model extends CI_Model
{

    //input values
    public function input_values()
    {
        $data = array(
           
            'title' => $this->input->post('title', true),
            'slug' => $this->input->post('slug', true),
            'description' => $this->input->post('description', true),
            'price' => $this->input->post('price', true),
            'status' => $this->input->post('status', false),
        );
        return $data;
    }

    public function add_product()
    {
        $this->load->library('upload');
        $data = $this->input_values();
        if (!empty($data['slug'])) {
            $data['slug'] = substr($data['slug'], 0, 200);
        }
        if ($_FILES['image']['name'] != '') {
            $path = FCPATH . 'uploads/product/';
            if (!is_dir($path)) {
                mkdir($path, 0755, TRUE);
            }
            $config_prof['encrypt_name']  = TRUE;
            $config_prof['upload_path']   = $path;
            $config_prof['allowed_types'] = 'png|jpeg|jpg';
            $config['max_size']           = '5000';
            $config_prof['overwrite']     = true;
            $this->upload->initialize($config_prof);
            if ($this->upload->do_upload('image')) {
                $file_data = $this->upload->data();
                $this->resizeImage($file_data);
                $data["image"] =  $file_data['file_name'];
            } else {
                return false;
            }
        }
        return $this->db->insert('products', $data);
    }

    public function resizeImage($file_data)
    {
        $this->load->library('image_lib');
        $path = FCPATH . "/uploads/product/";
        $height = 450;
        $width = 750;
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $file_data["raw_name"] . $file_data['file_ext'];
        $config['new_image'] = $path . $file_data["raw_name"] . $file_data['file_ext'];
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = "60";
        $config['width']         = $width;
        $config['height']       = $height;
        $this->image_lib->clear();
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    public function update_porduct($id)
    {
        $oldIng= $this->input->post('old_img');
        $this->load->library('upload');
        $data = $this->input_values();
        if ($_FILES['image']['name'] != '') {
            $path = FCPATH . 'uploads/product/';
            if (!is_dir($path)) {
                mkdir($path, 0755, TRUE);
            }
            $config_prof['encrypt_name']  = TRUE;
            $config_prof['upload_path']   = $path;
            $config_prof['allowed_types'] = 'png|jpeg|jpg';
            $config['max_size']           = '5000';
            $config_prof['overwrite']     = true;
            $this->upload->initialize($config_prof);
            if ($this->upload->do_upload('image')) {
                $file_data = $this->upload->data();
                $this->resizeImage($file_data);
                $data["image"] =  $file_data['file_name'];
                if (!empty($oldIng)) {
                    delete_file_from_server('uploads/product/' . $oldIng);
                }
            } else {
                $this->session->set_flashdata('error',  $this->upload->display_errors());
                redirect($this->agent->referrer());
            }
        }
        $this->db->where('id', $id);
        return $this->db->update('products', $data);
    }

    //update slug
    public function update_slug($id)
    {
        $product = $this->get_product($id);
        if (!empty($product)) {
            if (empty($product->slug) || $product->slug == "-") {
                $data = array(
                    'slug' => $product->id,
                );
            } else {
                    $data = array(
                        'slug' => $product->slug . "-" . $product->id,
                    );
            }
            $this->db->where('id', $product->id);
            return $this->db->update('products', $data);
        }
    }

    //get products
    public function get_products($where="")
    {

        if (!empty($where)) {
            $this->bp->where($where);
        }
        $query = $this->db->get('products');
        return $query->result();
    }

    //get latest products
    public function get_latest_products($limit)
    {
      
        $this->db->where('status', 1);
        $this->db->order_by('products.id', 'DESC')->limit(clean_number($limit));
        return $this->db->get('products')->result();
    }

    //get products count
    public function get_products_count($where="")
    {
        if (!empty($where)) {
            $this->bp->where($where);
        }
        $query = $this->db->get('products');
        return $query->num_rows();
    }

  
    //get product
    public function get_product($id)
    {
        $this->db->where('products.id', clean_number($id));
        return $this->db->get('products')->row();
    }

    

    //delete product
    public function delete_product($product_id)
    {
        $product = $this->get_product($product_id);
        if (!empty($product)) {
            delete_file_from_server('uploads/project/' . $product->image);
            $this->db->where('id', $product_id);
            return $this->db->delete('products');
        }
        return false;
    }

    public function change_status($id)
    {
        $id = clean_number($id);
        $user = $this->get_product($id);

        if (!empty($user)) {
            $data = array();
            if ($user->status == 0) {
                $data['status'] = 1;
            }
            if ($user->status == 1) {
                $data['status'] = 0;
            }

            $this->db->where('id', $id);
            return $this->db->update('products', $data);
        }

        return false;
    }
  

    
}
