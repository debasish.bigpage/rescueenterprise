<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Transaction_model extends CI_Model
{
    /*
    *-------------------------------------------------------------------------------------------------
    * Tranasaction Register
    *-------------------------------------------------------------------------------------------------
    */
    public function get_all_trannsactions($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->order_by("id","DESC");
        $query = $this->db->get('transactions');
        return $query->result();
        
    }


    public function get_all_member_trannsactions($or_where = '')
    {
        if (!empty($or_where)) {
            $this->db->or_where($or_where);
        }
        $this->db->order_by("id", "DESC");
        $query = $this->db->get('transactions');
        return $query->result();
    }

    public function add_transaction($data)
    {
        return $this->db->insert('transactions', $data);
    }

    
    //get project by id
    public function get_trannsaction_by_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('transactions');
        return $query->row();
    }

    public function getSumIncome($where='')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->select_sum('dr');
        $result = $this->db->get('transactions')->row();
        return $result->dr;
         
    }

    public function getSumExpenses($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->select_sum('cr');
        $result = $this->db->get('transactions')->row();
        return $result->cr;
    }

    public function getSumIncomeMember($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->select_sum('cr');
        $result = $this->db->get('transactions')->row();
        return $result->cr;
    }

    public function getSumExpensesMember($where = '')
    {
        if (!empty($where)) {
            $this->db->where($where);
        }
        $this->db->select_sum('dr');
        $result = $this->db->get('transactions')->row();
        return $result->dr;
    }

    public function getTnsPL($where='')
    {
        $dr = 0;
        $cr=0;
        $pl=0;
        if (empty($where)){
            $where = 1;
        }
        $sql = "SELECT * FROM transactions WHERE";
        $sql .= $where;
        $response = $this->db->query($sql)->result();
        if(!empty($response)){
            foreach ($response as $item) {
                $dr+=$item->dr;
                $cr+=$item->cr; 
            }
            $returnArr['dr'] = $dr;
            $returnArr['cr'] = $cr;
            $returnArr['pl'] = $dr-$cr;
            return $returnArr;

        }else{
            $returnArr['dr']=$dr;
            $returnArr['cr']=$cr;
            $returnArr['pl']= $pl;
            return $returnArr;
        }
        
    }

}
