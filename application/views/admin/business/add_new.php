   <div class="col-sm-12">
       <?php $this->load->view('admin/includes/_messages'); ?>
   </div>

   <div class="wrapper2">
       <div class="mblog">
           <h4><?php echo $title ?></h4>
           <?php echo form_open_multipart('admin/save-business-plan'); ?>
           <div class="form-group">
               <label>Leavel</label>
               <select name="level" class="form-control" required>
                   <option value="">Select One</option>
                   <option value="1">Leavel-1</option>
                   <option value="2">Leavel-2</option>
                   <option value="3">Leavel-3</option>
                   <option value="4">Leavel-4</option>
                   <option value="5">Leavel-5</option>
               </select>
           </div>

           <div class="form-group">
               <label>Total Person</label>
               <input type="number" name="leg" class="form-control" required>
           </div>
           <div class="form-group">
               <label>Amount (Per Level)</label>
               <input type="text" name="amount" class="form-control" required>
           </div>

           <div class="form-group">
               <label>Gift Image</label>
               <input type="file" name="image" required>

           </div>
       </div>
       <div class="sav-btn">
           <button type="submit">Save Business Plan</button>
       </div>
       <?php echo form_close(); ?>
   </div>