<div class="col-sm-12">
    <?php $this->load->view('admin/includes/_messages'); ?>
</div>
<div class="wrapper2">
    <div class="mblog-post">
        <div class="text-right madd-btn">
            <a href="<?php echo admin_url() ?>add-business-plan"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Plan</a>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="cs_datatable" role="grid" aria-describedby="example1_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Level</th>
                        <th>Leg</th>
                        <th>Amount</th>
                        <th>Total</th>
                        <th>Gift</th>
                        <th>Option</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($business_settings as $item) : ?>
                        <tr>
                            <td><?php echo html_escape($item->id); ?></td>
                            <td class="td-product">
                                Leavel- <?php echo html_escape($item->level); ?>
                            </td>
                            <td><?php echo html_escape($item->leg); ?></td>
                            <td class="td-product">
                                <i class="fa fa-inr" aria-hidden="true"></i>
                                <?php echo html_escape($item->amount); ?>
                            </td>
                            <td><?php echo html_escape($item->total); ?></td>
                            <td>
                                <div class="img-table">
                                    <img src="<?php echo base_url() . 'uploads/reward/' . $item->image; ?>" alt="" height="80" width="100" />
                                </div>

                            </td>
                            <td class="drp-btn">
                                <div class="dropdown drp">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Select a Option
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="<?php echo admin_url(); ?>edit-business-plan/<?php echo html_escape($item->id); ?>">Edit Plan<i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>



                                        <a class="dropdown-item" href="javascript:void(0)" onclick="delete_item('admin_controller/delete_businessplan','<?php echo $item->id; ?>','Are you want to delete this item?');">Delete <i class="fa fa-trash" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Level</th>
                        <th>Leg</th>
                        <th>Amount</th>
                        <th>Total</th>
                        <th>Gift</th>
                        <th>Option</th>

                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>