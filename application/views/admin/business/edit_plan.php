   <div class="col-sm-12">
       <?php $this->load->view('admin/includes/_messages'); ?>
   </div>

   <div class="wrapper2">
       <div class="mblog">
           <h4><?php echo $title ?></h4>
           <?php echo form_open_multipart('admin/update-business-plan'); ?>
           <div class="form-group">
               <label>Leavel</label>
               <select name="level" class="form-control" required>
                   <option value="">Select One</option>
                   <option <?php echo ($plan->level == 1) ? "selected" : "" ?> value="1">Leavel-1</option>
                   <option <?php echo ($plan->level == 2) ? "selected" : "" ?> value="2">Leavel-2</option>
                   <option <?php echo ($plan->level == 3) ? "selected" : "" ?> value="3">Leavel-3</option>
                   <option <?php echo ($plan->level == 4) ? "selected" : "" ?> value="4">Leavel-4</option>
                   <option <?php echo ($plan->level == 5) ? "selected" : "" ?> value="5">Leavel-5</option>
               </select>
           </div>

           <div class="form-group">
               <label>Total Person</label>
               <input type="number" name="leg" class="form-control" required value="<?php echo $plan->leg ?>">
           </div>
           <div class="form-group">
               <label>Amount (Per Level)</label>
               <input type="text" name="amount" class="form-control" required value="<?php echo $plan->amount ?>">
           </div>

           <div class="form-group">
               <label>Gift Image</label>
               <?php if (!empty($plan->image)) : ?>
                   <img src="<?php echo base_url() . 'uploads/reward/' . $plan->image ?>" class="img-thumbnail" height="150px" width="150px">
               <?php endif; ?>
               <input type="file" name="image" >

           </div>
       </div>
       <div class="sav-btn">
           <input type="hidden" name="id" value="<?php echo $plan->id ?>">
           <input type="hidden" name="old_img" value="<?php echo $plan->image ?>">
           <button type="submit">Update Business Plan</button>
       </div>
       <?php echo form_close(); ?>
   </div>