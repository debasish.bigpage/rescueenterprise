<div class="wrapper">
    <div class="">
        <?php echo form_open('admin/save-alter-user'); ?>
        <div class="wrapper-box">
            <h1 class="text-center">Edit Account</h1>
            <div class="col-sm-12">
                <?php $this->load->view('admin/includes/_messages'); ?>
            </div>
            <div class="wrapper-box-content">

                <div class="m-wrapper-content">

                    <div class="captcha">
                        <div class="captcha-content">
                            <div class="form-group">
                                <label>Customer ID</label>
                                <input type="text" name="username" value="<?php echo $user_data->username; ?>" class="form-control" placeholder="" disabled>
                            </div>

                            <div class="form-group">
                                <label>Customer Name</label>
                                <input type="text" name="full_name" value="<?php echo $user_data->full_name; ?>" class="form-control" placeholder="Name">
                            </div>

                            <div class="form-group">
                                <label>Date Of Birth</label>
                                <input type="date" name="dob" value="<?php echo date('Y-m-d', strtotime($user_data->dob)); ?>" class="form-control" placeholder="Date Of Birth">
                            </div>
                            <div class="form-group">
                                <label>Sex</label>
                                <strong>MALE</strong> <input type="radio" name="sex" value="male" required <?php echo ($user_data->sex == 'male') ? 'checked' : '' ?>>
                                <span> <strong> FEMALE </strong><input type="radio" name="sex" value="female" <?php echo ($user_data->sex == 'female') ? 'checked' : '' ?>></span>
                            </div>

                            <div class="form-group">
                                <label>Address</label>
                                <textarea type="text" name="address" class="form-control"><?php echo $user_data->address; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label>State</label>
                                <select name="state" id="state" class="form-control" required>
                                    <option <?php echo ($user_data->state == "Andhra Pradesh") ? 'selected' : '' ?> value="Andhra Pradesh">Andhra Pradesh</option>
                                    <option <?php echo ($user_data->state == "Andaman and Nicobar Islands") ? 'selected' : '' ?> value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                                    <option <?php echo ($user_data->state == "Arunachal Pradesh") ? 'selected' : '' ?> value="Arunachal Pradesh">Arunachal Pradesh</option>
                                    <option <?php echo ($user_data->state == "Assam") ? 'selected' : '' ?> value="Assam">Assam</option>
                                    <option <?php echo ($user_data->state == "Bihar") ? 'selected' : '' ?> value="Bihar">Bihar</option>
                                    <option <?php echo ($user_data->state == "Chandigarh") ? 'selected' : '' ?> value="Chandigarh">Chandigarh</option>
                                    <option <?php echo ($user_data->state == "Chhattisgarh") ? 'selected' : '' ?> value="Chhattisgarh">Chhattisgarh</option>
                                    <option <?php echo ($user_data->state == "Dadar and Nagar Havel") ? 'selected' : '' ?> value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option>
                                    <option <?php echo ($user_data->state == "Daman and Diu") ? 'selected' : '' ?> value="Daman and Diu">Daman and Diu</option>
                                    <option <?php echo ($user_data->state == "Delhi") ? 'selected' : '' ?> value="Delhi">Delhi</option>
                                    <option <?php echo ($user_data->state == "Lakshadweep") ? 'selected' : '' ?> value="Lakshadweep">Lakshadweep</option>
                                    <option <?php echo ($user_data->state == "Puducherry") ? 'selected' : '' ?> value="Puducherry">Puducherry</option>
                                    <option <?php echo ($user_data->state == "Goa") ? 'selected' : '' ?> value="Goa">Goa</option>
                                    <option <?php echo ($user_data->state == "Gujarat") ? 'selected' : '' ?> value="Gujarat">Gujarat</option>
                                    <option <?php echo ($user_data->state == "Haryana") ? 'selected' : '' ?> value="Haryana">Haryana</option>
                                    <option <?php echo ($user_data->state == "Himachal Pradesh") ? 'selected' : '' ?> value="Himachal Pradesh">Himachal Pradesh</option>
                                    <option <?php echo ($user_data->state == "Jammu and Kashmir") ? 'selected' : '' ?> value="Jammu and Kashmir">Jammu and Kashmir</option>
                                    <option <?php echo ($user_data->state == "Jharkhand") ? 'selected' : '' ?> value="Jharkhand">Jharkhand</option>
                                    <option <?php echo ($user_data->state == "Karnataka") ? 'selected' : '' ?> value="Karnataka">Karnataka</option>
                                    <option <?php echo ($user_data->state == "Kerala") ? 'selected' : '' ?> value="Kerala">Kerala</option>
                                    <option <?php echo ($user_data->state == "Madhya Pradesh") ? 'selected' : '' ?> value="Madhya Pradesh">Madhya Pradesh</option>
                                    <option <?php echo ($user_data->state == "Maharashtra") ? 'selected' : '' ?> value="Maharashtra">Maharashtra</option>
                                    <option <?php echo ($user_data->state == "Manipur") ? 'selected' : '' ?> value="Manipur">Manipur</option>
                                    <option <?php echo ($user_data->state == "Meghalaya") ? 'selected' : '' ?> value="Meghalaya">Meghalaya</option>
                                    <option <?php echo ($user_data->state == "Mizoram") ? 'selected' : '' ?> value="Mizoram">Mizoram</option>
                                    <option <?php echo ($user_data->state == "Nagaland") ? 'selected' : '' ?> value="Nagaland">Nagaland</option>
                                    <option <?php echo ($user_data->state == "Odisha") ? 'selected' : '' ?> value="Odisha">Odisha</option>
                                    <option <?php echo ($user_data->state == "Punjab") ? 'selected' : '' ?> value="Punjab">Punjab</option>
                                    <option <?php echo ($user_data->state == "Rajasthan") ? 'selected' : '' ?> value="Rajasthan">Rajasthan</option>
                                    <option <?php echo ($user_data->state == "Sikkim") ? 'selected' : '' ?> value="Sikkim">Sikkim</option>
                                    <option <?php echo ($user_data->state == "Tamil Nadu") ? 'selected' : '' ?> value="Tamil Nadu">Tamil Nadu</option>
                                    <option <?php echo ($user_data->state == "Telangana") ? 'selected' : '' ?> value="Telangana">Telangana</option>
                                    <option <?php echo ($user_data->state == "Tripura") ? 'selected' : '' ?> value="Tripura">Tripura</option>
                                    <option <?php echo ($user_data->state == "Uttar Pradesh") ? 'selected' : '' ?> value="Uttar Pradesh">Uttar Pradesh</option>
                                    <option <?php echo ($user_data->state == "Uttarakhand") ? 'selected' : '' ?> value="Uttarakhand">Uttarakhand</option>
                                    <option <?php echo ($user_data->state == "West Bengal") ? 'selected' : '' ?> value="West Bengal">West Bengal</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>PIN Code</label>
                                <input type="text" name="pin" value="<?php echo $user_data->pin; ?>" class="form-control" maxlength="6">
                            </div>

                            <div class="form-group">
                                <label>Mobile No</label>
                                <input type="text" name="mobile" value="<?php echo $user_data->mobile; ?>" class="form-control" maxlength="10">
                            </div>
                            <div class="form-group">
                                <label>Email Address</label>
                                <input type="text" name="email" value="<?php echo $user_data->email; ?>" class="form-control">
                            </div>

                           
                            <input type="hidden" name="id" value="<?php echo $user_data->id; ?>">
                            <div class="sav-btn">
                                <button type="submit">Alter User</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>