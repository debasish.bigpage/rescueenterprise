<html lang="EN">

<head>
  <meta charset="UTF-8">
  <link rel="shortcut icon" type="<?php echo base_url() . 'uploads/logo/' . $this->general_settings->favicon ?>" href="<?php echo base_url() . 'uploads/logo/' . $this->general_settings->favicon ?>">
  <title><?php echo xss_clean($title); ?> - <?php echo xss_clean($this->settings->site_title); ?></title>
  <link rel="icon" href="<?php echo base_url(); ?>assets/frontend/images/amma_logo_animated_ver4.gif" type="image/x-icon">
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/css/responsive.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/css/style.css'); ?>">
  <!-- Datatables -->
  <link href="<?php echo base_url('assets/backend/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('assets/backend/datatables/buttons.dataTables.min.css'); ?>">
  <script>
    var base_url = '<?php echo base_url(); ?>';
    var csfr_token_name = '<?php echo $this->security->get_csrf_token_name(); ?>';
    var csfr_cookie_name = '<?php echo $this->config->item('csrf_cookie_name'); ?>';
  </script>
</head>

<body>
  <aside class="mside-nav" id="show-mside-navigation1">
    <i class="fa fa-bars close-aside hidden-sm hidden-md hidden-lg" data-close="show-mside-navigation1"></i>
    <div class="heading">
      <?php if (!empty($this->auth_user->avatar)) : ?>
        <img src="<?php echo base_url() . 'uploads/member-doc/' . $this->auth_user->avatar; ?>" alt="">
      <?php else : ?>
        <img src="<?php echo base_url('assets/backend/images/man.jpg'); ?>" alt="">
      <?php endif; ?>
      <div class="info">
        <h3><a href="<?php echo base_url('dashboard') ?>"><?php echo ($this->session->userdata('bp_sess_usertype') == "admin") ? "Administrator" : "Member" ?></a></h3>
        <p>Active</p>
      </div>
    </div>
    <!--  <div class="search">
      <input type="text" placeholder="Type here"><i class="fa fa-search"></i>
    </div> -->
    <ul class="categories">
      <li class="f-list"><i class="fa fa-tachometer" aria-hidden="true"></i><a href="<?php echo base_url('dashboard') ?>">Dashboard</a>

      <li><i class="fa fa-shopping-basket fa-fw"></i><a href="#">Product</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() ?>products">Products</a></li>
          <li><a href="<?php echo admin_url() ?>add-product">Add Product</a></li>
        </ul>
      </li>
      <li><i class="fa fa-shopping-cart fa-fw"></i><a href="#">Product Order</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() . 'orders'; ?>"> Orders </a></li>
          <li><a href="<?php echo admin_url() . 'create-order'; ?>"> Create Orders </a></li>
        </ul>
      </li>
      <li><i class="fa fa-cubes fa-fw"></i><a href="#">Business Settings</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() . 'business-settings'; ?>"> Business Plan </a></li>
        </ul>
      </li>
      <li><i class="fa fa-users fa-fw"></i><a href="#">Users</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() . 'staff'; ?>"> Staff </a></li>
          <li><a href="<?php echo admin_url() . 'add-staff'; ?>"> Add Staff </a></li>
        </ul>
      </li>
      <li><i class="fa fa-line-chart fa-fw"></i><a href="#">Reports</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() . 'transactions'; ?>"> Transactions </a></li>
          <li><a href="<?php echo admin_url() . 'payout-report'; ?>"> Payouts </a></li>
          <li><a href="<?php echo admin_url() . 'pending-payout-report'; ?>"> Pending Payouts </a></li>
          <li><a href="<?php echo admin_url() . 'blocked-payout'; ?>"> Blocked Payouts </a></li>
          <li><a href="<?php echo admin_url() . 'mlm-users'; ?>"> Customer List</a></li>
          <li><a href="<?php echo admin_url() . 'mlm-accounts'; ?>"> Accounts List</a></li>
          <li><a href="<?php echo admin_url() . 'leavel-list'; ?>"> Leavel List</a></li>
        </ul>
      </li>
      <li><i class="fa fa-bolt fa-fw"></i><a href="#">Settings</a>
        <ul class="mside-nav-dropdown">
          <li><a href="<?php echo admin_url() . 'settings'; ?>"> General Setting </a></li>
          <li><a href="<?php echo admin_url() . 'email-settings'; ?>">Email Settings</a></li>
          <li><a href="<?php echo admin_url() . 'visual-settings'; ?>">Visual Settings</a></li>
        </ul>
      </li>

    </ul>
  </aside>
  <section id="contents" style="padding: 0;">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <i class="fa fa-align-right"></i>
          </button>
          <a class="navbar-brand" href="#"><?php echo $title ?></a>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo admin_url(); ?>profile"><i class="fa fa-user"></i> Profile</a></li>
                <li><a href="<?php echo base_url('change-password'); ?>"><i class="fa fa-lock"></i> Change Password</a></li>
                <li><a href="<?php echo base_url('logout'); ?>"><i class="fa fa-sign-out"></i> Log out</a></li>
              </ul>
            </li>
            <li style="padding-top: 17px;"><a href="#"><i data-show="show-mside-navigation1" class="fa fa-bars show-side-btn"></i></a></li>
          </ul>
        </div>
      </div>
    </nav>