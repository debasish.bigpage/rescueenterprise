   <div class="col-sm-12">
       <?php $this->load->view('admin/includes/_messages'); ?>
   </div>

   <div class="wrapper2">

       <div class="mblog">

           <h4><?php echo $title ?></h4>
           <?php echo form_open_multipart('admin/save-product'); ?>
           <div class="form-group">
               <label>Product Name</label>
               <input type="text" name="title" class="form-control" required>
           </div>
           <div class="form-group">
               <label>Slug (If you leave it blank, it will be generated automatically.)</label>
               <input type="text" name="slug" class="form-control">
           </div>
           <div class="form-group">
               <label>Product Price</label>
               <input type="text" name="price" class="form-control" required>
           </div>

           <div class="form-group">
               <label>Status</label>
               <select name="status" class="form-control" required>
                   <option value="">Select One</option>
                   <option value="1">Enable</option>
                   <option value="0">Disable</option>
               </select>
           </div>


           <div class="form-group">
               <label>Product Description</label>
               <textarea name="description" rows="5" class="form-control"></textarea>
           </div>
           <div class="form-group">
               <label>Product Image</label>
               <input type="file" name="image">
               
           </div>
       </div>
       <div class="sav-btn">
           <button type="submit">Save Product</button>
       </div>
       <?php echo form_close(); ?>
   </div>