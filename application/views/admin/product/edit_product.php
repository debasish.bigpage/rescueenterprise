   <div class="col-sm-12">
       <?php $this->load->view('admin/includes/_messages'); ?>
   </div>

   <div class="wrapper2">

       <div class="mblog">

           <h4><?php echo $title ?></h4>
           <?php echo form_open_multipart('admin/update-product'); ?>
           <div class="form-group">
               <label>Product Name</label>
               <input type="text" name="title" class="form-control" value="<?php echo $product->title ?>" required>
           </div>
           <div class="form-group">
               <label>Slug (If you leave it blank, it will be generated automatically.)</label>
               <input type="text" name="slug" value="<?php echo $product->slug ?>" class="form-control">
           </div>
           <div class="form-group">
               <label>Product Price</label>
               <input type="text" name="price" class="form-control" value="<?php echo $product->price ?>" required>
           </div>

           <div class="form-group">
               <label>Status</label>
               <select name="status" class="form-control" required>
                   <option value="">Select One</option>
                   <option <?php echo ($product->status == 1) ? "selected" : "" ?> value="1">Enable</option>
                   <option <?php echo ($product->status == 0) ? "selected" : "" ?> value="0">Disable</option>
               </select>
           </div>


           <div class="form-group">
               <label>Product Description</label>
               <textarea name="description" rows="5" class="form-control"><?php echo $product->description ?></textarea>
           </div>
           <div class="form-group">
               <label>Product Image</label>
               <div class="featuredImg">
                   <?php if (!empty($product->image)) : ?>
                       <img src="<?php echo base_url() . 'uploads/product/' . $product->image ?>" class="img-thumbnail" height="150px" width="150px">
                   <?php endif; ?>
               </div>
               <input type="file" name="image">

           </div>
       </div>
       <div class="sav-btn">
           <input type="hidden" value="<?php echo $product->id ?>" name="id">
           <input type="hidden" value="<?php echo $product->image ?>" name="old_img">
           <button type="submit">Save Product</button>
       </div>
       <?php echo form_close(); ?>
   </div>