<div class="wrapper2">
    <?php $this->load->view('admin/includes/_messages'); ?>
    <div class="mblog-post">



        <?php echo form_open('admin/unblock-all-payouts') ?>
        <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="cs_datatable" role="grid" aria-describedby="example1_info">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Payment To</th>
                        <th>Description</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Created On</th>
                        <th>Payment Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($transactions as $item) : ?>
                        <tr>
                            <td><input type="checkbox" name="user_ids[]" value="<?php echo $item->id ?>"></td>

                            <td><?php echo html_escape(getUsernameById($item->user_id)); ?></td>
                            <td><?php echo html_escape($item->description); ?></td>
                            <td><?php echo html_escape($item->amount); ?></td>
                            <td>
                                <?php if ($item->status == 1) : ?>
                                    <strong class="text-success">Paid</strong>
                                <?php elseif ($item->status == 0) : ?>
                                    <strong class="text-warning">Un-Paid</strong>
                                <?php else : ?>
                                    <strong class="text-danger">Blocked</strong>
                                <?php endif; ?>
                            </td>
                            <td><?php echo formatted_date($item->created_at); ?></td>
                            <td>
                                <?php if (!empty($item->payment_on)) : ?>
                                    <?php echo formatted_date($item->payment_on); ?>
                                <?php else : ?>
                                    NA
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if ($item->status == 2) : ?>
                                    <a href="<?php echo admin_url() . 'unblock-payout/' . $item->id ?>" class="btn btn-success">Unblock</a>
                                <?php else : ?>
                                    NA
                                <?php endif; ?>

                            </td>

                        </tr>
                    <?php endforeach; ?>

                </tbody>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Payment To</th>
                        <th>Description</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Created On</th>
                        <th>Payment Date</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="row">
            <div class="col-md-4">
                <button type="submit" class="btn btn-danger"> Unblock All</button>
            </div>
        </div>
        <?php echo form_close() ?>
    </div>
</div>