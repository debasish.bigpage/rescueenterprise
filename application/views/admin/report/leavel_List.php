<div class="wrapper2">
    <?php $this->load->view('admin/includes/_messages'); ?>
    <div class="mblog-post">
        <!-- level 1 list -->
        <div class="table-responsive text-center">
            <h3 class="text-center">Leavel-1 (<?php echo count($l1Accounts) ?>)</h3>
            <table class="table ">
                <thead>
                    <tr>
                        <th>CIF</th>
                        <th>Account ID</th>
                        <th>Leavel</th>
                        <th>Total Child</th>
                        <th>Created On</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($l1Accounts)) : ?>
                        <?php foreach ($l1Accounts as $item) : ?>
                            <tr>
                                <td>
                                    <a href="<?php echo base_url() . 'view-member/' . $item->id ?>">
                                        <?php echo html_escape(getUsernameById($item->user_id)); ?>
                                    </a>
                                </td>
                                <td>
                                    <?php echo html_escape($item->account_id); ?>
                                </td>
                                <td>Level-<?php echo html_escape($item->mlm_level); ?></td>
                                <td><?php echo html_escape($item->total_child); ?></td>
                                <td><?php echo formatted_date($item->created_on); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5">No Record Found</td>
                        </tr>
                    <?php endif; ?>

                </tbody>
            </table>
        </div>
        <!-- /leavel 1 list -->
        <!-- level 2 list -->
        <div class="table-responsive text-center">
            <h3 class="text-center">Leavel-2 (<?php echo count($l2Accounts) ?>)</h3>
            <table class="table ">
                <thead>
                    <tr>
                        <th>CIF</th>
                        <th>Account ID</th>
                        <th>Leavel</th>
                        <th>Total Child</th>
                        <th>Created On</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($l2Accounts)) : ?>
                        <?php foreach ($l2Accounts as $item) : ?>
                            <tr>
                                <td>
                                    <a href="<?php echo base_url() . 'view-member/' . $item->id ?>">
                                        <?php echo html_escape(getUsernameById($item->user_id)); ?>
                                    </a>
                                </td>
                                <td>
                                    <?php echo html_escape($item->account_id); ?>
                                </td>
                                <td>Level-<?php echo html_escape($item->mlm_level); ?></td>
                                <td><?php echo html_escape($item->total_child); ?></td>
                                <td><?php echo formatted_date($item->created_on); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5">No Record Found</td>
                        </tr>
                    <?php endif; ?>

                </tbody>
            </table>
        </div>
        <!-- /leavel 2 list -->
        <!-- level 3 list -->
        <div class="table-responsive text-center">
            <h3 class="text-center">Leavel-3 (<?php echo count($l3Accounts) ?>)</h3>
            <table class="table ">
                <thead>
                    <tr>
                        <th>CIF</th>
                        <th>Account ID</th>
                        <th>Leavel</th>
                        <th>Total Child</th>
                        <th>Created On</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($l3Accounts)) : ?>
                        <?php foreach ($l3Accounts as $item) : ?>
                            <tr>
                                <td>
                                    <a href="<?php echo base_url() . 'view-member/' . $item->id ?>">
                                        <?php echo html_escape(getUsernameById($item->user_id)); ?>
                                    </a>
                                </td>
                                <td>
                                    <?php echo html_escape($item->account_id); ?>
                                </td>
                                <td>Level-<?php echo html_escape($item->mlm_level); ?></td>
                                <td><?php echo html_escape($item->total_child); ?></td>
                                <td><?php echo formatted_date($item->created_on); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5">No Record Found</td>
                        </tr>
                    <?php endif; ?>

                </tbody>
            </table>
        </div>
        <!-- /leavel 3 list -->
        <!-- level 4 list -->
        <div class="table-responsive text-center">
            <h3 class="text-center">Leavel-4 (<?php echo count($l4Accounts) ?>)</h3>
            <table class="table ">
                <thead>
                    <tr>
                        <th>CIF</th>
                        <th>Account ID</th>
                        <th>Leavel</th>
                        <th>Total Child</th>
                        <th>Created On</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($l4Accounts)) : ?>
                        <?php foreach ($l4Accounts as $item) : ?>
                            <tr>
                                <td>
                                    <a href="<?php echo base_url() . 'view-member/' . $item->id ?>">
                                        <?php echo html_escape(getUsernameById($item->user_id)); ?>
                                    </a>
                                </td>
                                <td>
                                    <?php echo html_escape($item->account_id); ?>
                                </td>
                                <td>Level-<?php echo html_escape($item->mlm_level); ?></td>
                                <td><?php echo html_escape($item->total_child); ?></td>
                                <td><?php echo formatted_date($item->created_on); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5">No Record Found</td>
                        </tr>
                    <?php endif; ?>

                </tbody>
            </table>
        </div>
        <!-- /leavel 4 list -->
        <!-- level 5 list -->
        <div class="table-responsive text-center">
            <h3 class="text-center">Leavel-5 (<?php echo count($l5Accounts) ?>)</h3>
            <table class="table ">
                <thead>
                    <tr>
                        <th>CIF</th>
                        <th>Account ID</th>
                        <th>Leavel</th>
                        <th>Total Child</th>
                        <th>Created On</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($l5Accounts)) : ?>
                        <?php foreach ($l5Accounts as $item) : ?>
                            <tr>
                                <td>
                                    <a href="<?php echo base_url() . 'view-member/' . $item->id ?>">
                                        <?php echo html_escape(getUsernameById($item->user_id)); ?>
                                    </a>
                                </td>
                                <td>
                                    <?php echo html_escape($item->account_id); ?>
                                </td>
                                <td>Level-<?php echo html_escape($item->mlm_level); ?></td>
                                <td><?php echo html_escape($item->total_child); ?></td>
                                <td><?php echo formatted_date($item->created_on); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="5">No Record Found</td>
                        </tr>
                    <?php endif; ?>

                </tbody>
            </table>
        </div>
        <!-- /leavel 1 list -->
    </div>
</div>