<div class="wrapper2">
    <?php $this->load->view('admin/includes/_messages'); ?>
    <div class="mblog-post">
        <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="cs_datatable" role="grid" aria-describedby="example1_info">
                <thead>
                    <tr>
                        <th>CIF</th>
                        <th>Account ID</th>
                        <th>Leavel</th>
                        <th>Total Child</th>
                        <th>Created By</th>
                        <th>Tree</th>
                        <th>Created On</th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($users as $item) : ?>
                        <tr>
                            <td>
                                <a href="<?php echo base_url() . 'view-member/' . $item->id ?>">
                                    <?php echo html_escape(getUsernameById($item->user_id)); ?>
                                </a>
                            </td>
                            <td>
                                <?php echo html_escape($item->account_id); ?>
                            </td>
                            <td>Level-<?php echo html_escape($item->mlm_level); ?></td>
                            <td><?php echo html_escape($item->total_child); ?></td>
                            <td><?php echo html_escape(getUsernameById($item->created_by)); ?></td>
                            <td><a class="btn btn-primary" href="<?php echo admin_url(); ?>tree-view/<?php echo html_escape($item->id); ?>"><i class="fa fa-sitemap" aria-hidden="true"></i></a></td>
                            <td><?php echo formatted_date($item->created_on); ?></td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
                <tfoot>
                    <tr>
                        <th>CIF</th>
                        <th>Account ID</th>
                        <th>Leavel</th>
                        <th>Total Child</th>
                        <th>Created By</th>
                        <th>Tree</th>
                        <th>Created On</th>
                    </tr>
                </tfoot>
            </table>
        </div>


    </div>
</div>