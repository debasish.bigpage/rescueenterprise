<div class="wrapper2">
    <?php $this->load->view('admin/includes/_messages'); ?>
    <div class="mblog-post">
        <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="cs_datatable" role="grid" aria-describedby="example1_info">
                <thead>
                    <tr>
                        <th>User ID</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>A/c </th>
                        <th>Status</th>
                        <th>Created On</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($users as $item) : ?>
                        <tr>
                            <td><a href="<?php echo base_url() . 'view-member/' . $item->id ?>">
                                    <?php echo html_escape($item->username); ?></a>
                            </td>
                            <td>

                                <div class="img-table">
                                    <?php if (!empty($item->avatar)) : ?>
                                        <img src="<?php echo base_url() . 'uploads/product/' . $item->avatar; ?>" alt="" height="80" width="100" />
                                    <?php else : ?>
                                        <img src="<?php echo base_url('assets/backend/images/no-user.png') ?>" alt="" height="80" width="100" />
                                    <?php endif; ?>
                                </div>
                                <?php echo html_escape($item->full_name); ?>

                            </td>
                            <td><?php echo html_escape($item->mobile); ?></td>
                            <td><?php echo html_escape(get_subAc_count_by_cif($item->id)); ?></td>


                            <td>
                                <?php if ($item->banned == 1) : ?>
                                    <strong class="text-danger">Blocked</strong>
                                <?php else : ?>
                                    <strong class="text-success">Active</strong>
                                <?php endif; ?>

                            </td>
                            <td><?php echo formatted_date($item->created_on); ?></td>
                            <td class="drp-btn">
                                <div class="dropdown drp">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Select a Option
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="<?php echo admin_url(); ?>view-all-accounts/<?php echo html_escape($item->id); ?>"><i class="fa fa-users" aria-hidden="true"></i> All Accounts </a>
                                        <a class="dropdown-item" href="<?php echo admin_url(); ?>edit-account/<?php echo html_escape($item->id); ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Edit Customer </a>
                                        <a class="dropdown-item" href="<?php echo admin_url(); ?>edit-user-status/<?php echo html_escape($item->id); ?>"> <i class="fa fa-ban" aria-hidden="true"></i> Block/Unblock </a>

                                    </div>
                                </div>
                            </td>

                        </tr>
                    <?php endforeach; ?>

                </tbody>
                <tfoot>
                    <tr>
                        <th>User ID</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Legs</th>
                        <th>Level</th>
                        <th>Created On</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>


    </div>
</div>