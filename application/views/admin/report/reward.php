<div class="col-sm-12">
    <?php $this->load->view('admin/includes/_messages'); ?>
</div>
<div class="wrapper2">
    <div class="mblog-post">
        <div class="row">
            <?php echo form_open('admin/filter-reward') ?>
            <div class="col-md-4">
                <label for="">From</label>
                <div class="form-group">
                    <input type="date" name="form" class="form-control" required>
                </div>
            </div>
            <div class="col-md-4">
                <label for="">To</label>
                <div class="form-group">
                    <input type="date" name="to" class="form-control" required>
                </div>
            </div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary"> Filter</button>
            </div>
            <?php echo form_close() ?>
        </div>
        <div class="row">
            <div class="col-md-6">
                <strong>Reward Compleated
                    <?php echo html_escape($pay_done); ?></strong>
            </div>
            <div class="col-md-6">
                <strong>Reward Pending
                    <?php echo html_escape($pay_pending); ?></strong>
            </div>

        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="cs_datatable" role="grid" aria-describedby="example1_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Payment To</th>
                        <th>Description</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Created On</th>
                        <th>Payment Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($transactions as $item) : ?>
                        <tr>
                            <td><?php echo html_escape($item->id); ?></td>
                            <td><?php echo html_escape(getUsernameById($item->user_id)); ?></td>
                            <td><?php echo html_escape($item->description); ?></td>
                            <td><?php echo html_escape($item->amount); ?></td>
                            <td>
                                <?php if ($item->status == 1) : ?>
                                    <strong class="text-success">Paid</strong>
                                <?php else : ?>
                                    <strong class="text-danger">Un-Paid</strong>
                                <?php endif; ?>

                            </td>
                            <td><?php echo formatted_date($item->created_at); ?></td>
                            <td>
                                <?php if (!empty($item->payment_on)) : ?>
                                    <?php echo formatted_date($item->payment_on); ?>
                                <?php else : ?>
                                    NA
                                <?php endif; ?>
                            </td>


                        </tr>
                    <?php endforeach; ?>

                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Payment To</th>
                        <th>Description</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Created On</th>
                        <th>Payment Date</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>