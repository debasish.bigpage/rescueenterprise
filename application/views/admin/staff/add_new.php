<div class="post-b">
    <div class="post-h">
        <h5>Add Staff</h5>
        <a class="btn btn-primary" href="<?php echo admin_url() . 'staff'; ?>">All Staff</a>
    </div>
    <?php $this->load->view('admin/includes/_messages'); ?>
    <?php echo form_open('admin/save-staff'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="post-d">
                <h5>Staff Details</h5>
                <div class="registration-form">
                    <div class="row">
                        <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                            <label for="">NAME OF STAFF</label>
                            <div class="form-group">
                                <input type="text" name="full_name" placeholder="ENTER NAME OF APPLICANT" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                            <label for="">DATE OF BIRTH</label>
                            <div class="form-group">
                                <input type="DATE" name="dob" placeholder="ENTER DATE OF BIRTH " class="form-control" required >
                            </div>
                        </div>
                       
                        <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                            <label for="">MOB NO</label>
                            <div class="form-group">
                                <input type="text" name="mobile" maxlength="10" placeholder="ENTER MOB NO" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                            <label for="">EMAIL</label>
                            <div class="form-group">
                                <input type="email" name="email" placeholder="ENTER EMAIL" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                            <label for="">PASSWORD</label>
                            <div class="form-group">
                                <input type="password" name="password" placeholder="ENTER PASSWORD" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 ">
                            <label for="">RE-TYPE PASSWORD</label>
                            <div class="form-group">
                                <input type="password" name="cpass" placeholder="ENTER PASSWORD" class="form-control" required>
                            </div>
                        </div>
                    </div>
                   
                   
                  

            </div>
            <div class="right-fifth sub-btn2">
                <button type="submit" class="">Add Staff</button>
            </div>
            <?php form_close(); ?>


        </div>
    </div>
</div>