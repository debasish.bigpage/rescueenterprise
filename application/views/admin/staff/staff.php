<div class="col-sm-12">
    <?php $this->load->view('admin/includes/_messages'); ?>
</div>
<div class="wrapper2">
    <div class="mblog-post">
        <div class="text-right madd-btn">
            <a href="<?php echo admin_url() ?>add-staff"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Staff</a>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="cs_datatable" role="grid" aria-describedby="example1_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>UserID</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Option</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($staffs as $item) : ?>
                        <tr>
                            <td><?php echo html_escape($item->id); ?></td>
                            <td><?php echo html_escape($item->username); ?></td>
                            <td>
                                <div class="img-table2" style="height: 67px;">
                                    <?php if (!empty($item->avatar)) : ?>
                                        <img src="<?php echo base_url() . 'uploads/member-doc/' . $item->avatar; ?>" alt="" height="50" width="50" />
                                    <?php else : ?>                                                                                                                                 
                                        <img src="<?php echo base_url() . 'assets/backend/images/no-user.png' ?>" alt="" height="50" width="50" />
                                    <?php endif; ?>
                                </div>
                            </td>
                            <td><?php echo html_escape(ucfirst($item->full_name)); ?></td>
                            <td><?php echo ($item->banned == 0) ? "Active" : "In-active"; ?></td>
                            <td><?php echo formatted_date($item->created_on); ?></td>

                            <td class="drp-btn">
                                <div class="dropdown drp">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Select a Option
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="<?php echo admin_url(); ?>edit-staff/<?php echo html_escape($item->id); ?>">Edit <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                                        <a class="dropdown-item" href="<?php echo admin_url(); ?>edit-staff-status/<?php echo html_escape($item->id); ?>">Chnage Status <i class="fa fa-ban" aria-hidden="true"></i></a>

                                      


                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>UserID</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Status</th>
                        <th>Date</th>
                        <th>Option</th>

                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>