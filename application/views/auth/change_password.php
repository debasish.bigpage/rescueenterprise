<div class="wrapper">
    <div class="">
        <?php echo form_open('update-password'); ?>
        <div class="wrapper-box">
            <h1 class="text-center">Change Password</h1>
            <div class="col-sm-12">
                <?php $this->load->view('admin/includes/_messages'); ?>
            </div>
            <div class="wrapper-box-content">

                <div class="m-wrapper-content">

                    <div class="captcha">
                        <div class="captcha-content">
                            <div class="form-group">
                                <label>Old Password</label>
                                <input type="password" name="old_password" class="form-control" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label>New Password</label>
                                <input type="password" name="new_password" class="form-control" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label>Re-type Password</label>
                                <input type="password" name="password_confirm" class="form-control" placeholder="" required>
                            </div>


                            <div class="sav-btn">
                                <button>Update Changes</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>