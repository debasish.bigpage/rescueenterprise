    <div class="login">
        <div class="container">
            <div class="row">
                <div class="col-md-4 mx-auto">
                    <div class="logo">
                        <img src="<?php echo base_url() ?>assets/images/apicross-logo.png" alt="">
                        <h1 class="text-center"><?php echo $this->app_name ?></h1>
                    </div>
                    <div class="login-box">
                        <h2 class="text-center">Forgot Password</h2>
                        <div class="login-form">
                            <?php $this->load->view('auth/_messages') ?>
                            <?php echo form_open('check-forgot-password'); ?>
                            <div class="form-group">
                                <label for=""><i class="fas fa-user"></i> Email</label>
                                <input name="email" type="email" class="form-control" placeholder="Enter email" required>
                            </div>
                            <a href="<?php echo base_url('login'); ?>">Login</a>
                            <button type="submit" class="sub-btn form-control">Submit</button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>