    <div class="login">
        <div class="container">
            <div class="row">
                <div class="col-md-4 mx-auto">
                    <div class="logo">
                        <img src="<?php echo base_url() . 'uploads/logo/' . $this->general_settings->logo ?>" alt="">
                        <h1 class="text-center"><?php echo $this->app_name ?></h1>
                    </div>
                    <div class="login-box">
                        <h2 class="text-center">Login</h2>
                        <div class="login-form">
                            <?php $this->load->view('auth/_messages') ?>
                            <?php echo form_open('login-check', array('class' => 'form')); ?>
                            <div class="form-group">
                                <label for=""><i class="fas fa-user"></i> User ID</label>
                                <input name="username" type="text" class="form-control" placeholder="Enter User ID" required>
                            </div>
                            <div class="form-group">
                                <label for=""><i class="fa-solid fa-key"></i> Password</label>
                                <input name="password" type="password" class="form-control" placeholder="Enter Password" required>
                            </div>
                            <a href="<?php echo base_url('forgot-password'); ?>">Forget Password?</a>
                            <button type="submit" class="sub-btn form-control">Submit</button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>