<div class="post-b">
    <div class="post-h">
        <h5>Add New Order</h5>
        <a class="btn btn-primary" href="<?php echo staff_url() . 'orders'; ?>">All Order</a>
    </div>
    <?php $this->load->view('admin/includes/_messages'); ?>
    <?php echo form_open('staff-portal/save-order'); ?>

    <div class="row">
        <div class="col-md-12">
            <div class="post-d">
                <h5>Product Details</h5>
                <div class="table-responsive p-details">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Product Name</th>
                                <th>Product image</th>
                                <th>Price</th>
                                <th>Qty</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($products as $item) : ?>
                                <tr>

                                    <td> <?php echo html_escape($item->title); ?></td>
                                    <td class="td-product">

                                        <div class="img-table2" style="height: 67px;">
                                            <img src="<?php echo base_url() . 'uploads/product/' . $item->image; ?>" alt="" height="50" width="100" />
                                        </div>
                                    </td>
                                    <td><input type="text" name="rate[]" id="rate_<?php echo $item->id ?>" class="form-control" value="<?php echo $item->price ?>" readonly></td>
                                    <td><input type="number" name="qty[]" onblur="calculateTotal(<?php echo $item->id ?>)" id="qty_<?php echo $item->id ?>" class="form-control"></td>
                                    <td><input type="text" name="total[]" id="tot_<?php echo $item->id ?>" class="form-control gross" disabled></td>
                                    <input type="hidden" name="product_id[]" value="<?php echo $item->id ?>">
                                </tr>
                            <?php
                            endforeach; ?>
                            <tr >
                                <th colspan="5" style="text-align: right;" >Net Total: <i class="fa fa-inr" aria-hidden="true"></i>&nbsp;<input type="text" name="ntotal" value="0.00" readonly></span></th>
                            </tr>
                        </tbody>
                    </table>


                </div>
                <div class="containu-btn text-center">
                    <a href="javascript:void(0)" class="btn btn-primary btn-c" >Continue</a>
                </div>
            </div>
            <div class="cd-section">
            
                <div class="post-d">
                    <h5>Customer Details</h5>
                    <div class="form-group">
                        <label>Mobile Number <span class="required">*</span></label>
                        <input type="number" name="mobile" class="form-control" placeholder="Mobile Number" onblur="checkUserAvalable(this.value)" required>
                    </div>
                    <div class="form-group">
                        <label>Customer Full Name <span class="required">*</span></label>
                        <input type="text" name="full_name" class="form-control" placeholder="Full Name" required>
                    </div>


                    <div class="form-group">
                        <label>E-mail</label>
                        <input type="email" name="email" class="form-control" placeholder="E-mail">
                    </div>
                    <div class="form-group">
                        <label>Address <span class="required">*</span></label>
                        <input type="text" name="address" class="form-control" placeholder="Address " required>
                    </div>
                    <div class="form-group">
                        <label>State <span class="required">*</span></label>
                        <select name="state" id="state" class="form-control" required>
                            <option value="">Select One</option>
                            <option value="Andhra Pradesh">Andhra Pradesh</option>
                            <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                            <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                            <option value="Assam">Assam</option>
                            <option value="Bihar">Bihar</option>
                            <option value="Chandigarh">Chandigarh</option>
                            <option value="Chhattisgarh">Chhattisgarh</option>
                            <option value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option>
                            <option value="Daman and Diu">Daman and Diu</option>
                            <option value="Delhi">Delhi</option>
                            <option value="Lakshadweep">Lakshadweep</option>
                            <option value="Puducherry">Puducherry</option>
                            <option value="Goa">Goa</option>
                            <option value="Gujarat">Gujarat</option>
                            <option value="Haryana">Haryana</option>
                            <option value="Himachal Pradesh">Himachal Pradesh</option>
                            <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                            <option value="Jharkhand">Jharkhand</option>
                            <option value="Karnataka">Karnataka</option>
                            <option value="Kerala">Kerala</option>
                            <option value="Madhya Pradesh">Madhya Pradesh</option>
                            <option value="Maharashtra">Maharashtra</option>
                            <option value="Manipur">Manipur</option>
                            <option value="Meghalaya">Meghalaya</option>
                            <option value="Mizoram">Mizoram</option>
                            <option value="Nagaland">Nagaland</option>
                            <option value="Odisha">Odisha</option>
                            <option value="Punjab">Punjab</option>
                            <option value="Rajasthan">Rajasthan</option>
                            <option value="Sikkim">Sikkim</option>
                            <option value="Tamil Nadu">Tamil Nadu</option>
                            <option value="Telangana">Telangana</option>
                            <option value="Tripura">Tripura</option>
                            <option value="Uttar Pradesh">Uttar Pradesh</option>
                            <option value="Uttarakhand">Uttarakhand</option>
                            <option value="West Bengal">West Bengal</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>PIN Code <span class="required">*</span></label>
                        <input type="text" maxlength="6" name="pin" class="form-control" placeholder="PIN Code">
                    </div>
                    <input type="hidden" name="is_user_exists" value="0">


                </div>
                <div class="right-fifth sub-btn2">
                    <button type="submit" class="">Add New Order</button>
                </div>
                <?php form_close(); ?>
            </div>
    
            </div>
        </div>
    </div>