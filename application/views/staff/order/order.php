<div class="col-sm-12">
    <?php $this->load->view('admin/includes/_messages'); ?>
</div>
<div class="wrapper2">
    <div class="mblog-post">
        <div class="text-right madd-btn">
            <a href="<?php echo staff_url() ?>create-order"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Order</a>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="cs_datatable" role="grid" aria-describedby="example1_info">
                <thead>
                    <tr>
                        <th>Order No</th>
                        <th>CIF</th>
                        <th>Account</th>
                        <th>Order By</th>
                        <th>Amount</th>
                        <th>Order Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($orders as $item) : ?>
                        <tr>
                            <td><?php echo html_escape($item->order_number); ?></td>
                            <td><?php echo html_escape(get_user($item->user_id)->username); ?></td>
                            <td><?php echo html_escape(get_user_by_account_id($item->account_id)->account_id); ?></td>
                            <td><?php echo html_escape(get_user($item->user_id)->full_name); ?></td>
                            <td>
                                <i class="fa fa-inr" aria-hidden="true"></i>
                                <?php echo html_escape($item->price_total); ?>
                            </td>
                            <td><?php echo formatted_date($item->created_on); ?></td>

                            <td class="drp-btn">
                                <div class="dropdown drp">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Select a Option
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="<?php echo staff_url(); ?>view-order-details/<?php echo html_escape($item->id); ?>">View Order Details <i class="fa fa-eye" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
                <tfoot>
                    <tr>
                        <th>Order No</th>
                        <th>CIF</th>
                        <th>Account</th>
                        <th>Order By</th>
                        <th>Amount</th>
                        <th>Order Date</th>
                        <th>Action</th>

                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>