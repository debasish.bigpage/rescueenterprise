<div class="col-sm-12">
    <?php $this->load->view('admin/includes/_messages'); ?>
</div>
<div class="wrapper2">
    <div class="mblog-post">
        <div class="row">
            <?php echo form_open('staff-portal/filter-transactions') ?>
            <div class="col-md-4">
                <label for="">From</label>
                <div class="form-group">
                    <input type="date" name="form" class="form-control" required>
                </div>
            </div>
            <div class="col-md-4">
                <label for="">To</label>
                <div class="form-group">
                    <input type="date" name="to" class="form-control" required>
                </div>
            </div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary"> Filter</button>
            </div>
            <?php echo form_close() ?>
        </div>
     

        <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="cs_datatable" role="grid" aria-describedby="example1_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Txn By</th>
                        <th>Particulars</th>
                        <th>Dr</th>
                        <th>Cr</th>

                        <th>Txn Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($transactions as $item) : ?>
                        <tr>
                            <td><?php echo html_escape($item->id); ?></td>
                            <td><?php echo html_escape(getUsernameById($item->form_user_id)); ?></td>
                            <td><?php echo html_escape(getUsernameById($item->to_user_id)); ?></td>
                            <td><?php echo html_escape(getUsernameById($item->user_by)); ?></td>
                            <td><?php echo html_escape($item->perticullars); ?></td>
                            <td class="text-success"><?php echo html_escape($item->dr); ?></td>
                            <td class="text-danger"><?php echo html_escape($item->cr); ?></td>

                            <td><?php echo formatted_date($item->created_at); ?></td>


                        </tr>
                    <?php endforeach; ?>

                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Txn By</th>
                        <th>Particulars</th>
                        <th>Dr</th>
                        <th>Cr</th>
                        <th>Txn Date</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>