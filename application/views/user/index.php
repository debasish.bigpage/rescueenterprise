<div class="wrapper3">

    <div class="row">
       
        <div class="col-md-3 col-sm-6">
            <div class="counter red">
                <div class="counter-icon">
                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
                </div>
                <span class="counter-value"><?php echo $total_accounts ?></span>
                <h3>Accounts</h3>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="counter red">
                <div class="counter-icon">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </div>
                <span class="counter-value"><?php echo $total_order ?></span>
                <h3>Orders</h3>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="counter">
                <div class="counter-icon">
                    <i class="fa fa-inr" aria-hidden="true"></i>
                </div>
                <span class="counter-value"><?php echo $expenses ?></span>
                <h3>Order Amount</h3>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="counter red">
                <div class="counter-icon">
                    <i class="fa fa-inr" aria-hidden="true"></i>
                </div>
                <span class="counter-value"><?php echo $income ?></span>
                <h3>Income</h3>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="counter">
                <div class="counter-icon">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </div>
                <span class="counter-value"><?php echo $pay_pending ?></span>
                <h3>Pending</h3>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="counter red">
                <div class="counter-icon">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </div>
                <span class="counter-value"><?php echo $pay_blocked ?></span>
                <h3>Blocked</h3>
            </div>
        </div>
    </div>




    <div class="cus-table">

        <div class="row">
            <div class="col-md-12">
                <div class="panel">

                    <div class="panel-body table-responsive">
                        <div class="table-hed">
                            <h4>Letest Order</h4>
                        </div>
                        <div class="table-part">
                            <table class="table">
                                <thead>
                                    <tr>
                                    <tr>
                                        <th>Order No</th>
                                        <th>CIF</th>
                                        <th>Account</th>
                                        <th>Amount</th>
                                        <th>Order Date</th>
                                    </tr>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    foreach ($orders as $item) :
                                        if ($i < 6) :
                                    ?>
                                            <tr>
                                                <td><?php echo html_escape($item->order_number); ?></td>
                                                <td><?php echo html_escape(get_user($item->user_id)->username); ?></td>
                                                <td><?php echo html_escape(get_user_by_account_id($item->account_id)->account_id); ?></td>

                                                <td>
                                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                                    <?php echo html_escape($item->price_total); ?>
                                                </td>
                                                <td><?php echo formatted_date($item->created_on); ?></td>


                                            </tr>
                                    <?php $i++;
                                        endif;
                                    endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">

                            <div class="col-sm-12 col-xs-6">
                                <div class="view-btn">
                                    <a href="<?php echo member_url() . 'orders' ?>">View all</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>