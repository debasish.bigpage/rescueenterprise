<div class="post-b">
    <div class="post-h">
        <h5>Order Details</h5>
        <a class="btn btn-primary" href="<?php echo member_url() . 'orders'; ?>">All Order</a>
    </div>
    <?php $this->load->view('admin/includes/_messages'); ?>
    

    <div class="row">
        <div class="col-md-12">
            <div class="post-d">
                <h3 class="text-center">Order Details</h3>
                <div class="d-flex">
                <div class="post-d">
                    <h5>Order id : #<?php echo $order->order_number ?></h5>
                    <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td>Status</td>
                            <td>Compleated</td>
                        </tr>
                        <tr>
                            <td>Order Id</td>
                            <td><?php echo $order->id ?></td>
                        </tr>
                        <tr>
                            <td>Order Number</td>
                            <td><?php echo $order->order_number ?></td>
                        </tr>
                        <tr>
                            <td>Order Date</td>
                            <td><?php echo formatted_date($order->created_on) ?></td>
                        </tr>
                    </table>
                    </div>
                </div>

                <div class="post-d">
                    <h5>Buyer</h5>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <td>Customer Id</td>
                            <td><?php echo $customer_data->username ?></td>
                        </tr>
                        <tr>
                            <td>Account Id</td>
                            <td><?php echo get_user_by_account_id($order->account_id)->account_id ?></td>
                        </tr>
                        <tr>
                            <td>Order Total</td>
                            <td> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $order->price_total ?></td>
                        </tr>

                    </table>
                </div>
</div>
</div>
                <div class="post-d">
                    <h5>Customer Billing Details</h5>
                    <table class="table table-bordered table-striped">

                        <tr>
                            <td>Customer Name</td>
                            <td><?php echo $customer_data->full_name ?></td>

                            <td>Email</td>
                            <td><?php echo $customer_data->email ?></td>
                        </tr>
                        <tr>
                            <td>Mobile</td>
                            <td><?php echo $customer_data->mobile ?></td>

                            <td>Address</td>
                            <td><?php echo $customer_data->address ?></td>
                        </tr>
                        <tr>
                            <td>State</td>
                            <td><?php echo $customer_data->state ?></td>

                            <td>PIN</td>
                            <td><?php echo $customer_data->pin ?></td>
                        </tr>

                    </table>
                </div>
                <div class="post-d">
                    <h5>Product Details</h5>
                    <table class="table table-bordered table-striped">

                        <tr>
                            <td>Product ID</td>
                            <td>Product</td>
                            <td>Unit Price</td>
                            <td>Quantity</td>
                            <td>Total</td>
                        </tr>
                        <?php foreach ($products as $product) :
                            $productData = get_product_by_id($product->product_id);

                        ?>
                            <tr>
                                <td><?php echo html_escape($product->product_id); ?></td>
                               
                                <td class="td-product">
                                    
                                    
                                        <div class="img-table2" style="height: 67px;">
                                              <img src="<?php echo base_url() . 'uploads/product/'. $productData->image; ?>" alt="" height="50" width="100"/>
                                        </div>
                                        <?php echo html_escape($productData->title); ?>
                                    
                                </td>
                                <td><i class="fa fa-inr" aria-hidden="true"></i> <?php echo html_escape($product->product_unit_price); ?></td>
                                <td><?php echo html_escape($product->product_qty); ?></td>
                                <td><i class="fa fa-inr" aria-hidden="true"></i> <?php echo html_escape($product->product_total_price); ?></td>
                            </tr>
                        <?php endforeach; ?>

                    </table>
                </div>


            </div>
        </div>
    </div>