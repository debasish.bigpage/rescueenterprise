<div class="wrapper2">
    <?php $this->load->view('admin/includes/_messages'); ?>
    <div class="mblog-post">
        <!--  <div class="row">
            <div class="col-md-12">
                <a href="<?php echo $this->agent->referrer(); ?>" class="btn btn-primary pull-right">Back</a>
            </div>
        </div> -->

        <!-- html-tree -->
        <div class="mlm-tree-box photos-list mt-5">
            <div class="img-content img-content-parent">
                <a href="<?php echo member_url() . 'tree-view/' . $parentData->id ?>">
                    <img src="<?php echo base_url(); ?>assets/images/man.png">
                    <div class="text">
                        <p><strong>A/C ID :<?php echo $parentData->account_id ?></strong></p>
                        <p>CIF : <?php echo getUsernameById($parentData->user_id) ?></p>
                        <p>Total Leg : <?php echo $parentData->total_child ?></p>
                        <p>Leavel : <?php echo $parentData->mlm_level ?></p>

                    </div>

                </a>

            </div>
            <?php if (!empty($childData)) : ?>
                <div class="row1">
                    <?php if ($childData->l1 > 0) : ?>
                        <?php $l1ChildData = getAccountDataByAcID($childData->l1); ?>
                        <div class="img-content img-content1">
                            <a href="<?php echo member_url() . 'tree-view/' . $childData->l1 ?>">
                                <img src="<?php echo base_url(); ?>assets/images/man.png">
                                <div class="text">
                                    <p><strong>A/C ID :<?php echo $l1ChildData->account_id ?></strong></p>
                                    <p>CIF : <?php echo getUsernameById($l1ChildData->user_id) ?></p>
                                    <p>Total Leg : <?php echo $l1ChildData->total_child ?></p>
                                    <p>Leavel : <?php echo $l1ChildData->mlm_level ?></p>
                                </div>
                            </a>
                        </div>
                    <?php endif ?>
                    <?php if ($childData->l2 > 0) : ?>
                        <?php $l2ChildData = getAccountDataByAcID($childData->l2); ?>
                        <div class="img-content">
                            <a href="<?php echo member_url() . 'tree-view/' . $childData->l2 ?>">
                                <img src="<?php echo base_url(); ?>assets/images/man.png">
                                <div class="text">
                                    <p><strong>A/C ID :<?php echo $l2ChildData->account_id ?></strong></p>
                                    <p>CIF : <?php echo getUsernameById($l2ChildData->user_id) ?></p>
                                    <p>Total Leg : <?php echo $l2ChildData->total_child ?></p>
                                    <p>Leavel : <?php echo $l2ChildData->mlm_level ?></p>
                                </div>
                            </a>
                        </div>
                    <?php endif; ?>
                    <?php if ($childData->l3 > 0) : ?>
                        <?php $l3ChildData = getAccountDataByAcID($childData->l3); ?>
                        <div class="img-content">
                            <a href="<?php echo member_url() . 'tree-view/' . $childData->l3 ?>">
                                <img src="<?php echo base_url(); ?>assets/images/man.png">
                                <div class="text">
                                    <p><strong>A/C ID :<?php echo $l3ChildData->account_id ?></strong></p>
                                    <p>CIF : <?php echo getUsernameById($l3ChildData->user_id) ?></p>
                                    <p>Total Leg : <?php echo $l3ChildData->total_child ?></p>
                                    <p>Leavel : <?php echo $l3ChildData->mlm_level ?></p>
                                </div>
                            </a>
                        </div>
                    <?php endif; ?>
                    <?php if ($childData->l4 > 0) : ?>
                        <?php $l4ChildData = getAccountDataByAcID($childData->l4); ?>
                        <div class="img-content">
                            <a href="<?php echo member_url() . 'tree-view/' . $childData->l4 ?>">
                                <img src="<?php echo base_url(); ?>assets/images/man.png">
                                <div class="text">
                                    <p><strong>A/C ID :<?php echo $l4ChildData->account_id ?></strong></p>
                                    <p>CIF : <?php echo getUsernameById($l4ChildData->user_id) ?></p>
                                    <p>Total Leg : <?php echo $l4ChildData->total_child ?></p>
                                    <p>Leavel : <?php echo $l4ChildData->mlm_level ?></p>
                                </div>
                            </a>
                        </div>
                    <?php endif; ?>
                    <?php if ($childData->l5 > 0) : ?>
                        <?php $l5ChildData = getAccountDataByAcID($childData->l5); ?>
                        <div class="img-content img-content2">
                            <a href="<?php echo member_url() . 'tree-view/' . $childData->l5 ?>">
                                <img src="<?php echo base_url(); ?>assets/images/man.png">
                                <div class="text">
                                    <p><strong>A/C ID :<?php echo $l5ChildData->account_id ?></strong></p>
                                    <p>CIF : <?php echo getUsernameById($l5ChildData->user_id) ?></p>
                                    <p>Total Leg : <?php echo $l5ChildData->total_child ?></p>
                                    <p>Leavel : <?php echo $l5ChildData->mlm_level ?></p>
                                </div>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>