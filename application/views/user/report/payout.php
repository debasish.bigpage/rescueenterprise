<div class="col-sm-12">
    <?php $this->load->view('admin/includes/_messages'); ?>
</div>
<div class="wrapper2">
    <div class="mblog-post">
       

        <div class="table-responsive">
            <table class="table table-bordered table-striped dataTable" id="cs_datatable" role="grid" aria-describedby="example1_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Cus ID</th>
                        <th>A/C ID</th>
                        <th>Description</th>
                        <th>Amount</th>
                        
                        <th>Created On</th>
                        <th>Payment Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($transactions as $item) : ?>
                        <tr>
                            <td><?php echo html_escape($item->id); ?></td>
                            <td><?php echo html_escape(getUsernameById($item->user_id)); ?></td>
                            <td><?php echo html_escape(getUsernameById($item->account_id)); ?></td>
                            <td><?php echo html_escape($item->description); ?></td>
                            <td><?php echo html_escape($item->amount); ?></td>
                            <td><?php echo formatted_date($item->created_at); ?></td>
                            <td>
                                <?php if (!empty($item->payment_on)) : ?>
                                    <?php echo formatted_date($item->payment_on); ?>
                                <?php else : ?>
                                    NA
                                <?php endif; ?>
                            </td>


                        </tr>
                    <?php endforeach; ?>

                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Cus ID</th>
                        <th>A/C ID</th>
                        <th>Description</th>
                        <th>Amount</th>
                        
                        <th>Created On</th>
                        <th>Payment Date</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>