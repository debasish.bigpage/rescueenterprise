<div class="wrapper2 contact">
    <?php $this->load->view('admin/includes/_messages'); ?>
    <div class="mblog-post ">
        <h2 class="text-center">Customer Details</h2>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">

                <tr>
                    <td>Customer Id</td>
                    <td><strong class='text-primary'><?php echo $user_data->username ?></strong></td>
                    <td>Customer Added On</td>
                    <td><?php echo formatted_date($user_data->created_on); ?></td>
                </tr>
                <tr>
                    <td>Member Name</td>
                    <td><?php echo $user_data->full_name ?></td>
                    <td>Date Of Birth</td>
                    <td><?php echo date('d-M-Y', strtotime($user_data->dob))  ?></td>
                </tr>

                <tr>
                    <td>Address</td>
                    <td><?php echo $user_data->address  ?></td>
                    <td>State</td>
                    <td><?php echo $user_data->state  ?></td>
                </tr>

                <tr>
                    <td>PIN Code</td>
                    <td><?php echo $user_data->pin ?></td>
                    <td>Phone</td>
                    <td><?php echo $user_data->mobile  ?></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><?php echo $user_data->email ?></td>
                    <td>A/C Created By</td>
                    <td><?php echo getUsernameById($user_data->created_by)  ?></td>
                </tr>
                <tr>
                    <td>Total Accounts</td>
                    <td><?php echo get_subAc_count_by_cif($user_data->id) ?></td>
                    <td>Total Order(s)</td>
                    <td><?php echo totalOrderByCif($user_data->id)  ?></td>
                </tr>
            </table>
        </div>
        <?php if (!empty($sub_account)) : ?>
            <h2 class="text-center">Customer Accounts</h2>
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <tr>
                        <th>#</th>
                        <th>Account ID</th>
                        <th>Created By</th>
                        <th>Created On</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php  $i = 1; foreach ($sub_account as $item) :?>
                            <tr>
                                <td>
                                    <?php echo $i; ?>
                                </td>
                                <td>
                                    <?php echo html_escape($item->account_id); ?>
                                </td>
                                <td><?php echo html_escape(getUsernameById($item->created_by)); ?></td>
                                <td><?php echo formatted_date($item->created_on); ?></td>
                            </tr>
                        <?php $i++;endforeach; ?>

                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>