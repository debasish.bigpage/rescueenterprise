//token update
$("form").submit(function () {
    $("input[name='" + csfr_token_name + "']").val($.cookie(csfr_cookie_name));
  });
 

$(document).ready(function () {
    $('#cs_datatable').DataTable({
        "order": [[0, "desc"]],
        "aLengthMenu": [[15, 30, 60, 100], [15, 30, 60, 100, "All"]]
    });
});
function countChar(val) {
    var len = val.value.length;
    if (len >= 230) {
        val.value = val.value.substring(0, 230);
    } else {
        $('#charNum').text(230 - len);
    }
};


function delete_item(url, id, message) {
    swal({
        text: message,
        icon: "warning",
        buttons: true,
        buttons: [sweetalert_cancel, sweetalert_ok],
        dangerMode: true,
    }).then(function (willDelete) {
        if (willDelete) {
            var data = {
                'id': id,
            };
            data[csfr_token_name] = $.cookie(csfr_cookie_name);
            $.ajax({
                type: "POST",
                url: base_url + url,
                data: data,
                success: function (response) {
                    location.reload();
                }
            });
        }
    });
};

function calculateTotal(productID) {
    
    var productqty = $('#qty_'+productID).val();
    var rate = $('#rate_'+productID).val();
    var gross = parseFloat(rate * productqty);
    $("#tot_"+productID).val(gross);
    calculateNetTotal();

}

function calculateNetTotal() {
    var sum = 0;
        $("input[class *= 'gross']").each(function () {
            sum += +$(this).val();
        });
    sum = sum.toFixed(2);
    $('input[name="ntotal"]').val(sum);
}

function checkUserAvalable(mobile) {
    if(mobile.length<10){
        alert("Mobile Number at least 10 digit");
        location.reload();
    }else{
    var data = {
        "mobile": mobile,
    };
    data[csfr_token_name] = $.cookie(csfr_cookie_name);
    $.ajax({
        type: "POST",
        url: base_url + "auth_controller/is_user_exists_by_mobile",
        data: data,
        beforeSend: function () {
            $("#ajaxloaderModal").show();
        },
        success: function (response) {
            $("#ajaxloaderModal").hide();
            var obj = JSON.parse(response);
            if (obj.status == 1) {
                $('input[name="pin"]').val(obj.user_data.pin);
                $('input[name="full_name"]').val(obj.user_data.full_name);
                $('input[name="email"]').val(obj.user_data.email);
                $('input[name="address"]').val(obj.user_data.address);
                $('input[name="is_user_exists"]').val(1);
                $('#state').val(obj.user_data.state);
            }else if(obj.status==2){
                alert("This account is blocked by admin! You cant purchase product. Contact to admin.")
                location.reload();
            } else if (obj.status == 3) {
                alert("You Cant Purchase product before 25 days of last order");
                location.reload();
            } else {
                $('input[name="is_user_exists"]').val(0);
                $('input[name="full_name"]').val("");
                $('input[name="email"]').val("");
                $('input[name="address"]').val("");
                $('#state').val("");
                $('input[name="pin"]').val("");
            }
        }
    });
    }
}


























//get blog categories
function get_Scheme_categories_by_lang(val) {
    var data = {
        "lang_id": val
    };
    data[csfr_token_name] = $.cookie(csfr_cookie_name);

    $.ajax({
        type: "POST",
        url: base_url + "Scheme_controller/get_categories_by_lang",
        data: data,
        success: function (response) {
            $('#categories').children('option:not(:first)').remove();
            $("#categories").append(response);
        }
    });
}

function additionalImageUpload() {
    var name = document.getElementById("extra_image").files[0].name;
    var form_data = new FormData();
    var ext = name.split('.').pop().toLowerCase();
    form_data.append("extra_image", document.getElementById('extra_image').files[0]);
    form_data.append(csfr_token_name, $.cookie(csfr_cookie_name));
    $.ajax({
        url: base_url + "ajax_controller/extraImageUpload",
        method: "POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            $("#ajaxloaderModal").show();
        },
        success: function (resp) {
            $("#ajaxloaderModal").hide();
            var obj = JSON.parse(resp);
            if (obj.error == 0) {
                // var image = 
                $('input[name="extra_image"]').val(obj.uploded_message);
                $('.extra_img_div').html("<img src='" + base_url + "uploads/tempimg/" + obj.uploded_message + "' >" + "<a href='javascript:void(0)' onclick='rem_ext_img()'><i class='fa fa-times' aria-hidden='true'></i></a>");
                
            } else {
                $(".extra_img_div").html(obj.message);
            }
        }
    });
}

function featuredImageUpload() {
    var name = document.getElementById("featured_image").files[0].name;
    var form_data = new FormData();
    var ext = name.split('.').pop().toLowerCase();
    form_data.append("featured_image", document.getElementById('featured_image').files[0]);
    form_data.append(csfr_token_name, $.cookie(csfr_cookie_name));
    $.ajax({
        url: base_url + "ajax_controller/featuredImageUpload",
        method: "POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            $("#ajaxloaderModal").show();
        },
        success: function (resp) {
            $("#ajaxloaderModal").hide();
            var obj = JSON.parse(resp);
            if (obj.error == 0) {
                // var image = 
                $('input[name="featured_image"]').val(obj.uploded_message);
                $('.featuredImg').html("<img src='" + base_url + "uploads/tempimg/" + obj.uploded_message + "' >" + "<a href='javascript:void(0)' onclick='rem_featured_image_img()'><i class='fa fa-times' aria-hidden='true'></i></a>");
                $('.insertimg').hide();

            } else {
                $(".featuredImg").html(obj.message);
            }
        }
    });
}

function uploadFiles() {
    // appendArrayFiles();
    var id = parseInt($('input[name="file_index"]').val());
    var nid = id - 1;
    var fid = 'extraImg' + id;
    var IncID = id+1;
    var name = document.getElementById("upload_files").files[0].name;
    var form_data = new FormData();
    var ext = name.split('.').pop().toLowerCase();
    form_data.append("upload_files", document.getElementById('upload_files').files[0]);
    form_data.append(csfr_token_name, $.cookie(csfr_cookie_name));
    $.ajax({
        url: base_url + "ajax_controller/uploadFiles",
        method: "POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            $("#ajaxloaderModal").show();
        },
        success: function (resp) {
            $("#ajaxloaderModal").hide();
            var obj = JSON.parse(resp);
            var fieldName = obj.fieldName;
            if (obj.error == 0) {
                $(".dinamicfiles__").append('<input type="hidden" name="extraFiles__[]" value="' + obj.uploded_message+'">');
                $('input[name="file_index"]').val(IncID);
                $('#upload_files').val("");
                $(".fileList").append('<li class="uplodedfileli' + nid + '"><i class="fa fa-file" aria-hidden="true"></i> ' + obj.uploded_message + '<a href="javascript:void(0)"><i class="fa fa-times" aria-hidden="true" onclick="rem_temp_files(' + nid + ')"></i></a></li>');
                
            } else {
                $(".fuploadErr").html(obj.message);
                $('#upload_files').val("");
            }
        }
    });

}

function rem_temp_files(id) {
    var fileVal = $('input[name="extraFiles__[]"]:eq(' + id + ')').val();
    var data = {
        "file_name": fileVal,
    };
    data[csfr_token_name] = $.cookie(csfr_cookie_name);
    $.ajax({
        type: "POST",
        url: base_url + "blogs/removeTempBlogFile",
        data: data,
        beforeSend: function () {
            $("#ajaxloaderModal").show();
        },
        success: function (response) {
            $("#ajaxloaderModal").hide();
            var obj = JSON.parse(response);
            if (obj.error == 0) {
                $('input[name="extraFiles__[]"]:eq(' + id + ')').val("");
                $(".uplodedfileli" + id).remove();
            } else {
                $('input[name="extraFiles__[]"]:eq(' + id + ')').val("");
                $(".uplodedfileli" + id).remove();
            }
        }
    });
    
}


function rem_ext_img() {
    var images = $('input[name="extra_image"]').val();
    images = $.trim(images);   
    var data = {
        "image": images
    };
    data[csfr_token_name] = $.cookie(csfr_cookie_name);

    $.ajax({
        type: "POST",
        url: base_url + "ajax_controller/removeExtraImage",
        data: data,
        beforeSend: function () {
            $("#ajaxloaderModal").show();
        },
        success: function (response) {
            $("#ajaxloaderModal").hide();
            var obj = JSON.parse(response);
            if (obj.error == 0) {
                $('input[name="extra_image"]').val("");
                $('#extra_image').val("");
                $('.extra_img_div').html("");
            } else {
                $('input[name="extra_image"]').val("");
                $('#extra_image').val("");
                $('.extra_img_div').html("");
            }
        }
    });
}

function rem_featured_image_img() {
    var images = $('input[name="featured_image"]').val();
    images = $.trim(images);
    var data = {
        "image": images
    };
    data[csfr_token_name] = $.cookie(csfr_cookie_name);

    $.ajax({
        type: "POST",
        url: base_url + "ajax_controller/removefeaturedImage",
        data: data,
        beforeSend: function () {
            $("#ajaxloaderModal").show();
        },
        success: function (response) {
            $("#ajaxloaderModal").hide();
            var obj = JSON.parse(response);
            if (obj.error == 0) {
                $('input[name="featured_image"]').val("");
                $('#featured_image').val("");
                $('.featuredImg').html("");
                $('.insertimg').show();
            } else {
                $('input[name="featured_image"]').val("");
                $('#featured_image').val("");
                $('.featuredImg').html("");
                $('.insertimg').show();
            }
        }
    });
}

function remove_featured_image(id) {
    if (confirm("Are you sure you want to delete this image?")) {
        var data = {
            id: id,
        };
        data[csfr_token_name] = $.cookie(csfr_cookie_name);
        $.ajax({
            type: "POST",
            url: base_url + "activity_controller/deletefeaturedImage",
            data: data,
            beforeSend: function () {
                $("#ajaxloaderModal").show();
            },
            success: function (response) {
                $("#ajaxloaderModal").hide();
                var obj = JSON.parse(response);
                if (obj.error == 0) {
                    $('input[name="featured_image"]').val("");
                    $('#featured_image').val("");
                    $('.featuredImg').html("");
                    $('.insertimg').show();
                } else {
                   alert("Unable To remove Image!");
                }
            }
        });
    } else {
        return false;
    }
}

function remove_project_featured_image(id) {
    if (confirm("Are you sure you want to delete this image?")) {
        var data = {
            id: id,
        };
        data[csfr_token_name] = $.cookie(csfr_cookie_name);
        $.ajax({
            type: "POST",
            url: base_url + "project_controller/deletefeaturedImage",
            data: data,
            beforeSend: function () {
                $("#ajaxloaderModal").show();
            },
            success: function (response) {
                $("#ajaxloaderModal").hide();
                var obj = JSON.parse(response);
                if (obj.error == 0) {
                    $('input[name="featured_image"]').val("");
                    $('#featured_image').val("");
                    $('.featuredImg').html("");
                    $('.insertimg').show();
                } else {
                    alert("Unable To remove Image!");
                }
            }
        });
    } else {
        return false;
    }
}

function remove_govt_scheme_featured_image(id) {
    if (confirm("Are you sure you want to delete this image?")) {
        var data = {
            id: id,
        };
        data[csfr_token_name] = $.cookie(csfr_cookie_name);
        $.ajax({
            type: "POST",
            url: base_url + "scheme_controller/deletefeaturedImage",
            data: data,
            beforeSend: function () {
                $("#ajaxloaderModal").show();
            },
            success: function (response) {
                $("#ajaxloaderModal").hide();
                var obj = JSON.parse(response);
                if (obj.error == 0) {
                    $('input[name="featured_image"]').val("");
                    $('#featured_image').val("");
                    $('.featuredImg').html("");
                    $('.insertimg').show();
                } else {
                    alert("Unable To remove Image!");
                }
            }
        });
    } else {
        return false;
    }
}

function remove_Campaign_featured_image(id) {
    if (confirm("Are you sure you want to delete this image?")) {
        var data = {
            id: id,
        };
        data[csfr_token_name] = $.cookie(csfr_cookie_name);
        $.ajax({
            type: "POST",
            url: base_url + "campaign_controller/deletefeaturedImage",
            data: data,
            beforeSend: function () {
                $("#ajaxloaderModal").show();
            },
            success: function (response) {
                $("#ajaxloaderModal").hide();
                var obj = JSON.parse(response);
                if (obj.error == 0) {
                    $('input[name="featured_image"]').val("");
                    $('#featured_image').val("");
                    $('.featuredImg').html("");
                    $('.insertimg').show();
                } else {
                    alert("Unable To remove Image!");
                }
            }
        });
    } else {
        return false;
    }
}

function removeExtraImage(id){
    if (confirm("Are you sure you want to delete this image?")) {
        var data = {
            id: id,
        };
        data[csfr_token_name] = $.cookie(csfr_cookie_name);
        $.ajax({
            type: "POST",
            url: base_url + "blogs/deleteExtraImage",
            data: data,
            beforeSend: function () {
                $("#ajaxloaderModal").show();
            },
            success: function (response) {
                $("#ajaxloaderModal").hide();
                var obj = JSON.parse(response);
                if (obj.error == 0) {
                    $('input[name="extra_image"]').val("");
                    $('#extra_image').val("");
                    $('.extra_img_div').html("");
                } else {
                    alert("Unable To remove Image!");
                }
            }
        });
    } else {
        return false;
    }
}

function remove_upload_files(fileid,rowid) {
    if (confirm("Are you sure you want to delete this File?")) {
        var data = {
            fileid: fileid,
        };
        data[csfr_token_name] = $.cookie(csfr_cookie_name);
        $.ajax({
            type: "POST",
            url: base_url + "blogs/deleteUplodedFiles",
            data: data,
            beforeSend: function () {
                $("#ajaxloaderModal").show();
            },
            success: function (response) {
                $("#ajaxloaderModal").hide();
                var obj = JSON.parse(response);
                if (obj.error == 0) {
                    $(".fileList" + rowid).remove();
                } else {
                    alert("Unable To remove File!");
                }
            }
        });
    } else {
        return false;
    }
}


// ###################### PODCARDS ###################
//get blog categories
function get_podcasts_categories_by_lang(val) {
    var data = {
        "lang_id": val
    };
    data[csfr_token_name] = $.cookie(csfr_cookie_name);

    $.ajax({
        type: "POST",
        url: base_url + "podcasts/get_categories_by_lang",
        data: data,
        success: function (response) {
            $('#categories').children('option:not(:first)').remove();
            $("#categories").append(response);
        }
    });
}

function remove_upload_Podcasts_files(fileid, rowid) {
    if (confirm("Are you sure you want to delete this File?")) {
        var data = {
            fileid: fileid,
        };
        data[csfr_token_name] = $.cookie(csfr_cookie_name);
        $.ajax({
            type: "POST",
            url: base_url + "podcasts/deleteUplodedFiles",
            data: data,
            beforeSend: function () {
                $("#ajaxloaderModal").show();
            },
            success: function (response) {
                $("#ajaxloaderModal").hide();
                var obj = JSON.parse(response);
                if (obj.error == 0) {
                    $(".fileList" + rowid).remove();
                } else {
                    alert("Unable To remove File!");
                }
            }
        });
    } else {
        return false;
    }
}

function teamImageUpload() {
    var name = document.getElementById("member_image").files[0].name;
    var form_data = new FormData();
    var ext = name.split('.').pop().toLowerCase();
    form_data.append("member_image", document.getElementById('member_image').files[0]);
    form_data.append(csfr_token_name, $.cookie(csfr_cookie_name));
    $.ajax({
        url: base_url + "ajax_controller/teamImageUpload",
        method: "POST",
        data: form_data,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function () {
            $("#ajaxloaderModal").show();
        },
        success: function (resp) {
            $("#ajaxloaderModal").hide();
            
             var obj = JSON.parse(resp);
            
            if (obj.error == 0) {
                
                $('input[name="member_image"]').val(obj.uploded_message);
                $('.member_image').html("<img src='" + base_url + "uploads/tempimg/" + obj.uploded_message + "' >" + "<a href='javascript:void(0)' onclick='removeTeamImgTemp()'><i class='fa fa-times' aria-hidden='true'></i></a>");

            } else {
                $(".member_image").html(obj.message);
            } 
        }
    });
}

function removeTeamImgTemp() {
    var images = $('input[name="member_image"]').val();
    images = $.trim(images);
    var data = {
        "image": images
    };
    data[csfr_token_name] = $.cookie(csfr_cookie_name);

    $.ajax({
        type: "POST",
        url: base_url + "ajax_controller/removeMemberImage",
        data: data,
        beforeSend: function () {
            $("#ajaxloaderModal").show();
        },
        success: function (response) {
            $("#ajaxloaderModal").hide();
            var obj = JSON.parse(response);
            if (obj.error == 0) {
                $('input[name="member_image"]').val("");
                $('#member_image').val("");
                $('.member_image').html("");
            } else {
                $('input[name="member_image"]').val("");
                $('#member_image').val("");
                $('.member_image').html("");
            }
        }
    });
}

// PAGE JS
//get blog categories
function get_parrent_page_by_lang(val) {
    var data = {
        "lang_id": val
    };
    data[csfr_token_name] = $.cookie(csfr_cookie_name);

    $.ajax({
        type: "POST",
        url: base_url + "blogs/get_categories_by_lang",
        data: data,
        success: function (response) {
            $('#categories').children('option:not(:first)').remove();
            $("#categories").append(response);
        }
    });
}

$(document).ready(function(){
    $('.containu-btn .btn-c').click(function(){
        $('.cd-section').toggle();
    });
});


//load jquery first
$(document).ready(function() {
  // create showDescription function
  function  showDescription() {
    //this will be done on event
        $(this).find('.text').slideToggle('slow');
  }
//mouse event
  $('.photos-list').on('mouseenter mouseleave', 'a', showDescription );
});