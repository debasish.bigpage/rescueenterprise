-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 26, 2022 at 01:40 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `productmlm`
--

-- --------------------------------------------------------

--
-- Table structure for table `business_settings`
--

CREATE TABLE `business_settings` (
  `id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `leg` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `business_settings`
--

INSERT INTO `business_settings` (`id`, `level`, `leg`, `amount`, `total`, `image`) VALUES
(1, 1, 5, '10.00', '50.00', '8667d630b5f83cb1fcf6cc490756030c.png'),
(2, 2, 25, '8.00', '200.00', '7b88b728d6ec224b0012bc92bbc63786.png'),
(3, 3, 125, '6.00', '750.00', '36b603070d46e5b8a209464142156d96.png'),
(4, 4, 625, '4.00', '2500.00', '33679723b1a3ab7b1c3ee42b1f18ce18.png'),
(5, 5, 3125, '6.00', '18750.00', '603c4211ff828deee742b2848d0a1d08.png');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('36d9661b825e938e9bcb878c42e0f96759263669', '127.0.0.1', 1650630392, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303633303339313b),
('49431d878bc069e6ea2bedd8f230bb42421f7a0d', '127.0.0.1', 1650630695, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303633303639353b62705f736573735f757365725f69647c733a313a2231223b62705f736573735f757365725f656d61696c7c733a31353a22696e666f40626967706167652e696e223b62705f736573735f757365725f726f6c657c733a353a2261646d696e223b62705f736573735f6c6f676765645f696e7c623a313b62705f736573735f757365726e616d657c733a353a2261646d696e223b62705f736573735f75736572747970657c733a353a2261646d696e223b737563636573737c733a31333a2253746166662055706461746564223b5f5f63695f766172737c613a313a7b733a373a2273756363657373223b733a333a226f6c64223b7d),
('57fc008153480d29b48f6ebbf0c6f39e0e3ea697', '127.0.0.1', 1650631009, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303633313030393b62705f736573735f757365725f69647c733a313a2237223b62705f736573735f757365725f656d61696c7c733a32323a226176696a6974737461666640626967706167652e696e223b62705f736573735f757365725f726f6c657c733a353a227374616666223b62705f736573735f6c6f676765645f696e7c623a313b62705f736573735f757365726e616d657c733a31303a2252455332324332383037223b62705f736573735f75736572747970657c733a353a227374616666223b),
('3f0049c8dc9e9a738a86ccb9ec44258b8e885a8b', '127.0.0.1', 1650631469, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303633313436393b62705f736573735f757365725f69647c733a313a2231223b62705f736573735f757365725f656d61696c7c733a31353a22696e666f40626967706167652e696e223b62705f736573735f757365725f726f6c657c733a353a2261646d696e223b62705f736573735f6c6f676765645f696e7c623a313b62705f736573735f757365726e616d657c733a353a2261646d696e223b62705f736573735f75736572747970657c733a353a2261646d696e223b),
('65d1fccc45214eb3986ea748994003930e213710', '127.0.0.1', 1650633815, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303633333831353b62705f736573735f757365725f69647c733a313a2231223b62705f736573735f757365725f656d61696c7c733a31353a22696e666f40626967706167652e696e223b62705f736573735f757365725f726f6c657c733a353a2261646d696e223b62705f736573735f6c6f676765645f696e7c623a313b62705f736573735f757365726e616d657c733a353a2261646d696e223b62705f736573735f75736572747970657c733a353a2261646d696e223b),
('c88b2730f42d98818186a883c3373a751ae3e5e7', '127.0.0.1', 1650633815, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303633333831353b),
('006524ef96dab20d70d33a9e4ebd935a5b3647c6', '127.0.0.1', 1650692856, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303639323835363b),
('c81f93d95579d9e5e112cdeb351bbc4a5ad8adc3', '127.0.0.1', 1650692895, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303639323835373b),
('3f28536624d59eeb89720787e7ec47b7cb7e27c8', '127.0.0.1', 1650709028, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303730393032373b),
('2742f88b37dc2bd89608b41be1e1fcae354737f4', '127.0.0.1', 1650711712, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303731313731323b62705f736573735f757365725f69647c733a313a2231223b62705f736573735f757365725f656d61696c7c733a31353a22696e666f40626967706167652e696e223b62705f736573735f757365725f726f6c657c733a353a2261646d696e223b62705f736573735f6c6f676765645f696e7c623a313b62705f736573735f757365726e616d657c733a353a2261646d696e223b62705f736573735f75736572747970657c733a353a2261646d696e223b),
('e5c3203c93e40f783ee3cfe3514af8fbfa8ca1e1', '127.0.0.1', 1650711712, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303731313731323b),
('008c122505dc1ac5f1cfc0e4e5f6067a16b0d3dd', '127.0.0.1', 1650949376, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303934393337353b),
('37f64e12dd2f6971e18ec07fcd5ff5b1c54a4ffa', '127.0.0.1', 1650949916, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303934393931363b62705f736573735f757365725f69647c733a313a2231223b62705f736573735f757365725f656d61696c7c733a31353a22696e666f40626967706167652e696e223b62705f736573735f757365725f726f6c657c733a353a2261646d696e223b62705f736573735f6c6f676765645f696e7c623a313b62705f736573735f757365726e616d657c733a353a2261646d696e223b62705f736573735f75736572747970657c733a353a2261646d696e223b),
('53abec295192fa94a31f608942dcbb207d9669a3', '127.0.0.1', 1650954738, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303935343733383b62705f736573735f757365725f69647c733a313a2231223b62705f736573735f757365725f656d61696c7c733a31353a22696e666f40626967706167652e696e223b62705f736573735f757365725f726f6c657c733a353a2261646d696e223b62705f736573735f6c6f676765645f696e7c623a313b62705f736573735f757365726e616d657c733a353a2261646d696e223b62705f736573735f75736572747970657c733a353a2261646d696e223b),
('73ca9df3f9bf10d70fee147b39c5bfbc03ca5f68', '127.0.0.1', 1650955192, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303935353139323b62705f736573735f757365725f69647c733a313a2231223b62705f736573735f757365725f656d61696c7c733a31353a22696e666f40626967706167652e696e223b62705f736573735f757365725f726f6c657c733a353a2261646d696e223b62705f736573735f6c6f676765645f696e7c623a313b62705f736573735f757365726e616d657c733a353a2261646d696e223b62705f736573735f75736572747970657c733a353a2261646d696e223b),
('67b0286eb32028ef7cf6613231ef6774567bb81e', '127.0.0.1', 1650955213, 0x5f5f63695f6c6173745f726567656e65726174657c693a313635303935353139323b);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `message` varchar(5000) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(11) NOT NULL,
  `site_lang` int(11) DEFAULT 1,
  `application_name` varchar(255) DEFAULT NULL,
  `recaptcha_site_key` varchar(500) DEFAULT NULL,
  `recaptcha_secret_key` varchar(500) DEFAULT NULL,
  `recaptcha_lang` varchar(500) DEFAULT NULL,
  `custom_css_codes` mediumtext DEFAULT NULL,
  `mail_host` varchar(255) DEFAULT NULL,
  `mail_port` varchar(255) DEFAULT NULL,
  `mail_username` varchar(255) DEFAULT NULL,
  `mail_password` varchar(255) DEFAULT NULL,
  `mail_protocol` varchar(15) NOT NULL,
  `mail_title` varchar(255) DEFAULT NULL,
  `google_analytics` text DEFAULT NULL,
  `multilingual_system` int(11) DEFAULT 1,
  `reviews` tinyint(1) DEFAULT 1,
  `blog_comments` int(11) DEFAULT 1,
  `slider_status` tinyint(1) DEFAULT 1,
  `logo` varchar(255) DEFAULT NULL,
  `logo_email` varchar(255) DEFAULT NULL,
  `favicon` varchar(255) DEFAULT NULL,
  `menu_limit` tinyint(4) NOT NULL DEFAULT 9,
  `item_per_page` tinyint(4) NOT NULL,
  `max_file_size_image` bigint(20) DEFAULT 10485760,
  `google_adsense_code` varchar(2000) DEFAULT NULL,
  `custom_javascript_codes` mediumtext DEFAULT NULL,
  `comment_approval_system` tinyint(1) DEFAULT 1,
  `sitemap_frequency` varchar(30) DEFAULT NULL,
  `sitemap_last_modification` varchar(30) DEFAULT NULL,
  `sitemap_priority` varchar(30) DEFAULT NULL,
  `last_cron_update` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `site_lang`, `application_name`, `recaptcha_site_key`, `recaptcha_secret_key`, `recaptcha_lang`, `custom_css_codes`, `mail_host`, `mail_port`, `mail_username`, `mail_password`, `mail_protocol`, `mail_title`, `google_analytics`, `multilingual_system`, `reviews`, `blog_comments`, `slider_status`, `logo`, `logo_email`, `favicon`, `menu_limit`, `item_per_page`, `max_file_size_image`, `google_adsense_code`, `custom_javascript_codes`, `comment_approval_system`, `sitemap_frequency`, `sitemap_last_modification`, `sitemap_priority`, `last_cron_update`) VALUES
(1, 1, 'Bigpage', '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI', '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe', 'en', '', 'smtp.gmail.com', '587', 'user', '123456', 'SMTP', 'Bigpage', '', 1, 1, 1, 1, '94b0f46ba09fe4959217deddabc48908.png', '94b0f46ba09fe4959217deddabc48908.png', 'd5eb4d5aadf468cc6248521a1edbe43e.png', 9, 15, 10485760, NULL, '', 1, 'monthly', 'server_response', 'automatically', '2021-09-18 12:03:33');

-- --------------------------------------------------------

--
-- Table structure for table `network`
--

CREATE TABLE `network` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `parrent_id` int(11) DEFAULT NULL,
  `user_account_id` int(11) DEFAULT NULL,
  `parrent_account_id` int(11) DEFAULT NULL,
  `l1` int(11) DEFAULT 0,
  `l2` int(11) DEFAULT 0,
  `l3` int(11) DEFAULT 0,
  `l4` int(11) DEFAULT 0,
  `l5` int(11) NOT NULL DEFAULT 0,
  `total_compleated_set` bigint(20) DEFAULT 0,
  `total_set_member` bigint(20) NOT NULL DEFAULT 0,
  `is_my_set_compleate` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `network`
--

INSERT INTO `network` (`id`, `user_id`, `parrent_id`, `user_account_id`, `parrent_account_id`, `l1`, `l2`, `l3`, `l4`, `l5`, `total_compleated_set`, `total_set_member`, `is_my_set_compleate`) VALUES
(1, 1, 1, 1, 1, 2, 3, 4, 5, 6, 0, 0, 1),
(2, 2, 1, 2, 1, 7, 8, 9, 10, 11, 0, 0, 1),
(3, 2, 1, 3, 1, 12, 13, 14, 15, 16, 0, 0, 1),
(4, 1, 1, 4, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 1, 1, 5, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 3, 1, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 4, 2, 7, 2, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 5, 2, 8, 2, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 3, 2, 9, 2, 0, 0, 0, 0, 0, 0, 0, 0),
(10, 8, 2, 10, 2, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 9, 2, 11, 2, 0, 0, 0, 0, 0, 0, 0, 0),
(12, 9, 2, 12, 3, 0, 0, 0, 0, 0, 0, 0, 0),
(13, 3, 2, 13, 3, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 3, 2, 14, 3, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 4, 2, 15, 3, 0, 0, 0, 0, 0, 0, 0, 0),
(16, 1, 2, 16, 3, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `order_number` bigint(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `price_total` decimal(10,2) NOT NULL,
  `created_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_number`, `user_id`, `account_id`, `create_by`, `price_total`, `created_on`) VALUES
(1, 10001, 2, 2, 1, '450.00', '2022-03-01 12:10:21'),
(2, 10002, 2, 3, 1, '150.00', '2022-04-06 12:10:31'),
(3, 10003, 1, 4, 1, '150.00', '2022-04-01 12:11:30'),
(4, 10004, 1, 5, 1, '300.00', '2022-04-05 12:11:40'),
(5, 10005, 3, 6, 1, '450.00', '2022-02-01 12:11:51'),
(6, 10006, 4, 7, 1, '450.00', '2022-04-03 12:11:55'),
(7, 10007, 5, 8, 1, '150.00', '2022-04-04 12:11:58'),
(8, 10008, 3, 9, 1, '300.00', '2022-04-04 12:12:00'),
(9, 10009, 8, 10, 1, '1800.00', '2022-04-13 12:12:06'),
(10, 10010, 9, 11, 1, '150.00', '2022-04-12 12:12:04'),
(11, 10011, 9, 12, 1, '150.00', '2022-04-05 12:12:09'),
(12, 10012, 3, 13, 7, '1200.00', '2022-04-05 12:12:13'),
(13, 10013, 3, 14, 1, '2250.00', '2022-04-22 03:52:57'),
(14, 10014, 4, 15, 1, '150.00', '2022-04-22 08:59:37'),
(15, 10015, 1, 16, 1, '600.00', '2022-04-22 09:14:44');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_unit_price` decimal(10,2) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `product_total_price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `order_id`, `product_id`, `product_unit_price`, `product_qty`, `product_total_price`) VALUES
(1, 1, 1, '150.00', 2, '300.00'),
(2, 1, 2, '150.00', 1, '150.00'),
(3, 2, 1, '150.00', 1, '150.00'),
(4, 3, 1, '150.00', 1, '150.00'),
(5, 4, 2, '150.00', 2, '300.00'),
(6, 5, 2, '150.00', 3, '450.00'),
(7, 6, 1, '150.00', 1, '150.00'),
(8, 6, 2, '150.00', 2, '300.00'),
(9, 7, 2, '150.00', 1, '150.00'),
(10, 8, 1, '150.00', 2, '300.00'),
(11, 9, 9, '900.00', 2, '1800.00'),
(12, 10, 1, '150.00', 1, '150.00'),
(13, 11, 2, '150.00', 1, '150.00'),
(14, 12, 1, '150.00', 1, '150.00'),
(15, 12, 2, '150.00', 1, '150.00'),
(16, 12, 9, '900.00', 1, '900.00'),
(17, 13, 1, '150.00', 1, '150.00'),
(18, 13, 2, '150.00', 2, '300.00'),
(19, 13, 9, '900.00', 2, '1800.00'),
(20, 14, 1, '150.00', 1, '150.00'),
(21, 15, 1, '150.00', 4, '600.00');

-- --------------------------------------------------------

--
-- Table structure for table `payout_tracking`
--

CREATE TABLE `payout_tracking` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `amount` double(10,2) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `payment_on` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=pending 1= paid,2= not purcahed '
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payout_tracking`
--

INSERT INTO `payout_tracking` (`id`, `user_id`, `account_id`, `amount`, `description`, `created_at`, `payment_on`, `status`) VALUES
(1, 1, 1, 50.00, 'Comapny reward allotted for upgrading the leavel form leavel-0 to level-1', '2022-04-19 07:57:21', '2022-04-22 03:55:07', 1),
(2, 2, 2, 50.00, 'Comapny reward allotted for upgrading the leavel form leavel-0 to level-1', '2022-04-19 07:57:22', '2022-04-22 03:54:59', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `slug` varchar(500) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `slug`, `description`, `price`, `image`, `status`) VALUES
(1, 'Ghee 500ml', '1', NULL, '150.00', 'f663d3827dbf960d3c66fa62d5a3ff95.jpeg', 1),
(2, 'CTC tea 400grm', '2-2-2', 'test', '150.00', '453b5eb1d53983da7f76451bdafceb12.jpg', 1),
(9, 'CTC TEA 1KG', '9', 'sdsdsadasdsa', '900.00', '524842b9ef473b3597f370afe89d5243.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `site_title` varchar(255) DEFAULT NULL,
  `homepage_title` varchar(255) DEFAULT 'Home',
  `site_description` varchar(500) DEFAULT NULL,
  `keywords` varchar(500) DEFAULT NULL,
  `facebook_url` varchar(500) DEFAULT NULL,
  `twitter_url` varchar(500) DEFAULT NULL,
  `instagram_url` varchar(500) DEFAULT NULL,
  `linkedin_url` varchar(500) DEFAULT NULL,
  `youtube_url` varchar(500) DEFAULT NULL,
  `contact_text` text DEFAULT NULL,
  `contact_address` varchar(500) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `site_font` smallint(6) DEFAULT 19,
  `about_us` text DEFAULT NULL,
  `map` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `site_title`, `homepage_title`, `site_description`, `keywords`, `facebook_url`, `twitter_url`, `instagram_url`, `linkedin_url`, `youtube_url`, `contact_text`, `contact_address`, `contact_email`, `contact_phone`, `created_at`, `site_font`, `about_us`, `map`) VALUES
(1, 'Bigpage', 'Home', 'Bigpage', '', '#', '#', '#', '#', '#', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n', ' Nabapally Circular Rd, Laxmi Narayan Colony, Gupta Colony, Barasat, Kolkata, West Bengal 700126', 'innfo@bigpage.in', '+91 919163631616', '2021-07-20 13:09:21', 19, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14720.423416857297!2d88.4757461!3d22.7243066!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x10711240c4e475e4!2sBigpage%20E-Commerce%20Pvt.%20Ltd%20-%20Website%20Design%2C%20Development%2C%20Ecommerce%20and%20Web%20Hosting%20Company%20in%20Kolkata!5e0!3m2!1sen!2sin!4v1645876420324!5m2!1sen!2sin');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `form_user_id` int(11) NOT NULL,
  `form_account_id` int(11) NOT NULL,
  `user_by` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `perticullars` text NOT NULL,
  `dr` double(10,2) NOT NULL DEFAULT 0.00,
  `cr` double(10,2) NOT NULL DEFAULT 0.00
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `form_user_id`, `form_account_id`, `user_by`, `to_user_id`, `created_at`, `perticullars`, `dr`, `cr`) VALUES
(1, 6, 14, 1, 1, '2022-03-24 08:14:02', 'Product purchase', 600.00, 0.00),
(2, 6, 15, 1, 1, '2022-03-24 08:16:15', 'Product purchase', 300.00, 0.00),
(3, 7, 16, 1, 1, '2022-03-25 07:58:34', 'Product purchase', 750.00, 0.00),
(4, 7, 17, 1, 1, '2022-03-25 08:49:07', 'Product purchase', 150.00, 0.00),
(5, 7, 18, 1, 1, '2022-03-25 08:58:23', 'Product purchase', 150.00, 0.00),
(6, 7, 19, 1, 1, '2022-03-25 09:00:37', 'Product purchase', 750.00, 0.00),
(7, 7, 20, 1, 1, '2022-03-26 00:20:28', 'Product purchase', 1050.00, 0.00),
(8, 2, 2, 1, 1, '2022-03-26 03:35:28', 'Product purchase', 450.00, 0.00),
(9, 2, 3, 1, 1, '2022-03-26 03:36:16', 'Product purchase', 150.00, 0.00),
(10, 1, 4, 1, 1, '2022-03-26 03:37:00', 'Product purchase', 150.00, 0.00),
(11, 1, 5, 1, 1, '2022-03-26 03:37:25', 'Product purchase', 300.00, 0.00),
(12, 3, 6, 1, 1, '2022-03-26 03:38:19', 'Product purchase', 450.00, 0.00),
(13, 4, 7, 1, 1, '2022-03-26 03:39:31', 'Product purchase', 450.00, 0.00),
(14, 5, 8, 1, 1, '2022-03-26 06:48:51', 'Product purchase', 150.00, 0.00),
(18, 3, 9, 1, 0, '2022-04-02 09:49:01', 'Product purchase', 300.00, 0.00),
(19, 8, 10, 1, 0, '2022-04-06 08:47:20', 'Product purchase', 1800.00, 0.00),
(20, 9, 11, 1, 0, '2022-04-06 08:50:01', 'Product purchase', 150.00, 0.00),
(21, 9, 12, 1, 0, '2022-04-06 08:50:45', 'Product purchase', 150.00, 0.00),
(22, 3, 13, 7, 0, '2022-04-13 07:33:15', 'Product purchase', 1200.00, 0.00),
(23, 3, 14, 1, 0, '2022-04-22 03:52:57', 'Product purchase', 2250.00, 0.00),
(24, 1, 2, 1, 2, '2022-04-22 03:54:59', 'Comapny reward allotted for upgrading the leavel form leavel-0 to level-1', 0.00, 50.00),
(25, 1, 1, 0, 1, '2022-04-22 03:55:07', 'Comapny reward allotted for upgrading the leavel form leavel-0 to level-1', 0.00, 50.00),
(26, 4, 15, 1, 0, '2022-04-22 08:59:37', 'Product purchase', 150.00, 0.00),
(27, 1, 16, 1, 0, '2022-04-22 09:14:44', 'Product purchase', 600.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT 'name@domain.com',
  `email_status` int(11) DEFAULT 0,
  `token` varchar(500) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(100) DEFAULT 'member',
  `user_type` varchar(100) DEFAULT 'registered',
  `avatar` varchar(255) DEFAULT NULL,
  `banned` int(11) DEFAULT 0,
  `last_seen` timestamp NULL DEFAULT NULL,
  `full_name` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `sex` varchar(5) NOT NULL,
  `age` varchar(3) NOT NULL,
  `pan` varchar(10) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `spon_id` varchar(255) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `approved_on` varchar(10) DEFAULT NULL,
  `total_leg` int(11) NOT NULL DEFAULT 0,
  `parrent_id` int(11) NOT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `occupation` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `pin` varchar(6) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `acno` varchar(255) DEFAULT NULL,
  `nomine_name` varchar(255) DEFAULT NULL,
  `relation` varchar(255) DEFAULT NULL,
  `bank_ifsc` varchar(255) DEFAULT NULL,
  `is_approved` tinyint(1) NOT NULL DEFAULT 1,
  `created_by` int(11) NOT NULL DEFAULT 1,
  `pan_img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `slug`, `email`, `email_status`, `token`, `password`, `role`, `user_type`, `avatar`, `banned`, `last_seen`, `full_name`, `dob`, `sex`, `age`, `pan`, `mobile`, `spon_id`, `created_on`, `approved_on`, `total_leg`, `parrent_id`, `father_name`, `occupation`, `address`, `state`, `pin`, `phone`, `bank_name`, `acno`, `nomine_name`, `relation`, `bank_ifsc`, `is_approved`, `created_by`, `pan_img`) VALUES
(1, 'admin', 'admin', 'info@bigpage.in', 1, '6146064c0def80-24671451-43991686', '$2a$08$nfZWpgJUAjUWFVRb.LG8M.q9hjWsUFNIMvJdy5R7/ugHF.w/rHrhS', 'admin', 'registered', '59c1c9e204231232d44e45c2bc389296.png', 0, '2022-04-26 03:10:13', 'admin Name', '2009-11-30', 'male', '', '', '9830098300', '', '2022-03-10 12:01:04', NULL, 0, 0, NULL, NULL, 'adminaddress', 'West Bengal', '700126', '9830098305', NULL, NULL, NULL, 'na', NULL, 1, 1, '74c70add6e1c66c104e74dc2b49b2c01.png'),
(2, 'REM22C2602', 'REM22C2602', 'dev@bigpage.in', 1, '623ec9464978b2-53798383-65323507', '$2a$08$nfZWpgJUAjUWFVRb.LG8M.q9hjWsUFNIMvJdy5R7/ugHF.w/rHrhS', 'member', 'registered', NULL, 0, '2022-04-22 09:08:46', 'Debasish Dasgupta', '0000-00-00', '', '', '', '9830098301', '', '2022-03-26 03:35:26', NULL, 0, 0, NULL, NULL, 'Barasat', 'West Bengal', '700126', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL),
(3, 'REM22C2603', 'REM22C2603', 'avijit@bigpage.in', 1, '623ec9f2a22551-04647011-51034558', '$2a$08$KLUI/CBgSvQk6FmbIS/V.OPCV7MfB8XUqWHNGmTufHK.vFm0Z.1ie', 'member', 'registered', NULL, 0, '2022-03-26 03:38:18', 'Avijit Roy', '0000-00-00', '', '', '', '9830098302', '', '2022-03-26 03:38:18', NULL, 0, 0, NULL, NULL, 'Barasat', 'West Bengal', '700126', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL),
(4, 'REM22C2604', 'REM22C2604', 'biswajit@bigpage.in', 1, '623eca3ac89698-93833192-77458371', '$2a$08$DTizP9t0FoSilvd5pW64tO2X8BuOH6cIZHSa5tkcK9L8eLZMJFmN.', 'member', 'registered', NULL, 0, '2022-03-26 03:39:30', 'Biswajit Guha', '0000-00-00', '', '', '', '9830098303', '', '2022-03-26 03:39:30', NULL, 0, 0, NULL, NULL, 'Kolkata', 'West Bengal', '700126', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL),
(5, 'REM22C2605', 'REM22C2605', 'swarnendu@bigpage.in', 1, '623ef69ace9c12-53562923-25471795', '$2a$08$UsCSDrcZx8wP.s7kXsAuLOBVza7FC/PHi2nUmqoNaFf2q5E4o4YSa', 'member', 'registered', NULL, 0, '2022-03-26 06:48:50', 'Swarnendu Roy', '0000-00-00', '', '', '', '9830098304', '', '2022-03-26 06:48:50', NULL, 0, 0, NULL, NULL, 'Barasat', 'West Bengal', '700126', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL),
(6, 'RES22C2606', 'RES22C2606', 'debasish.bigpage@gmail.com', 1, '623ef9ac07b501-59201520-94826038', '123456', 'staff', 'registered', NULL, 0, '2022-03-26 07:01:56', 'Biswajit Roy', '1951-01-01', '', '', '', '9163631616', '', '2022-03-26 07:01:56', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL),
(7, 'RES22C2807', 'RES22C2807', 'avijitstaff@bigpage.in', 1, '62414212bcdb93-47094142-73611889', '$2a$08$/sIDKXIFI0fy1pVthc9kJOlTlXZBWHaXxoPheiDFftredYull3YVa', 'staff', 'registered', 'd258debb364108ca100d2ef1639bdc8b.png', 0, '2022-04-22 09:06:49', 'Avijit Staff', '1990-01-01', 'male', '', '', '9830098306', '', '2022-03-28 01:35:22', NULL, 0, 0, NULL, NULL, 'Baraasat', 'West Bengal', '700126', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL),
(8, 'REM22D0608', 'REM22D0608', '', 1, '624d84cf48b2d4-10629225-89773595', '$2a$08$5k0dkXSKBmcP8pmwB601yORGoD4Lcfm6F1lhTpuJcFTIByoy01T42', 'member', 'registered', NULL, 0, '2022-04-06 08:47:19', 'Debasish Roy', '0000-00-00', '', '', '', '9830098305', '', '2022-04-06 08:47:19', NULL, 0, 0, NULL, NULL, 'barrasat', 'West Bengal', '700126', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL),
(9, 'REM22D0609', 'REM22D0609', '', 1, '624d85711b00b7-95152460-15147116', '$2a$08$TZrzv0aQDe8xWnmEA4a7BOFnI/4BkVYpN2GPJH5zV9upB/PEw9CGK', 'member', 'registered', NULL, 0, '2022-04-14 09:18:39', 'Loknath Ghosh', '0000-00-00', '', '', '', '6289265439', '', '2022-04-06 08:50:01', NULL, 0, 0, NULL, NULL, 'Barasat', 'West Bengal', '700126', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_id` text NOT NULL,
  `mlm_level` tinyint(1) NOT NULL DEFAULT 0,
  `total_child` int(11) NOT NULL DEFAULT 0,
  `created_on` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `stop_payment` tinyint(1) NOT NULL DEFAULT 0,
  `is_set_leg` tinyint(1) NOT NULL DEFAULT 0,
  `seat_full` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`id`, `user_id`, `account_id`, `mlm_level`, `total_child`, `created_on`, `created_by`, `stop_payment`, `is_set_leg`, `seat_full`) VALUES
(1, 1, 'RMA202201', 1, 13, '2022-03-26 08:02:43', 1, 0, 0, 1),
(2, 2, 'REA22C2602', 1, 5, '2022-03-26 03:35:27', 1, 0, 1, 1),
(3, 2, 'REA22C2603', 0, 3, '2022-03-26 03:36:14', 1, 0, 1, 1),
(4, 1, 'REA22C2604', 0, 0, '2022-03-26 03:37:00', 1, 0, 1, 0),
(5, 1, 'REA22C2605', 0, 0, '2022-03-26 03:37:24', 1, 0, 1, 0),
(6, 3, 'REA22C2606', 0, 0, '2022-03-26 03:38:18', 1, 0, 1, 0),
(7, 4, 'REA22C2607', 0, 0, '2022-03-26 03:39:30', 1, 0, 1, 0),
(8, 5, 'REA22C2608', 0, 0, '2022-03-26 06:48:50', 1, 0, 1, 0),
(9, 3, 'REA22D0209', 0, 0, '2022-04-02 09:49:00', 1, 0, 1, 0),
(10, 8, 'REA22D06010', 0, 0, '2022-04-06 08:47:19', 1, 0, 1, 0),
(11, 9, 'REA22D06011', 0, 0, '2022-04-06 08:50:01', 1, 0, 1, 0),
(12, 9, 'REA22D06012', 0, 0, '2022-04-06 08:50:45', 1, 0, 1, 0),
(13, 3, 'REA22D13013', 0, 0, '2022-04-13 07:33:14', 7, 0, 1, 0),
(14, 3, 'REA22D22014', 0, 0, '2022-04-22 03:52:57', 1, 0, 1, 0),
(15, 4, 'REA22D22015', 0, 0, '2022-04-22 08:59:37', 1, 0, 1, 0),
(16, 1, 'REA22D22016', 0, 0, '2022-04-22 09:14:44', 1, 0, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `business_settings`
--
ALTER TABLE `business_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `network`
--
ALTER TABLE `network`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payout_tracking`
--
ALTER TABLE `payout_tracking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `business_settings`
--
ALTER TABLE `business_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `network`
--
ALTER TABLE `network`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `payout_tracking`
--
ALTER TABLE `payout_tracking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
